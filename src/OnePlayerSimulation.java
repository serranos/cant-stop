import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by sofiaserrano on 6/28/16.
 */
public class OnePlayerSimulation {
    public final int[] defaultValues = { 3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3 };
    private final boolean COMPUTER_IS_PLAYING = true;
    public int[][] board;
    private boolean gameHasBeenWon;
    private int numPlayers = 1;
    private int CAPTURED = -50;
    private DecisionMaker dm;
    private SimulationReport gameInterface;
    private int turns;
    private int[] columnsOrRoll;
    private boolean setColumns;
    private int[] initialColumnChoice;

    public OnePlayerSimulation(String filename, String whichType) {
        if (whichType.trim().toLowerCase().equals("setcolumns")) {
            setColumns = true;
        } else {
            setColumns = false;
        }
        board = new int[1][11];
        dm = new ProbabilisticDecisionMaker();
        gameInterface = new SimulationReport(filename, setColumns);
        gameInterface.setOnePlayerSimulation(this);
    }

    public OnePlayerSimulation() {

    }

    public void playGame(int[] inputInfo)
    {
        this.columnsOrRoll = inputInfo;
        if (inputInfo.length != 4) {
            for (int i = 0; i < inputInfo.length; i++) {
                board[0][inputInfo[i] - 2] += 1;
            }
        }
        instantiateBoard();
        int player = 0;
        gameHasBeenWon = false;
        boolean firstTimeThrough = true;
        turns = 0;
        while (!gameHasBeenWon)
        {
            turns++;
            for (player = 0; player < numPlayers && !gameHasBeenWon; player++)
            {
                boolean willRollAgain = true;
                boolean notMissed = true;
                int[] startingPositions = getStartingPositions(player);
                List<Integer> placeholders = new ArrayList<Integer>();
                if (inputInfo.length != 4 && firstTimeThrough) {
                    for (int i = 0; i < inputInfo.length; i++) {
                        if (!placeholders.contains(new Integer(inputInfo[i]))) {
                            placeholders.add(inputInfo[i]);
                        }
                    }
                }
                gameInterface.showWhoseTurnItIs(player, COMPUTER_IS_PLAYING);
                while (willRollAgain && notMissed)
                {
                    int[] roll;
                    if (firstTimeThrough)
                    {
                        gameInterface.updateDisplayOfBoard(board, player, placeholders, startingPositions);
                        if (inputInfo.length == 4) {
                            roll = inputInfo;
                        } else {
                            roll = rollDice();
                        }
                    } else {
                        roll = rollDice();
                    }
                    if (player == 0 && COMPUTER_IS_PLAYING)
                    {
                        //System.out.println("Computer's turn -----------------------------");
                        List<int[]> validMoves = getValidMoves(roll, placeholders, player);
						/*System.out.println("V M : ");
						for (int i = 0; i < validMoves.size(); i++)
						{
							System.out.print(Arrays.toString(validMoves.get(i)) + " ");
						}*/
                        //System.out.println();
                        if (validMoves.size() > 1)
                        {

                            int maxIndex = dm.decideOnPair(player, validMoves, placeholders, board, startingPositions);
                            gameInterface.getChoiceAboutDicePairing(roll, validMoves, placeholders, maxIndex);
                            int[] twoNumbersUserChose = validMoves.get(maxIndex);
                            if (firstTimeThrough) {
                                initialColumnChoice = twoNumbersUserChose;
                            }

							/*System.out.println("More than one valid move");
							System.out.println("\tPlaceholders are at " + placeholders.toString());
							System.out.println("\tRoll is " + Arrays.toString(roll));
							System.out.println("Computer chose: " + twoNumbersUserChose[0] + "   "+ twoNumbersUserChose[1]);*/

                            willRollAgain = makeMove(twoNumbersUserChose, player, placeholders);
                            gameInterface.updateDisplayOfBoard(board, player, placeholders, startingPositions);
                            if (!willRollAgain && checkGame(player))
                            {
                                gameHasBeenWon = true;
                                gameInterface.displayWin(player, COMPUTER_IS_PLAYING);
                                break;
                            }
                            if (!willRollAgain)
                            {
								/*if(placeholders.size() == 3)
								{*/
                                willRollAgain = dm.shouldRoll(player, board, placeholders, startingPositions);
								/*}
								else
								{
									willRollAgain = true;
								}*/

                                if (willRollAgain)
                                {
                                    gameInterface.getChoiceWhetherToRollAgain(1);
                                }
                                else
                                {
                                    gameInterface.getChoiceWhetherToRollAgain(0);
                                }
                            }
                        }
                        else if (validMoves.size() == 1)
                        {
                            int[] onlyChoiceForTwoNumbers = validMoves.get(0);
                            if (firstTimeThrough) {
                                initialColumnChoice = onlyChoiceForTwoNumbers;
                            }
                            gameInterface.showWhatOnlyDiceOptionIs(roll, false, placeholders, onlyChoiceForTwoNumbers,
                                true);
                            willRollAgain = makeMove(onlyChoiceForTwoNumbers, player, placeholders);
                            gameInterface.updateDisplayOfBoard(board, player, placeholders, startingPositions);
                            if (!willRollAgain && checkGame(player))
                            {
                                gameHasBeenWon = true;
                                gameInterface.displayWin(player, COMPUTER_IS_PLAYING);
                                break;
                            }
                            if (!willRollAgain)
                            {
                                willRollAgain = dm.shouldRoll(player, board, placeholders, startingPositions);
								/*}
								else
								{
									willRollAgain = true;
								}*/

                                if (willRollAgain)
                                {
                                    gameInterface.getChoiceWhetherToRollAgain(1);
                                }
                                else
                                {
                                    gameInterface.getChoiceWhetherToRollAgain(0);
                                }
                            }
                        }
                        else
                        {
                            gameInterface.showWhatOnlyDiceOptionIs(roll, true, placeholders, roll, true);
                            notMissed = false;
                            for (int i = 0; i < 11; i++)
                            {
                                board[player][i] = startingPositions[i];
                            }
                            gameInterface.updateDisplayOfBoard(board, player, new ArrayList<Integer>(),
                                startingPositions);
                        }
                    }
                    firstTimeThrough = false;
                }
                setCaptured(player, placeholders);
            }
        }
    }

    public int getTotalTurns() {
        return turns;
    }

    public int[] getLastSetOfColumnsOrRoll() {
        return columnsOrRoll;
    }

    public int[] getInitialColumnChoice() {
        return initialColumnChoice;
    }

    public void instantiateBoard()
    {
        for (int i = 0; i < 11; i++)
        {
            for (int j = 0; j < board.length; j++)
            {
                if (i == 0)
                {
                    board[j][i] = -3;
                }
                else if (i == 1)
                {
                    board[j][i] = -5;
                }
                else if (i == 2)
                {
                    board[j][i] = -7;
                }
                else if (i == 3)
                {
                    board[j][i] = -9;
                }
                else if (i == 4)
                {
                    board[j][i] = -11;
                }
                else if (i == 5)
                {
                    board[j][i] = -13;
                }
                else if (i == 6)
                {
                    board[j][i] = -11;
                }
                else if (i == 7)
                {
                    board[j][i] = -9;
                }
                else if (i == 8)
                {
                    board[j][i] = -7;
                }
                else if (i == 9)
                {
                    board[j][i] = -5;
                }
                else
                {
                    board[j][i] = -3;
                }
            }
        }
    }

    public int[] rollDice()
    {
        int[] returnDice = { (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1,
            (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1 };
        Arrays.sort(returnDice);
        return returnDice;
    }

    public List<int[]> getPossiblePairings(int[] dice)
    {
        List<int[]> ret = new ArrayList<int[]>();
        int totalSum = dice[0] + dice[1] + dice[2] + dice[3];

        for (int i = 1; i < 4; i++)
        {
            int sum1 = dice[0] + dice[i];
            int sum2 = totalSum - sum1;
            int[] newComb = new int[2];
            if (sum1 < sum2)
            {
                newComb[0] = sum1;
                newComb[1] = sum2;
            }
            else
            {
                newComb[0] = sum2;
                newComb[1] = sum1;
            }
            boolean in = false;

            for (int[] comb : ret)
            {
                if (newComb[0] == comb[0] && newComb[1] == comb[1])
                {
                    in = true;
                    break;
                }
            }
            if (!in)
                ret.add(newComb);
        }
        return ret;
    }

    private void setCaptured(int player, List<Integer> placeholders)
    {
        for (Integer p : placeholders)
        {
            if (board[player][p - 2] == 0)// that player won the column
            {
                for (int k = 0; k < board.length; k++)
                {
                    if (k != player)
                    {
                        board[k][p - 2] = CAPTURED;// other players
                        // can no longer
                        // play on that
                        // column
                    }
                }
            }
        }

    }

    private boolean checkGame(int player)
    {
        int count = 0;
        for (int i = 0; i < board[player].length; i++)
        {
            if (board[player][i] == 0)
                count++;
        }
        return count >= 3;
    }

    public List<int[]> getValidMoves(int[] roll, List<Integer> placeholders, int player)
    {
        List<int[]> ret = new ArrayList<int[]>();
        List<int[]> possiblePairs = getPossiblePairings(roll);
        for (int[] pos : possiblePairs)
        {
            boolean has0 = placeholders.contains(pos[0]);
            boolean has1 = placeholders.contains(pos[1]);

            if (board[player][pos[0] - 2] == 0)
                has0 = false;
            if (board[player][pos[1] - 2] == 0)
                has1 = false;
            if (has0 && has1)
            {
                int[] newMove = new int[2];
                newMove[0] = pos[0];
                newMove[1] = pos[1];
                ret.add(newMove);
            }
            else if (has0)
            {
                int[] newMove = new int[2];
                newMove[0] = pos[0];
                newMove[1] = -1;
                if (placeholders.size() < 3)
                {
                    if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
                        newMove[1] = pos[1];
                }
                ret.add(newMove);
            }
            else if (has1)
            {
                int[] newMove = new int[2];
                newMove[0] = pos[1];
                newMove[1] = -1;
                if (placeholders.size() < 3)
                {
                    if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
                        newMove[1] = pos[0];
                }
                ret.add(newMove);
            }
            else
            {
                if (placeholders.size() < 2)
                {
                    int[] newMove = new int[2];
                    if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
                        newMove[0] = pos[0];
                    else
                        newMove[0] = -1;
                    if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
                        newMove[1] = pos[1];
                    else
                        newMove[1] = -1;
                    if (newMove[0] != -1 || newMove[1] != -1)
                        ret.add(newMove);
                }
                else if (placeholders.size() < 3)
                {
                    if (pos[0] != pos[1])
                    {
                        if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
                        {
                            int[] newMove = new int[2];
                            newMove[0] = pos[0];
                            newMove[1] = -1;
                            ret.add(newMove);
                        }
                        if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
                        {
                            int[] otherMove = new int[2];
                            otherMove[0] = pos[1];
                            otherMove[1] = -1;
                            ret.add(otherMove);
                        }
                    }
                    else
                    {
                        if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
                        {
                            int[] newMove = new int[2];
                            newMove[0] = pos[0];
                            newMove[1] = pos[0];
                            ret.add(newMove);
                        }

                    }
                }
            }
        }
        return ret;
    }

    public boolean makeMove(int[] choice, int player, List<Integer> placeholders)
    {
        for (int i = 0; i < 2; i++)
        {
            if (choice[i] != -1)
            {
                if (placeholders.contains(choice[i]))
                {
                    if (board[player][choice[i] - 2] != 0)
                        board[player][choice[i] - 2]++;
                }
                else
                {
                    placeholders.add(choice[i]);
                    board[player][choice[i] - 2]++;

                }
            }
        }
        boolean hasToRoll = false;
        for (int p = 0; p < board.length && !hasToRoll; p++)
        {
            for (int p2 = p + 1; p2 < board.length && !hasToRoll; p2++)
            {
                for (int i = 0; i < board[0].length && !hasToRoll; i++)
                {
                    if (board[p][i] != -1 * defaultValues[i] && board[p2][i] == board[p][i])
                    {
						/*for(int t = 0; t < board.length; t++)
							System.out.println(Arrays.toString(board[t]));
						System.out.println(p + " " + p2 + " " + " " + i + " " + board[p][i] + " " + board[p2][i] + " " + defaultValues[i]);
						*/hasToRoll = true;
                    }
                }
            }
        }
        return hasToRoll;
    }

    public int[] getStartingPositions(int player)
    {
        int[] returnArray = new int[11];
        for (int i = 0; i < 11; i++)
        {
            returnArray[i] = board[player][i];
        }
        return returnArray;
    }

    public void runFullSimulation() {
        if (setColumns) {
            int[] columns = new int[2];
            int counter = 0;
            for (int i = 2; i <= 12; i++) {
                for (int j = i; j <= 12; j++) {
                    //for (int k = j + 1; k <= 12; k++) {
                        columns[0] = i;
                        columns[1] = i;
                        for (int h = 0; h < 1000; h++) {
                            playGame(columns);
                        }
                    //}
                    counter++;
                    System.out.println(counter + "/55 done");
                }
            }
        } else {
            // set roll
            int[] roll = new int[4];
            for (int a = 1; a < 7; a++) {
                for (int b = 1; b < 7; b++) {
                    for (int c = 1; c < 7; c++) {
                        for (int d = 1; d < 7; d++) {
                            roll[0] = a;
                            roll[1] = b;
                            roll[2] = c;
                            roll[3] = d;
                            playGame(roll);
                        }
                    }
                }
            }
        }
    }

    private void produceOutputFileOfAverages() {
        PrintWriter toFile = null;
        try {
            toFile = new PrintWriter("Averages.txt");
        } catch (FileNotFoundException e) {
            System.err.println("Averages.txt not created.");
        }
        Scanner scan = null;
        try {
            scan = new Scanner(new File("SetColumnSimulationResults1000.txt"));
        } catch (FileNotFoundException e) {
            System.err.println("SetColumnSimulationResults1000.txt not found.");
        }
        TreeMap<List<Integer>, Integer> map = new TreeMap<>();
        while (scan.hasNextLine()) {
            String line = scan.nextLine().trim();
            if (line.contains("[")) {
                int firstNum = Integer.parseInt(line.substring(1, line.indexOf(",")));
                int secondNum = Integer.parseInt(line.substring(line.indexOf(",") + 2, line.indexOf("]")));
                int count = Integer.parseInt(line.substring(line.indexOf("]") + 2));
                List<Integer> array = new ComparableArrayList<Integer>();
                array.add(firstNum);
                array.add(secondNum);
                Integer oldVal = map.get(array);
                if (oldVal != null) {
                    map.put(array, oldVal + count);
                } else {
                    map.put(array, count);
                }
            }
        }
        Set<List<Integer>> keys = map.keySet();
        for (List<Integer> key : keys) {
            Integer totalCount = map.get(key);
            toFile.println("[" + key.get(0) + ", " + key.get(1) + "] " + (1.0 * totalCount / 1000.0));
        }
        toFile.close();
    }

    public static void main(String[] args) {
        OnePlayerSimulation ops = null;
        if (args.length == 2) {
            ops = new OnePlayerSimulation(args[0], args[1]);
        } else if (args.length == 1) {
            ops = new OnePlayerSimulation();
        }
        //ops.runFullSimulation();
        ops.produceOutputFileOfAverages();
    }
}
