import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class RollOut
{
	private final int MAX_THREAD_COUNT = 6;
	private final int[] DEFAULT_VALUES = { 3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3 };
	private final int CAPTURED = -100;

	int[] winCounts;
	int winCountIfRoll;
	int winCountIfStop;
	private ProbabilisticDecisionMaker pdm;
	private List<Integer> initialPlaceholders;
	private int[] initialStartingPositions;
	private int[][] initialBoard;
	private int numTimes;
	private int numPlayers;

	public RollOut(List<int[]> availableMoves, List<Integer> initialPlaceholders, int[] initialStartingPositions, int[][] initialBoard, int totTimes)
	{
		pdm = new ProbabilisticDecisionMaker();
		pdm.decidingPairIsMonteCarlo = false;
		pdm.shouldRollIsMonteCarlo = false;
		this.initialPlaceholders = initialPlaceholders;
		this.initialStartingPositions = initialStartingPositions;
		this.initialBoard = initialBoard;
		numPlayers = initialBoard.length;

		ExecutorService executor = Executors.newFixedThreadPool(MAX_THREAD_COUNT);
		// SingleRollOut[] rolls = new SingleRollOut[winCounts.length];
		ArrayList<Future<Integer>> futures = new ArrayList<Future<Integer>>();

		if (availableMoves == null)
		{
			winCountIfRoll = 0;
			winCountIfStop = 0;
			int threadPerMove = MAX_THREAD_COUNT / 2;
			numTimes = totTimes / threadPerMove;
			for (int i = 0; i < threadPerMove; i++)
				futures.add(i, executor.submit(new RollOrStopRollOut(true)));
			for (int i = 0; i < threadPerMove; i++)
				futures.add(threadPerMove + i, executor.submit(new RollOrStopRollOut(false)));

			for (int i = 0; i < threadPerMove; i++)
			{
				try
				{
					winCountIfRoll += futures.get(i).get();
				}
				catch (InterruptedException | ExecutionException e)
				{
					e.printStackTrace();
				}
			}

			for (int i = 0; i < threadPerMove; i++)
			{
				try
				{
					winCountIfStop += futures.get(threadPerMove + i).get();
				}
				catch (InterruptedException | ExecutionException e)
				{
					e.printStackTrace();
				}
			}
		}
		else
		{
			winCounts = new int[availableMoves.size()];
			int threadPerMove = MAX_THREAD_COUNT / winCounts.length;
			numTimes = totTimes / threadPerMove;
			for (int i = 0; i < winCounts.length; i++)
			{
				for (int j = 0; j < threadPerMove; j++)
				{
					futures.add(i * threadPerMove + j, executor.submit(new SingleRollOut(availableMoves.get(i))));
				}
			}

			for (int i = 0; i < winCounts.length; i++)
			{
				for (int j = 0; j < threadPerMove; j++)
				{
					try
					{
						winCounts[i] += futures.get(i * threadPerMove + j).get();
					}
					catch (InterruptedException | ExecutionException e)
					{
						e.printStackTrace();
					}
				}
			}
		}
	}

	private class SingleRollOut implements Callable<Integer>
	{
		private int[] initialMove;

		SingleRollOut(int[] initialMove)
		{
			this.initialMove = initialMove;
		}

		@Override
		public Integer call() throws Exception
		{
			int numWins = 0;
			boolean firstTimeThrough, gameHasBeenWon;
			int[][] board = new int[numPlayers][DEFAULT_VALUES.length];

			for (int turn = 0; turn < numTimes; turn++)
			{
				gameHasBeenWon = false;
				firstTimeThrough = true;
				for (int i = 0; i < numPlayers; i++)
					for (int j = 0; j < 11; j++)
						board[i][j] = initialBoard[i][j];

				int player = 0;
				while (!gameHasBeenWon)
				{
					for (player = 0; player < numPlayers && !gameHasBeenWon; player++)
					{
						boolean willRollAgain = true, notMissed = true;
						int[] startingPositions;
						List<Integer> placeholders;
						if (firstTimeThrough)
						{
							firstTimeThrough = false;
							startingPositions = Arrays.copyOf(initialStartingPositions, initialStartingPositions.length);
							placeholders = new ArrayList<Integer>(initialPlaceholders);
							willRollAgain = makeMove(initialMove, player, placeholders, board);
							if (!willRollAgain)
							{
								willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
							}
						}
						else
						{
							startingPositions = getStartingPositions(player, board);
							placeholders = new ArrayList<Integer>();
						}

						while (willRollAgain && notMissed)
						{
							int[] roll = rollDice();
							List<int[]> validMoves = getValidMoves(roll, placeholders, player, board);

							if (validMoves.size() > 1)
							{
								int maxIndex = pdm.decideOnPair(player, validMoves, placeholders, board, startingPositions);
								int[] twoNumbersUserChose = validMoves.get(maxIndex);
								willRollAgain = makeMove(twoNumbersUserChose, player, placeholders, board);
								if (!willRollAgain && checkGame(player, board))
								{
									gameHasBeenWon = true;
									break;
								}
								if (!willRollAgain)
								{
									willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
								}
							}
							else if (validMoves.size() == 1)
							{
								int[] onlyChoiceForTwoNumbers = validMoves.get(0);
								willRollAgain = makeMove(onlyChoiceForTwoNumbers, player, placeholders, board);
								if (!willRollAgain && checkGame(player, board))
								{
									gameHasBeenWon = true;
									break;
								}
								if (!willRollAgain)
								{
									willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
								}
							}
							else
							{
								notMissed = false;
								for (int i = 0; i < 11; i++)
								{
									board[player][i] = startingPositions[i];
								}
							}
						}
						setCaptured(player, placeholders, board);
					}
				}

				if (player == 1) numWins++;
			}
			return numWins;
		}
	}

	private class RollOrStopRollOut implements Callable<Integer>
	{
		private boolean roll;

		RollOrStopRollOut(boolean roll)
		{
			this.roll = roll;
		}

		public Integer call() throws Exception
		{
			int numWins = 0;
			boolean firstTimeThrough, gameHasBeenWon;
			int[][] board = new int[numPlayers][DEFAULT_VALUES.length];

			for (int turn = 0; turn < numTimes; turn++)
			{
				gameHasBeenWon = false;
				firstTimeThrough = true;
				for (int i = 0; i < numPlayers; i++)
					for (int j = 0; j < 11; j++)
						board[i][j] = initialBoard[i][j];

				int player = 0;
				while (!gameHasBeenWon)
				{
					for (player = 0; player < numPlayers && !gameHasBeenWon; player++)
					{
						boolean willRollAgain = true, notMissed = true;
						int[] startingPositions;
						List<Integer> placeholders;
						if (firstTimeThrough)
						{
							firstTimeThrough = false;
							startingPositions = Arrays.copyOf(initialStartingPositions, initialStartingPositions.length);
							placeholders = new ArrayList<Integer>(initialPlaceholders);
							willRollAgain = roll;
						}
						else
						{
							startingPositions = getStartingPositions(player, board);
							placeholders = new ArrayList<Integer>();
						}
						while (willRollAgain && notMissed)
						{
							int[] roll = rollDice();
							List<int[]> validMoves = getValidMoves(roll, placeholders, player, board);

							if (validMoves.size() > 1)
							{
								int maxIndex = pdm.decideOnPair(player, validMoves, placeholders, board, startingPositions);
								int[] twoNumbersUserChose = validMoves.get(maxIndex);
								willRollAgain = makeMove(twoNumbersUserChose, player, placeholders, board);
								if (!willRollAgain && checkGame(player, board))
								{
									gameHasBeenWon = true;
									break;
								}
								if (!willRollAgain)
								{
									willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
								}
							}
							else if (validMoves.size() == 1)
							{
								int[] onlyChoiceForTwoNumbers = validMoves.get(0);
								willRollAgain = makeMove(onlyChoiceForTwoNumbers, player, placeholders, board);
								if (!willRollAgain && checkGame(player, board))
								{
									gameHasBeenWon = true;
									break;
								}
								if (!willRollAgain)
								{
									willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
								}
							}
							else
							{
								notMissed = false;
								for (int i = 0; i < 11; i++)
								{
									board[player][i] = startingPositions[i];
								}
							}
						}
						setCaptured(player, placeholders, board);
					}
				}

				if (player == 1) numWins++;
			}
			return numWins;
		}
	}

	private int[] getStartingPositions(int player, int[][] board)
	{
		int[] returnArray = new int[11];
		for (int i = 0; i < 11; i++)
		{
			returnArray[i] = board[player][i];
		}
		return returnArray;
	}

	private boolean checkGame(int player, int[][] board)
	{
		int count = 0;
		for (int i = 0; i < board[player].length; i++)
		{
			if (board[player][i] == 0) count++;
		}
		return count >= 3;
	}

	private List<int[]> getValidMoves(int[] roll, List<Integer> placeholders, int player, int[][] board)
	{
		List<int[]> ret = new ArrayList<int[]>();
		List<int[]> possiblePairs = pdm.getPossiblePairings(roll);
		for (int[] pos : possiblePairs)
		{
			boolean has0 = placeholders.contains(pos[0]);
			boolean has1 = placeholders.contains(pos[1]);

			if (board[player][pos[0] - 2] == 0) has0 = false;
			if (board[player][pos[1] - 2] == 0) has1 = false;
			if (has0 && has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = pos[1];
				ret.add(newMove);
			}
			else if (has0)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0) newMove[1] = pos[1];
				}
				ret.add(newMove);
			}
			else if (has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[1];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0) newMove[1] = pos[0];
				}
				ret.add(newMove);
			}
			else
			{
				if (placeholders.size() < 2)
				{
					int[] newMove = new int[2];
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0) newMove[0] = pos[0];
					else newMove[0] = -1;
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0) newMove[1] = pos[1];
					else newMove[1] = -1;
					if (newMove[0] != -1 || newMove[1] != -1) ret.add(newMove);
				}
				else if (placeholders.size() < 3)
				{
					if (pos[0] != pos[1])
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = -1;
							ret.add(newMove);
						}
						if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
						{
							int[] otherMove = new int[2];
							otherMove[0] = pos[1];
							otherMove[1] = -1;
							ret.add(otherMove);
						}
					}
					else
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = pos[0];
							ret.add(newMove);
						}

					}
				}
			}
		}
		return ret;
	}

	private boolean makeMove(int[] choice, int player, List<Integer> placeholders, int[][] board)
	{
		for (int i = 0; i < 2; i++)
		{
			if (choice[i] != -1)
			{
				if (placeholders.contains(choice[i]))
				{
					if (board[player][choice[i] - 2] != 0) board[player][choice[i] - 2]++;
				}
				else
				{
					placeholders.add(choice[i]);
					board[player][choice[i] - 2]++;

				}
			}
		}
		boolean hasToRoll = false;
		for (int p = 0; p < board.length && !hasToRoll; p++)
		{
			for (int p2 = p + 1; p2 < board.length && !hasToRoll; p2++)
			{
				for (int i = 0; i < board[0].length && !hasToRoll; i++)
				{
					if (board[p][i] != -1 * DEFAULT_VALUES[i] && board[p2][i] == board[p][i])
					{
						hasToRoll = true;
					}
				}
			}
		}
		return hasToRoll;
	}

	private void setCaptured(int player, List<Integer> placeholders, int[][] board)
	{
		for (Integer p : placeholders)
		{
			if (board[player][p - 2] == 0)// that player won the column
			{
				for (int k = 0; k < board.length; k++)
				{
					if (k != player)
					{
						board[k][p - 2] = CAPTURED;
					}
				}
			}
		}
	}

	private int[] rollDice()
	{
		int[] returnDice = { (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1 };
		Arrays.sort(returnDice);
		return returnDice;
	}
}
