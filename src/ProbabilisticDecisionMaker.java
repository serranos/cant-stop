import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;

public class ProbabilisticDecisionMaker extends DecisionMaker
{
	public boolean decidingPairIsMonteCarlo = false;
	public boolean shouldRollIsMonteCarlo = false;
	private int decidingPairTimes = 1200;
	private int shouldRollTimes = 600;

	private int[] lengths = {};
	private int[] probs;
	private int avgProb;
	private int dieFaces = 6;
	private int permutations = dieFaces * dieFaces * dieFaces * dieFaces;
	private List<int[]> diceCombos;
	private List<Integer> frequencies;
	public final int emptyCol = (2 * dieFaces - 2) + 2 + 1;

	//CONSTRUCTOR
	public ProbabilisticDecisionMaker()
	{
		String filename = "supplementary/" + dieFaces + "sidedDieCombsAndWeights.txt";
		setVariables(filename);
	}

	private void setVariables(String filename)
	{
		diceCombos = new ArrayList<>();
		frequencies = new ArrayList<>();
		Scanner scanner = null;
		URL url = ClassLoader.getSystemResource(filename);
		try
		{
			scanner = new Scanner(url.openStream());
		}
		catch (IOException e)
		{
			System.err.println("Problem loading file " + filename);
		}
		int turn = 0;
		while (scanner.hasNextInt())
		{
			if (turn == 0)
			{
				diceCombos.add(new int[4]);
			}

			if (turn < 4)
			{
				diceCombos.get(diceCombos.size() - 1)[turn] = scanner.nextInt();
			}
			turn++;
			if (turn == 4) frequencies.add(scanner.nextInt());
			if (turn == 5) turn = 0;
		}
		scanner.close();

		String weightFileName = "supplementary/weightsFor" + dieFaces + "SidedDie.txt";
		url = ClassLoader.getSystemResource(weightFileName);
		try
		{
			scanner = new Scanner(url.openStream());
		}
		catch (IOException e)
		{
			System.err.println("Problem loading file " + weightFileName);
		}
		probs = new int[(2 * dieFaces - 2) + 1];
		int index = 0;
		int sumOfProbs = 0;
		while (scanner.hasNextInt())
		{
			probs[index] = scanner.nextInt();
			sumOfProbs += probs[index];
			index++;
		}
		scanner.close();

		avgProb = sumOfProbs / probs.length;

		lengths = new int[(2 * dieFaces - 2) + 1];
		lengths[0] = 3;
		for (int i = 1; i <= (2 * dieFaces - 2) / 2; i++)
			lengths[i] = lengths[i - 1] + 2;
		for (int i = (2 * dieFaces - 2) / 2 + 1; i < (2 * dieFaces - 2) + 1; i++)
			lengths[i] = lengths[i - 1] - 2;
		// System.out.println(Arrays.toString(expectedProgs));
	}

	// DECIDE ON PAIR METHODS
	/**
	 * The first implemented decide on pair method. Looks at only the relative
	 * progress at a column after having made a move.
	 * 
	 * @return index of the preferred move in availableMoves
	 */
	public int decideOnPairOriginal(int player, List<int[]> availableMoves, int[][] board, int[] startingPos)
	{
		int[] positions = board[player];
		double[] values = new double[availableMoves.size()];
		for (int i = 0; i < availableMoves.size(); i++)
		{
			int[] curMove = availableMoves.get(i);
			for (int j = 0; j < 2; j++)
			{
				if (curMove[j] != -1)
				{
					int ind = curMove[j] - 2;
					double relativeProgress = (double) (positions[ind] + 1 + lengths[ind]) * lengths[lengths.length / 2] / lengths[ind];
					values[i] += relativeProgress;
				}
			}
		}

		int maxIndex = 0;

		for (int i = 1; i < availableMoves.size(); i++)
		{
			if (values[maxIndex] < values[i]) maxIndex = i;
		}
		return maxIndex;
	}

	/**
	 * Considers 1-relative progress, 2- not stepping on someone (via expected
	 * progress)
	 * 
	 * @return index of the preferred move in availableMoves
	 */
	public int decideOnPairWithExpectedProgress(int player, List<int[]> availableMoves, List<Integer> placeholders, int[][] board, int[] startingPos)
	{
		double[] values = new double[availableMoves.size()];

		for (int i = 0; i < availableMoves.size(); i++)
		{
			int[] curMove = availableMoves.get(i);
			for (int j = 0; j < 2; j++)
			{
				if (curMove[j] != -1)
				{
					int ind = curMove[j] - 2;
					int progressSoFar = board[player][ind] + lengths[ind];
					// For not going on unlikely columns
					/*
					 * double probOfFinishing = 0;
					 * 
					 * for (int p = 1; p < board.length; p++) { int
					 * progressOfOther = board[p][ind] + lengths[ind]; if
					 * (progressOfOther > progressSoFar) {
					 * //System.out.print("somebody else: " + progressOfOther +
					 * ", "); double temp = 0; temp = Math.pow(((double)
					 * probs[ind]) / permutations, lengths[ind] -
					 * progressOfOther); if (temp > probOfFinishing)
					 * probOfFinishing = temp; } }
					 */

					double relativeProgress = ((double) progressSoFar) * lengths[lengths.length / 2] / lengths[ind];
					values[i] += relativeProgress; // * (1 - probOfFinishing);

					int expProg = calculateExpectedProgress(placeholders, curMove[j]);
					int curProg = board[player][ind] - startingPos[ind];
					int diff = expProg - curProg;
					if (board[player][ind] + diff > 0)
					{
						diff = -board[player][ind];
					}
					double prob = Math.pow(((double) probs[ind] / permutations), diff);
					int cur = board[0][ind] + diff;
					for (int p = 1; p < board.length; p++)
					{
						while (cur == board[p][ind])
						{
							prob *= (double) probs[ind] / permutations;
							expProg++;
							cur++;
						}
					}

					double relativeExpProg = ((double) diff) * lengths[lengths.length / 2] / lengths[ind];
					values[i] += prob * relativeExpProg; // * (1 -
															// probOfFinishing);
				}
			}
		}

		int maxIndex = 0;
		for (int i = 1; i < availableMoves.size(); i++)
		{
			if (values[maxIndex] < values[i]) maxIndex = i;
		}
		return maxIndex;
	}

	/**
	 * Considers 1-relative progress 2-not stepping on somebody 3-likelihood of
	 * winning that column
	 * 
	 * @return index of the preferred move
	 */
	public int decideOnPairWithLikelihoodOfWinning(int player, List<int[]> availableMoves, List<Integer> placeholders, int[][] board, int[] startingPos)
	{
		double[] values = new double[availableMoves.size()];

		for (int i = 0; i < availableMoves.size(); i++)
		{
			int[] curMove = availableMoves.get(i);
			for (int j = 0; j < 2; j++)
			{
				if (curMove[j] != -1)
				{
					int ind = curMove[j] - 2;
					int progressSoFar = board[player][ind] + lengths[ind];
					int newProg = progressSoFar + 1;
					int progressOfOther = 0;
					for (int p = 1; p < board.length; p++)
					{
						int temp = board[p][ind] + lengths[ind];
						if (temp > progressOfOther) progressOfOther = temp;
					}

					double relativeProg = ((double) progressSoFar) * lengths[lengths.length / 2] / lengths[ind];
					double likelihoodOfWinning = 0.5 + (newProg - progressOfOther) * 1.0 / lengths[ind];
					values[i] += relativeProg * likelihoodOfWinning;

					int expProg = calculateExpectedProgress(placeholders, curMove[j]);
					int curProg = board[player][ind] - startingPos[ind];
					int diff = expProg - curProg;
					if (board[0][ind] + diff > 0)
					{
						diff = -board[player][ind];
					}

					double prob = Math.pow(((double) probs[ind] / permutations), diff);
					int cur = board[player][ind] + diff;
					for (int p = 1; p < board.length; p++)
					{
						while (cur == board[p][ind])
						{
							prob *= (double) probs[ind] / permutations;
							expProg++;
							cur++;
						}
					}

					double relativeExpProgress = diff * 1.0 * lengths[lengths.length / 2] / lengths[ind];
					values[i] += prob * relativeExpProgress * likelihoodOfWinning;
				}
			}
		}

		int maxIndex = 0;

		for (int i = 1; i < availableMoves.size(); i++)
		{
			if (values[maxIndex] < values[i]) maxIndex = i;
		}

		return maxIndex;
	}

	/**
	 * @see DecisionMaker#decideOnPair(java.util.List, java.util.List, int[][],
	 *      int[]) The decideOnPair method called from game
	 * 
	 */
	public int decideOnPair(int player, List<int[]> availableMoves, List<Integer> placeholders, int[][] board, int[] startingPos)
	{
		if (!decidingPairIsMonteCarlo)// set false in the version using rollouts
		{
			return decideOnPairOriginal(player, availableMoves, board, startingPos);
		}
		else
		{
			int[] values = new int[availableMoves.size()];
			RollOut rout = new RollOut(availableMoves, placeholders, startingPos, board, decidingPairTimes);

			values = rout.winCounts;

			/*for (int m = 0; m < availableMoves.size(); m++)
			{
				System.out.println(Arrays.toString(availableMoves.get(m)) + " : " + values[m]);
			}*/

			int maxIndex = 0;
			for (int i = 1; i < availableMoves.size(); i++)
			{
				if (values[maxIndex] < values[i]) maxIndex = i;
			}
			return maxIndex;
		}

	}

	// SHOULD ROLL METHODS
	public boolean shouldRollWithForcedRollingNearTheEndOfAColumn(int player, int[][] board, List<Integer> placeholders, int[] startingPositions)
	{
		int col1 = 0, col2 = 0, col3 = 0, c1 = 0, c2 = 0, c3 = 0;
		boolean won1, won2, won3;
		boolean col1free = false, col2free = false, col3free = false;
		if (placeholders.size() > 0)
		{
			col1 = placeholders.get(0);
			c1 = board[player][col1 - 2] - startingPositions[col1 - 2];
			won1 = (board[player][col1 - 2] == 0);
		}
		else
		{
			col1free = true;
			won1 = false;
		}
		if (placeholders.size() > 1)
		{
			col2 = placeholders.get(1);
			c2 = board[player][col2 - 2] - startingPositions[col2 - 2];
			won2 = (board[player][col2 - 2] == 0);
		}
		else
		{
			col2free = true;
			won2 = false;
		}
		if (placeholders.size() > 2)
		{
			col3 = placeholders.get(2);
			c3 = board[player][col3 - 2] - startingPositions[col3 - 2];
			won3 = (board[player][col3 - 2] == 0);
		}
		else
		{
			col3free = true;
			won3 = false;
		}

		if (won1 && won2 && won3)
		{
			return false;
		}
		// on all 3 columns, none won
		else if ((!col1free) && (!col2free) && (!col3free) && (!won1) && (!won2) && (!won3))
		{
			int curDist1 = board[0][col1 - 2] * -1;
			int curDist2 = board[0][col2 - 2] * -1;
			int curDist3 = board[0][col3 - 2] * -1;

			int minProg1 = 13;
			int minProg2 = 13;
			int minProg3 = 13;

			for (int i = 1; i < board.length; i++)
			{
				if (board[i][col1 - 2] * -1 < minProg1) minProg1 = board[i][col1 - 2] * -1;
				if (board[i][col2 - 2] * -1 < minProg2) minProg2 = board[i][col2 - 2] * -1;
				if (board[i][col3 - 2] * -1 < minProg3) minProg3 = board[i][col3 - 2] * -1;
			}

			List<Integer> p = new ArrayList<Integer>();

			int expProg1 = calculateExpectedProgress(p, col1);
			int expProg2 = calculateExpectedProgress(p, col2);
			int expProg3 = calculateExpectedProgress(p, col3);

			/*
			 * System.out.println("1: " + curDist1 + " " + minProg1 + " " +
			 * expProg1); System.out.println("2: " + curDist2 + " " + minProg2 +
			 * " " + expProg2); System.out.println("3: " + curDist3 + " " +
			 * minProg3 + " " + expProg3);
			 */

			if (minProg1 != -1 && minProg1 < expProg1 && curDist1 < expProg1)
			{
				return true;
			}
			if (minProg2 != -1 && minProg2 < expProg2 && curDist2 < expProg2)
			{
				return true;
			}

			if (minProg3 != -1 && minProg3 < expProg3 && curDist3 < expProg3)
			{
				return true;
			}

			return shouldRollProgProgProg(col1, col2, col3, c1, c2, c3);
		}
		// 1 col won, 2 col in progress
		else if (won1 && (!col2free) && (!col3free))
		{
			return shouldRollProgProgWon(col2, col3, col1, c2, c3, c1);
		}
		else if (won2 && (!col1free) && (!col3free))
		{
			return shouldRollProgProgWon(col1, col3, col2, c1, c3, c2);
		}
		else if (won3 && (!col2free) && (!col1free))
		{
			return shouldRollProgProgWon(col1, col2, col3, c1, c2, c3);
		}
		// on 2 columns, other is free
		else if (!col1free && !col2free && col3free && !won1 && !won2)
		{
			return shouldRollProgProgFree(col1, col2, c1, c2);
		}
		else if (!col2free && !col3free && col2free && !won2 && !won3)
		{
			return shouldRollProgProgFree(col2, col3, c2, c3);
		}
		else if (!col1free && !col3free && col2free && !won1 && !won3)
		{
			return shouldRollProgProgFree(col1, col3, c1, c3);
		}
		// 1 col, 2 won
		else if (!col1free && won2 && won3)
		{
			return shouldRollProgWonWon(col1, col2, col3, c1, c2, c3);
		}
		else if (!col2free && won1 && won3)
		{
			return shouldRollProgWonWon(col2, col1, col3, c2, c1, c3);
		}
		else if (!col3free && won2 && won1)
		{
			return shouldRollProgWonWon(col3, col1, col2, c3, c1, c2);
		}
		// 1 col, 1 won, 1 free
		else if (!col1free && col2free && won3)
		{
			return shouldRollProgWonFree(col1, col3, c1, c3);
		}
		else if (!col1free && col3free && won2)
		{
			return shouldRollProgWonFree(col1, col2, c1, c2);
		}
		else if (!col2free && col1free && won3)
		{
			return shouldRollProgWonFree(col2, col3, c2, c3);
		}
		else if (!col2free && col3free && won1)
		{
			return shouldRollProgWonFree(col2, col1, c2, c1);
		}
		else if (!col3free && col1free && won2)
		{
			return shouldRollProgWonFree(col3, col2, c3, c2);
		}
		else if (!col3free && col2free && won1)
		{
			return shouldRollProgWonFree(col3, col1, c3, c1);
		}
		// 1 col, 2 free
		else if (!col1free && col2free && col3free)
		{
			return shouldRollProgFreeFree(col1, c1);
		}
		else if (!col2free && col1free && col3free)
		{
			return shouldRollProgFreeFree(col2, c2);
		}
		else if (!col3free && col2free && col1free)
		{
			return shouldRollProgFreeFree(col3, c3);
		}
		// 1 free, 2 won
		else if (won1 && won2 && col3free)
		{
			return shouldRollWonWonFree(col1, col2, c1, c2);
		}
		else if (won1 && won3 && col2free)
		{
			return shouldRollWonWonFree(col1, col3, c1, c3);
		}
		else if (won2 && won3 && col3free)
		{
			return shouldRollWonWonFree(col2, col3, c2, c3);
		}
		// 1 won, 2 free
		else if (won1 && col2free && col3free)
		{
			return shouldRollWonFreeFree(col1, c1);
		}
		else if (won2 && col1free && col3free)
		{
			return shouldRollWonFreeFree(col2, c2);
		}
		else if (won3 && col1free && col2free)
		{
			return shouldRollWonFreeFree(col3, c3);
		}
		else
		{
			System.out.println("there is an error is the distribution of shouldRoll, there is a case not taken care of.");
			return false;
		}

	}

	public boolean shouldRollOriginal(int player, int[][] board, List<Integer> placeholders, int[] startingPositions)
	{
		int col1 = 0, col2 = 0, col3 = 0, c1 = 0, c2 = 0, c3 = 0;
		boolean won1, won2, won3;
		boolean col1free = false, col2free = false, col3free = false;
		if (placeholders.size() > 0)
		{
			col1 = placeholders.get(0);
			c1 = board[player][col1 - 2] - startingPositions[col1 - 2];
			won1 = (board[player][col1 - 2] == 0);
		}
		else
		{
			col1free = true;
			won1 = false;
		}
		if (placeholders.size() > 1)
		{
			col2 = placeholders.get(1);
			c2 = board[player][col2 - 2] - startingPositions[col2 - 2];
			won2 = (board[player][col2 - 2] == 0);
		}
		else
		{
			col2free = true;
			won2 = false;
		}
		if (placeholders.size() > 2)
		{
			col3 = placeholders.get(2);
			c3 = board[player][col3 - 2] - startingPositions[col3 - 2];
			won3 = (board[player][col3 - 2] == 0);
		}
		else
		{
			col3free = true;
			won3 = false;
		}

		if (won1 && won2 && won3)
		{
			return false;
		}
		// on all 3 columns, none won
		else if ((!col1free) && (!col2free) && (!col3free) && (!won1) && (!won2) && (!won3))
		{
			int curDist1 = board[0][col1 - 2] * -1;
			int curDist2 = board[0][col2 - 2] * -1;
			int curDist3 = board[0][col3 - 2] * -1;

			int minProg1 = 13;
			int minProg2 = 13;
			int minProg3 = 13;

			for (int i = 1; i < board.length; i++)
			{
				if (board[i][col1 - 2] * -1 < minProg1) minProg1 = board[i][col1 - 2] * -1;
				if (board[i][col2 - 2] * -1 < minProg2) minProg2 = board[i][col2 - 2] * -1;
				if (board[i][col3 - 2] * -1 < minProg3) minProg3 = board[i][col3 - 2] * -1;
			}

			List<Integer> p = new ArrayList<Integer>();

			int expProg1 = calculateExpectedProgress(p, col1);
			int expProg2 = calculateExpectedProgress(p, col2);
			int expProg3 = calculateExpectedProgress(p, col3);

			//System.out.println("1: " + curDist1 + " " + minProg1 + " " + expProg1);
			//System.out.println("2: " + curDist2 + " " + minProg2 + " " + expProg2);
			//System.out.println("3: " + curDist3 + " " + minProg3 + " " + expProg3);

			if (minProg1 != -1 && minProg1 < expProg1 && curDist1 < expProg1)
			{
				return true;
			}
			if (minProg2 != -1 && minProg2 < expProg2 && curDist2 < expProg2)
			{
				return true;
			}

			if (minProg3 != -1 && minProg3 < expProg3 && curDist3 < expProg3)
			{
				return true;
			}

			return shouldRollProgProgProg(col1, col2, col3, c1, c2, c3);
		}
		// 1 col won, 2 col in progress
		else if (won1 && (!col2free) && (!col3free))
		{
			return shouldRollProgProgWon(col2, col3, col1, c2, c3, c1);
		}
		else if (won2 && (!col1free) && (!col3free))
		{
			return shouldRollProgProgWon(col1, col3, col2, c1, c3, c2);
		}
		else if (won3 && (!col2free) && (!col1free))
		{
			return shouldRollProgProgWon(col1, col2, col3, c1, c2, c3);
		}
		// on 2 columns, other is free
		else if (!col1free && !col2free && col3free && !won1 && !won2)
		{
			return shouldRollProgProgFree(col1, col2, c1, c2);
		}
		else if (!col2free && !col3free && col2free && !won2 && !won3)
		{
			return shouldRollProgProgFree(col2, col3, c2, c3);
		}
		else if (!col1free && !col3free && col2free && !won1 && !won3)
		{
			return shouldRollProgProgFree(col1, col3, c1, c3);
		}
		// 1 col, 2 won
		else if (!col1free && won2 && won3)
		{
			return shouldRollProgWonWon(col1, col2, col3, c1, c2, c3);
		}
		else if (!col2free && won1 && won3)
		{
			return shouldRollProgWonWon(col2, col1, col3, c2, c1, c3);
		}
		else if (!col3free && won2 && won1)
		{
			return shouldRollProgWonWon(col3, col1, col2, c3, c1, c2);
		}
		// 1 col, 1 won, 1 free
		else if (!col1free && col2free && won3)
		{
			return shouldRollProgWonFree(col1, col3, c1, c3);
		}
		else if (!col1free && col3free && won2)
		{
			return shouldRollProgWonFree(col1, col2, c1, c2);
		}
		else if (!col2free && col1free && won3)
		{
			return shouldRollProgWonFree(col2, col3, c2, c3);
		}
		else if (!col2free && col3free && won1)
		{
			return shouldRollProgWonFree(col2, col1, c2, c1);
		}
		else if (!col3free && col1free && won2)
		{
			return shouldRollProgWonFree(col3, col2, c3, c2);
		}
		else if (!col3free && col2free && won1)
		{
			return shouldRollProgWonFree(col3, col1, c3, c1);
		}
		// 1 col, 2 free
		else if (!col1free && col2free && col3free)
		{
			return shouldRollProgFreeFree(col1, c1);
		}
		else if (!col2free && col1free && col3free)
		{
			return shouldRollProgFreeFree(col2, c2);
		}
		else if (!col3free && col2free && col1free)
		{
			return shouldRollProgFreeFree(col3, c3);
		}
		// 1 free, 2 won
		else if (won1 && won2 && col3free)
		{
			return shouldRollWonWonFree(col1, col2, c1, c2);
		}
		else if (won1 && won3 && col2free)
		{
			return shouldRollWonWonFree(col1, col3, c1, c3);
		}
		else if (won2 && won3 && col3free)
		{
			return shouldRollWonWonFree(col2, col3, c2, c3);
		}
		// 1 won, 2 free
		else if (won1 && col2free && col3free)
		{
			return shouldRollWonFreeFree(col1, c1);
		}
		else if (won2 && col1free && col3free)
		{
			return shouldRollWonFreeFree(col2, c2);
		}
		else if (won3 && col1free && col2free)
		{
			return shouldRollWonFreeFree(col3, c3);
		}
		else
		{
			System.out.println("there is an error is the distribution of shouldRoll, there is a case not taken care of.");
			return false;
		}
	}

	public boolean shouldRoll(int player, int[][] board, List<Integer> placeholders, int[] startingPositions)
	{
		if (!shouldRollIsMonteCarlo)
		{
			return shouldRollOriginal(player, board, placeholders, startingPositions);
		}
		else
		{
			RollOut rout = new RollOut(null, placeholders, startingPositions, board, shouldRollTimes);

			System.out.println("should roll: " + rout.winCountIfRoll + " " + rout.winCountIfStop);

			return rout.winCountIfRoll > rout.winCountIfStop;
		}

	}

	//VARIOUS UTILITY METHODS FOR SHOULD ROLL AND DECIDE ON PAIR
	/**
	 * Given a list of placeholders and a column i, calculate the expected
	 * progress on that column. If a partially filled placeholders list is
	 * given, calculate average expected progresses considering all possible
	 * options
	 * 
	 * @return the ceiling of the expected progress value
	 */
	private int calculateExpectedProgress(List<Integer> placeholders, int i)
	{
		List<Integer> p = new ArrayList<Integer>(placeholders);
		if (p.size() == 3 || (p.size() == 2 && p.indexOf(i) == -1))
		{
			int col1 = p.get(0);
			int col2 = p.get(1);
			int col3;
			if (p.size() == 3) col3 = p.get(2);
			else
			{
				p.add(i);
				col3 = p.get(2);
			}

			int[] prob = calculateProbabilityOfNotMissing(col1, col2, col3);
			double c = calculateC(prob[0], prob[1]);
			int w1 = probs[col1 - 2];
			int w2 = probs[col2 - 2];
			int w3 = probs[col3 - 2];
			int ind = p.indexOf(i);

			if (ind == 0)
			{
				return (int) Math.ceil(c * ((double) w1 / (w1 + w2 + w3)));
			}
			else if (ind == 1)
			{
				return (int) Math.ceil(c * ((double) w2 / (w1 + w2 + w3)));
			}
			else return (int) Math.ceil(c * ((double) w3 / (w1 + w2 + w3)));
		}
		else if ((p.size() == 2 && p.indexOf(i) != -1) || (p.size() == 1 && p.indexOf(i) == -1))
		{
			int col1 = p.get(0);
			int col2;
			if (p.size() == 2) col2 = p.get(1);
			else
			{
				p.add(i);
				col2 = p.get(1);
			}

			double sum = 0;
			int count = 0;
			int ind = p.indexOf(i);

			for (int col3 = 2; col3 <= 12; col3++)
			{
				if (col3 != col1 && col3 != col2)
				{
					int[] prob = calculateProbabilityOfNotMissing(col1, col2, col3);
					double c = calculateC(prob[0], prob[1]);
					int w1 = probs[col1 - 2];
					int w2 = probs[col2 - 2];
					int w3 = probs[col3 - 2];
					if (ind == 0)
					{
						sum += c * ((double) w1 / (w1 + w2 + w3));
					}
					else
					{
						sum += c * ((double) w2 / (w1 + w2 + w3));
					}
					count++;
				}
			}
			return (int) Math.ceil((double) sum / count);
		}
		else
		{
			if (p.size() == 0) p.add(i);
			int col1 = p.get(0);
			double sum = 0;
			int count = 0;
			for (int col2 = 2; col2 <= 12; col2++)
			{
				if (col2 != col1) for (int col3 = col2 + 1; col3 <= 12; col3++)
				{
					if (col3 != col1)
					{
						int[] prob = calculateProbabilityOfNotMissing(col1, col2, col3);
						double c = calculateC(prob[0], prob[1]);
						int w1 = probs[col1 - 2];
						int w2 = probs[col2 - 2];
						int w3 = probs[col3 - 2];
						sum += c * ((double) w1 / (w1 + w2 + w3));
						count++;
					}
				}
			}
			return (int) Math.ceil((double) sum / count);
		}
	}

	/**
	 * Given three valid columns calculate the probability rolling at least one
	 * of those columns @return[0] probability of rolling something resulting in
	 * at least one unit progress @return[1] probability of rolling something
	 * resulting in two unit progress
	 */
	private int[] calculateProbabilityOfNotMissing(int col1, int col2, int col3)
	{
		int workingCombos = 0;
		int combosWith2 = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithThreeColumns(diceCombos.get(i), col1, col2, col3);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);
			boolean somethingEqualsSecond = (pair[0] == col2) || (pair[1] == col2);
			boolean somethingEqualsThird = (pair[0] == col3) || (pair[1] == col3);

			if (somethingEqualsFirst || somethingEqualsSecond || somethingEqualsThird)
			{
				int sumOfPair = pair[0] + pair[1];
				if ((somethingEqualsFirst && somethingEqualsSecond) || (somethingEqualsFirst && somethingEqualsThird) || (somethingEqualsSecond && somethingEqualsThird)
						|| (somethingEqualsFirst && sumOfPair == 2 * col1) || (somethingEqualsSecond && sumOfPair == 2 * col2) || (somethingEqualsThird && sumOfPair == 2 * col3))
				{
					combosWith2 += frequencies.get(i);
				}
				workingCombos += frequencies.get(i);
			}
		}
		int[] ret = { workingCombos, combosWith2 };
		return ret;
	}

	/**
	 * Given the values for rolls that result in making progress, calculate the
	 * expected total progress on the columns
	 * 
	 * @return
	 */
	private double calculateC(int countOfMakingProgress, int countOf2)
	{
		double countOf1 = countOfMakingProgress - countOf2;
		double c = ((double) countOf1 / permutations + (double) 2 * countOf2 / permutations) / (1 - (double) countOf1 / permutations - (double) countOf2 / permutations);
		return c;
	}

	private boolean shouldRollWonFreeFree(int col1, int c1)
	{
		int p1 = calcProbOfHittingFirstWithOneColumn(col1);

		int p11;
		if (col1 % 2 == 0) p11 = 0;
		else p11 = 1;

		long leftSide = p1 * (avgProb * c1 + probs[col1 - 2]) + (permutations - p11) * (avgProb * c1 + probs[col1 - 2] * 2);
		long rightSide = c1 * permutations;
		return leftSide > rightSide;
	}

	private boolean shouldRollProgFreeFree(int col1, long c1)
	{

		int p1 = calcProbOfHittingFirstWithOneColumn(col1);
		int p11 = calcProbOfHittingFirstTwiceWithOneColumn(col1);

		long leftSide = p1 * (avgProb * (c1 + 1) + probs[col1 - 2]) + p11 * (c1 + 2) + (permutations - p1 - p11) * (avgProb * c1 + probs[col1 - 2] * 2);

		long rightSide = c1 * permutations;

		return leftSide > rightSide;
	}

	private boolean shouldRollProgWonFree(int col1, int col2, long c1, long c2)
	{
		int p1 = calcProbOfHittingFirstWithOneColumn(col1);
		int p11 = calcProbOfHittingFirstTwiceWithOneColumn(col1);

		int w1 = probs[col2 - 2] * avgProb;
		int w2 = probs[col1 - 2] * avgProb;
		int wa = probs[col1 - 2] * probs[col2 - 2];

		long leftSide = p1 * (w1 * (c1 + 1) + w2 * c2) + p11 * (w1 * (c1 + 2) + w2 * c2) + (permutations - p1 - p11) * (w1 * c1 + w2 * c2 + wa * 2);

		long rightSide = (c1 * w1 + c2 * w2) * permutations;

		return leftSide > rightSide;
	}

	private boolean shouldRollProgWonWon(int col1, int col2, int col3, long c1, long c2, long c3)
	{
		int p1 = calcProbOfHittingFirstWithOneColumn(col1);
		int p11 = calcProbOfHittingFirstTwiceWithOneColumn(col1);
		int w1 = probs[col2 - 2] * probs[col3 - 2];
		int w2 = probs[col1 - 2] * probs[col3 - 2];
		int w3 = probs[col1 - 2] * probs[col2 - 2];

		long leftSide = p1 * (w1 * (c1 + 1) + w2 * c2 + w3 * c3) + p11 * (w1 * (c1 + 2) + w2 * c2 + w3 * c3);

		long rightSide = (c1 * w1 + c2 * w2 + c3 * w3) * permutations;

		return leftSide > rightSide;
	}

	private int calcProbOfHittingFirstWithOneColumn(int col1)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithOneColumn(diceCombos.get(i), col1);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);

			if (somethingEqualsFirst)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;

				if (remaining != col1)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	private int calcProbOfHittingFirstTwiceWithOneColumn(int col1)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithOneColumn(diceCombos.get(i), col1);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);

			if (somethingEqualsFirst)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;
				if (remaining == col1)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	private int[] pairDeciderWithOneColumn(int[] roll, int col1)
	{
		List<int[]> moves = getPossiblePairings(roll);
		for (int i = 0; i < moves.size(); i++)
		{
			int[] curMove = moves.get(i);
			if (!(curMove[0] == col1 || curMove[1] == col1)) moves.remove(i);
		}
		if (moves.size() == 0)
		{
			int[] ret = { -1, -1 };
			return ret;
		}

		int[] progress = new int[moves.size()];
		for (int i = 0; i < moves.size(); i++)
		{
			int[] curMove = moves.get(i);
			int prog = 0;
			for (int j = 0; j < 2; j++)
			{
				if (curMove[j] == col1) prog++;
			}
			progress[i] = prog;
		}

		int selectedIndex = 0;
		for (int i = 1; i < progress.length; i++)
		{
			if (progress[i] > progress[selectedIndex]) selectedIndex = i;
		}

		return moves.get(selectedIndex);
	}

	private boolean shouldRollWonWonFree(int col1, int col2, long c1, long c2)
	{
		int p1 = calcProbOfHittingFirstWithTwoColumns(col1, col2);
		int p11;
		if (col1 % 2 == 0) p11 = 0;
		else p11 = 1;
		int p2 = calcProbOfHittingFirstWithTwoColumns(col2, col1);
		int p22;
		if (col1 % 2 == 0) p22 = 0;
		else p22 = 1;
		int p12 = calcProbOfHittingFirstAndSecondWithTwoColumns(col1, col2);

		int w1 = probs[col2 - 2] * avgProb;
		int w2 = probs[col1 - 2] * avgProb;
		int wa = probs[col1 - 2] * probs[col2 - 2];

		long leftSide = p1 * (w1 * c1 + w2 * c2 + wa) + p2 * (w1 * c1 + w2 * c2 + wa) + (permutations - p11 - p22 - p12) * (w1 * c1 + w2 * c2 + wa * 2);
		long rightSide = (c1 * w1 + c2 * w2) * permutations;
		return leftSide > rightSide;
	}

	private boolean shouldRollProgProgFree(int col1, int col2, long c1, long c2)
	{
		int p1 = calcProbOfHittingFirstWithTwoColumns(col1, col2);
		int p2 = calcProbOfHittingFirstWithTwoColumns(col2, col1);
		int p12 = calcProbOfHittingFirstAndSecondWithTwoColumns(col1, col2);
		int p11 = calcProbOfHittingFirstTwiceWithTwoColumns(col1, col2);
		int p22 = calcProbOfHittingFirstTwiceWithTwoColumns(col2, col1);

		int w1 = probs[col2 - 2] * avgProb;
		int w2 = probs[col1 - 2] * avgProb;
		int wa = probs[col1 - 2] * probs[col2 - 2];

		long leftSide = p1 * (w1 * (c1 + 1) + w2 * c2 + wa) + p2 * (w1 * c1 + w2 * (c2 + 1) + wa) + p12 * (w1 * (c1 + 1) + w2 * (c2 + 1)) + p11 * (w1 * (c1 + 2) + w2 * c2) + p22
				* (w1 * c1 + w2 * (c2 + 2)) + (permutations - p1 - p2 - p12 - p11 - p22) * (w1 * c1 + w2 * c2 + wa * 2);

		long rightSide = (c1 * w1 + c2 * w2) * permutations;

		return leftSide > rightSide;
	}

	private boolean shouldRollProgProgWon(int col1, int col2, int col3, long c1, long c2, long c3)
	{
		int p1 = calcProbOfHittingFirstWithTwoColumns(col1, col2);
		int p2 = calcProbOfHittingFirstWithTwoColumns(col2, col1);
		int p12 = calcProbOfHittingFirstAndSecondWithTwoColumns(col1, col2);
		int p11 = calcProbOfHittingFirstTwiceWithTwoColumns(col1, col2);
		int p22 = calcProbOfHittingFirstTwiceWithTwoColumns(col2, col1);

		int w1 = probs[col2 - 2] * probs[col3 - 2];
		int w2 = probs[col1 - 2] * probs[col3 - 2];
		int w3 = probs[col1 - 2] * probs[col2 - 2];

		long leftSide = p1 * (w1 * (c1 + 1) + w2 * c2 + w3 * c3) + p2 * (w1 * c1 + w2 * (c2 + 1) + w3 * c3) + p12 * (w1 * (c1 + 1) + w2 * (c2 + 1) + w3 * c3) + p11
				* (w1 * (c1 + 2) + w2 * c2 + w3 * c3) + p22 * (w1 * c1 + w2 * (c2 + 2) + w3 * c3);

		long rightSide = (c1 * w1 + c2 * w2 + c3 * w3) * permutations;

		// System.out.println("2m lh: " + leftSide + "\n2m rh: " + rightSide);

		return leftSide > rightSide;
	}

	private int calcProbOfHittingFirstWithTwoColumns(int col1, int col2)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithTwoColumns(diceCombos.get(i), col1, col2);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);

			if (somethingEqualsFirst)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;

				if (remaining != col1 && remaining != col2)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	private int calcProbOfHittingFirstAndSecondWithTwoColumns(int col1, int col2)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithTwoColumns(diceCombos.get(i), col1, col2);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);
			boolean somethingEqualsSecond = (pair[0] == col2) || (pair[1] == col2);

			if (somethingEqualsFirst && somethingEqualsSecond)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;
				if (remaining == col2)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	private int calcProbOfHittingFirstTwiceWithTwoColumns(int col1, int col2)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithTwoColumns(diceCombos.get(i), col1, col2);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);

			if (somethingEqualsFirst)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;
				if (remaining == col1)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	private int[] pairDeciderWithTwoColumns(int[] roll, int col1, int col2)
	{
		List<int[]> moves = getPossiblePairings(roll);
		for (int i = 0; i < moves.size(); i++)
		{
			int[] curMove = moves.get(i);
			if (!(curMove[0] == col1 || curMove[0] == col2 || curMove[1] == col1 || curMove[1] == col2)) moves.remove(i);
		}
		if (moves.size() == 0)
		{
			int[] ret = { -1, -1 };
			return ret;
		}

		int w1 = probs[col2 - 2];
		int w2 = probs[col1 - 2];

		int[] progress = new int[moves.size()];
		for (int i = 0; i < moves.size(); i++)
		{
			int[] curMove = moves.get(i);
			int prog = 0;
			for (int j = 0; j < 2; j++)
			{
				if (curMove[j] == col1) prog += w1;
				else if (curMove[j] == col2) prog += w2;
			}
			progress[i] = prog;
		}

		int selectedIndex = 0;
		for (int i = 1; i < progress.length; i++)
		{
			if (progress[i] > progress[selectedIndex]) selectedIndex = i;
		}

		return moves.get(selectedIndex);
	}

	public boolean shouldRollProgProgProg(int col1, int col2, int col3, long c1, long c2, long c3)
	{
		int p1 = calcProbOfHittingFirstWithThreeColumns(col1, col2, col3);
		int p2 = calcProbOfHittingFirstWithThreeColumns(col2, col1, col3);
		int p3 = calcProbOfHittingFirstWithThreeColumns(col3, col1, col2);
		int p12 = calcProbOfHittingFirstAndSecondWithThreeColumns(col1, col2, col3);
		int p13 = calcProbOfHittingFirstAndSecondWithThreeColumns(col1, col3, col2);
		int p23 = calcProbOfHittingFirstAndSecondWithThreeColumns(col2, col3, col1);
		int p11 = calcProbOfHittingFirstTwiceWithThreeColumns(col1, col2, col3);
		int p22 = calcProbOfHittingFirstTwiceWithThreeColumns(col2, col1, col3);
		int p33 = calcProbOfHittingFirstTwiceWithThreeColumns(col3, col2, col1);

		int w1 = probs[col2 - 2] * probs[col3 - 2];
		int w2 = probs[col1 - 2] * probs[col3 - 2];
		int w3 = probs[col1 - 2] * probs[col2 - 2];

		long leftSide = p1 * (w1 * (c1 + 1) + w2 * c2 + w3 * c3) + p2 * (w1 * c1 + w2 * (c2 + 1) + w3 * c3) + p3 * (w1 * c1 + w2 * c2 + w3 * (c3 + 1)) + p12
				* (w1 * (c1 + 1) + w2 * (c2 + 1) + w3 * c3) + p13 * (w1 * (c1 + 1) + w2 * c2 + w3 * (c3 + 1)) + p23 * (w1 * c1 + w2 * (c2 + 1) + w3 * (c3 + 1)) + p11
				* (w1 * (c1 + 2) + w2 * c2 + w3 * c3) + p22 * (w1 * c1 + w2 * (c2 + 2) + w3 * c3) + p33 * (w1 * c1 + w2 * c2 + w3 * (c3 + 2));

		long rightSide = (c1 * w1 + c2 * w2 + c3 * w3) * permutations;

		// System.out.println("3 lh: " + leftSide + "\n3 rh: " + rightSide);

		return leftSide > rightSide;
	}

	private int calcProbOfHittingFirstWithThreeColumns(int col1, int col2, int col3)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithThreeColumns(diceCombos.get(i), col1, col2, col3);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);

			if (somethingEqualsFirst)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;

				if (remaining != col1 && remaining != col2 && remaining != col3)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	private int calcProbOfHittingFirstAndSecondWithThreeColumns(int col1, int col2, int col3)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithThreeColumns(diceCombos.get(i), col1, col2, col3);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);
			boolean somethingEqualsSecond = (pair[0] == col2) || (pair[1] == col2);

			if (somethingEqualsFirst && somethingEqualsSecond)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;
				if (remaining == col2)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	private int calcProbOfHittingFirstTwiceWithThreeColumns(int col1, int col2, int col3)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDeciderWithThreeColumns(diceCombos.get(i), col1, col2, col3);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);

			if (somethingEqualsFirst)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;
				if (remaining == col1)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	private int[] pairDeciderWithThreeColumns(int[] roll, int col1, int col2, int col3)
	{
		List<int[]> moves = getPossiblePairings(roll);
		for (int i = 0; i < moves.size(); i++)
		{
			int[] curMove = moves.get(i);
			if (!(curMove[0] == col1 || curMove[0] == col2 || curMove[0] == col3 || curMove[1] == col1 || curMove[1] == col2 || curMove[1] == col3)) moves.remove(i);
		}
		if (moves.size() == 0)
		{
			int[] ret = { -1, -1 };
			return ret;
		}

		int w1 = probs[col2 - 2] * probs[col3 - 2];
		int w2 = probs[col1 - 2] * probs[col3 - 2];
		int w3 = probs[col1 - 2] * probs[col2 - 2];

		int[] progress = new int[moves.size()];
		for (int i = 0; i < moves.size(); i++)
		{
			int[] curMove = moves.get(i);
			int prog = 0;
			for (int j = 0; j < 2; j++)
			{
				if (curMove[j] == col1) prog += w1;
				else if (curMove[j] == col2) prog += w2;
				else if (curMove[j] == col3) prog += w3;
			}
			progress[i] = prog;
		}

		int selectedIndex = 0;
		for (int i = 1; i < progress.length; i++)
		{
			if (progress[i] > progress[selectedIndex]) selectedIndex = i;
		}

		return moves.get(selectedIndex);
	}

	// MAIN CLASS
	public static void main(String[] args) throws FileNotFoundException
	{
		ProbabilisticDecisionMaker calc = new ProbabilisticDecisionMaker();

		int col1 = 6, col2 = 7, col3 = 8;
		int c1 = 8, c2 = 2, c3 = 7;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 8; c2 = 3; c3 = 6;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 8; c2 = 4; c3 = 5;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 8; c2 = 5; c3 = 4;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 8; c2 = 6; c3 = 4;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 4; c2 = 7; c3 = 6;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 3; c2 = 8; c3 = 7;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 8; c2 = 9; c3 = 0;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 7; c2 = 10; c3 = 1;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
		c1 = 7; c2 = 11; c3 = 0;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
				c1 = 7; c2 = 11; c3 = 0;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
						c1 = 6; c2 = 12; c3 = 0;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
						c1 = 4; c2 = 14; c3 = 0;
		System.out.println(calc.shouldRollProgProgProg(col1, col2, col3, c1, c2, c3));
	}

	@Deprecated
	public boolean shouldRollExp(int col1, int col2, int col3, long c1, long c2, long c3)
	{
		int p1 = calculateProbabilityOfHittingFirst(col1, col2, col3);
		int p2 = calculateProbabilityOfHittingFirst(col2, col1, col3);
		int p3 = calculateProbabilityOfHittingFirst(col3, col1, col2);
		int p12 = calculateProbabilityOfHittingFirstAndSecond(col1, col2, col3);
		int p13 = calculateProbabilityOfHittingFirstAndSecond(col1, col3, col2);
		int p23 = calculateProbabilityOfHittingFirstAndSecond(col2, col3, col1);
		int p11 = calculateProbabilityOfNotMissing(col1, -col2, -col3)[1];
		int p22 = calculateProbabilityOfNotMissing(col2, -col1, -col3)[1];
		int p33 = calculateProbabilityOfNotMissing(col3, -col1, -col2)[1];

		int w1 = probs[Math.abs(col2) - 2] * probs[Math.abs(col3) - 2];
		int w2 = probs[Math.abs(col1) - 2] * probs[Math.abs(col3) - 2];
		int w3 = probs[Math.abs(col1) - 2] * probs[Math.abs(col2) - 2];

		// System.out.println("IN SHOULDROLLEXP: " + p1 + " " + p2 + " " + p3 +
		// " "+ p12 + " "+ p23 + " "+ p13 + " "+ p11 + " "+ p22 + " "+ p33 +
		// " "+ w1 + " "+
		// w2 + " " + w3);

		// System.out.println("test: " + pos1[0] + " " + pos2[0] + " " +
		// pos3[0]);

		long leftSide = p1 * (w1 * (c1 + 1) + w2 * c2 + w3 * c3) + p2 * (w1 * c1 + w2 * (c2 + 1) + w3 * c3) + p3 * (w1 * c1 + w2 * c2 + w3 * (c3 + 1)) + p12
				* (w1 * (c1 + 1) + w2 * (c2 + 1) + w3 * c3) + p13 * (w1 * (c1 + 1) + w2 * c2 + w3 * (c3 + 1)) + p23 * (w1 * c1 + w2 * (c2 + 1) + w3 * (c3 + 1)) + p11
				* (w1 * (c1 + 2) + w2 * c2 + w3 * c3) + p22 * (w1 * c1 + w2 * (c2 + 2) + w3 * c3) + p33 * (w1 * c1 + w2 * c2 + w3 * (c3 + 2));

		long rightSide = (c1 * w1 + c2 * w2 + c3 * w3) * permutations;

		// System.out.println("exp edit lh: " + leftSide + "\nexp edit rh: " +
		// rightSide);

		return leftSide > rightSide;
	}

	@Deprecated
	public boolean shouldRollExpOurs(int col1, int col2, int col3, long c1, long c2, long c3)
	{
		int[] p1 = calculateProbabilityOfNotMissing(col1, -col2, -col3);
		int[] p2 = calculateProbabilityOfNotMissing(col2, -col1, -col3);
		int[] p3 = calculateProbabilityOfNotMissing(col3, -col1, -col2);
		long leftHandPart1 = calculateProbabilityOfHittingFirst(col1, col2, col3) * (c1 + 1) + calculateProbabilityOfHittingFirstAndSecond(col1, col2, col3) * (c1 + 1)
				+ calculateProbabilityOfHittingFirstAndSecond(col1, col3, col2) * (c1 + 1) + p1[1] * (c1 + 2) + calculateProbabilityOfMissingThird(col2, col3, col1) * c1;

		long leftHandPart2 = calculateProbabilityOfHittingFirst(col2, col1, col3) * (c2 + 1) + calculateProbabilityOfHittingFirstAndSecond(col2, col1, col3) * (c2 + 1)
				+ calculateProbabilityOfHittingFirstAndSecond(col2, col3, col1) * (c2 + 1) + p2[1] * (c2 + 2) + calculateProbabilityOfMissingThird(col1, col3, col2) * c2;

		long leftHandPart3 = calculateProbabilityOfHittingFirst(col3, col1, col2) * (c3 + 1) + calculateProbabilityOfHittingFirstAndSecond(col3, col1, col2) * (c3 + 1)
				+ calculateProbabilityOfHittingFirstAndSecond(col3, col2, col1) * (c3 + 1) + p3[1] * (c3 + 2) + calculateProbabilityOfMissingThird(col1, col2, col3) * c3;

		long rightHandPart1 = c1 * permutations;
		long rightHandPart2 = c2 * permutations;
		long rightHandPart3 = c3 * permutations;

		int w1 = probs[Math.abs(col2) - 2] * probs[Math.abs(col3) - 2];
		int w2 = probs[Math.abs(col1) - 2] * probs[Math.abs(col3) - 2];
		int w3 = probs[Math.abs(col1) - 2] * probs[Math.abs(col2) - 2];

		//System.out.println("exp org lh: " + (w1 * leftHandPart1 + w2 * leftHandPart2 + w3 * leftHandPart3));
		//System.out.println("exp org rh: " + (w1 * rightHandPart1 + w2 * rightHandPart2 + w3 * rightHandPart3));

		return w1 * leftHandPart1 + w2 * leftHandPart2 + w3 * leftHandPart3 > w1 * rightHandPart1 + w2 * rightHandPart2 + w3 * rightHandPart3;
	}

	@Deprecated
	private int calculateProbabilityOfHittingFirstAndSecond(int col1, int col2, int col3)
	{

		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDecider(diceCombos.get(i), col1, col2, col3);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);
			boolean somethingEqualsSecond = (pair[0] == col2) || (pair[1] == col2);

			if (somethingEqualsFirst && somethingEqualsSecond)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;
				if (remaining == col2)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	@Deprecated
	public int calculateProbabilityOfHittingFirst(int col1, int col2, int col3)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDecider(diceCombos.get(i), col1, col2, col3);

			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);

			if (somethingEqualsFirst)
			{
				int sumOfPair = pair[0] + pair[1];
				int remaining = sumOfPair - col1;

				if (remaining != col1 && remaining != col2 && remaining != col3)
				{
					workingCombos += frequencies.get(i);
				}
			}
		}
		return workingCombos;
	}

	@Deprecated
	public int calculateProbabilityOfMissingThird(int col1, int col2, int col3)
	{
		int workingCombos = 0;
		for (int i = 0; i < diceCombos.size(); i++)
		{
			int[] pair = pairDecider(diceCombos.get(i), col1, col2, col3);
			boolean somethingEqualsFirst = (pair[0] == col1) || (pair[1] == col1);
			boolean somethingEqualsSecond = (pair[0] == col2) || (pair[1] == col2);
			boolean somethingEqualsThird = (pair[0] == col3) || (pair[1] == col3);

			if ((somethingEqualsFirst || somethingEqualsSecond) && !somethingEqualsThird)
			{
				workingCombos += frequencies.get(i);
			}
		}
		return workingCombos;
	}

	@Deprecated
	public int[] pairDecider(int[] roll, int col1, int col2, int col3)
	{
		if (col1 < 0) col1 = -col1;
		if (col2 < 0) col2 = -col2;
		if (col3 < 0) col3 = -col3;
		return pairDeciderWithThreeColumns(roll, col1, col2, col3);
	}

	@Deprecated
	public boolean shouldRollExpProgress(int countOfOne, int countOfTwo, int current)
	{
		return (countOfOne + 2 * countOfTwo) >= (permutations - countOfOne - countOfTwo) * current;
	}

}