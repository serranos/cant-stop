import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("ALL")
public class GraphicalUserGameInterface implements GameInterface, EventHandler<KeyEvent> {
    private final boolean GENERATE_TRANSCRIPT = false;
    private final boolean WAIT_AT_MOST_TWO_SECONDS_IN_COMPUTERS_TURN = false;
    private final String[] playerColorsInOrder = {"blue", "green", "yellow", "orange"};
    private final double backForwardIconSize = 25.0;

    private double screenHeight = 660;
    private double screenWidth = 900;
    private int[] defaultBoardValues;
    private boolean notAlreadyStarted = true;
    private CommandLineGameInterface transcript;
    private ExecutorService gameRunner;

    private Game game;
    private Stage stage;
    private Scene startScene;
    private Scene gameScene;
    private ImageHolder imageHolder;
    @FXML
    private AnchorPane gamePane;
    private StackPane startPane;
    private Button startButton;
    private VBox dicePanel;
    private List<VisibleDie> diceList;

    private VBox turnDisplayBox;
    private Text turnDisplayText;
    private Text instructionsText;
    private AnchorPane pieceLayer;
    private AnchorPane whitePieceLayer;
    private VBox rollAgainOrStopBox;
    private Button diceButton;
    private VBox columnChoiceBox;
    private Button columnOneButton;
    private Button columnTwoButton;
    private Button rollAgainButton;
    private Button stopButton;
    private Button computerOKButton;
    private AnchorPane computerOKPane;
    private Button playAgainButton;
    private AnchorPane playAgainPane;
    private AtomicBoolean stopGame;
    private Button restartGameButton;

    private Scene numPlayerScene;
    private Scene computerPlayingScene;
    private Scene feedbackChoiceScene;
    private Scene whosPlaying1;
    private Scene whosPlaying2;
    private Scene whosPlaying3;
    private Scene whosPlaying4;
    private Scene whosPlaying2Comp;
    private Scene whosPlaying3Comp;
    private Scene whosPlaying4Comp;
    private List<TextField> whosPlaying1Box;
    private List<TextField> whosPlaying2Boxes;
    private List<TextField> whosPlaying3Boxes;
    private List<TextField> whosPlaying4Boxes;
    private List<TextField> whosPlaying2CompBoxes;
    private List<TextField> whosPlaying3CompBoxes;
    private List<TextField> whosPlaying4CompBoxes;
    private Scene order2;
    private Scene order3;
    private Scene order4;
    private List<Button> order2Buttons;
    private List<Button> order3Buttons;
    private List<Button> order4Buttons;
    private AtomicInteger whichWhosPlayingOnScreen;
    private boolean shouldStartGame;
    private AtomicBoolean runSettingsScreens = new AtomicBoolean(false);

    private AtomicInteger curScreen;
    private AtomicBoolean randomize;
    private int numPlayers;
    private int feedbackCode;
    private boolean computerIsPlaying;
    private int[] orderOfPlayers;
    private String[] playerNames;

    private AtomicBoolean readyToMoveOn;
    private ExecutorService staller = Executors.newFixedThreadPool(4);
    private AtomicBoolean rollAgain;
    private AtomicBoolean columnOnePressed;
    private AtomicBoolean diceOnScreenForDisplayRoll = new AtomicBoolean(false);

    private class WaitingTask implements Callable<Boolean> {
        private final AtomicBoolean readyToMoveOn;
        private final long curTime;
        private final boolean isComputerPaddingTime;

        private WaitingTask(AtomicBoolean readyToMoveOn, boolean isComputerPaddingTime) {
            this.readyToMoveOn = readyToMoveOn;
            curTime = System.currentTimeMillis();
            this.isComputerPaddingTime = isComputerPaddingTime;
        }

        @Override
        public Boolean call() throws Exception {
            while (!this.readyToMoveOn.get() && !stopGame.get()) {
                if (isComputerPaddingTime && WAIT_AT_MOST_TWO_SECONDS_IN_COMPUTERS_TURN &&
                    System.currentTimeMillis() - curTime >= 2000) {
                    this.readyToMoveOn.set(true);
                }
            }
            return true;
        }
    }

    public void setGameStageAndScenes(Game game, Stage stage, Scene gameScene, AnchorPane gamePane) {
        this.game = game;
        this.stage = stage;
        this.gameScene = gameScene;
        this.imageHolder = new ImageHolder();
        this.gamePane = gamePane;
        this.readyToMoveOn = new AtomicBoolean();
        this.rollAgain = new AtomicBoolean();
        this.columnOnePressed = new AtomicBoolean();
        this.defaultBoardValues = game.defaultValues;
        setUpStartScreen();
        setUpSettingsScreens();
        setUpMainGameScreens();
    }

    private void setUpSettingsScreens() {
        numPlayers = 2;
        computerIsPlaying = true;
        feedbackCode = 1;
        orderOfPlayers = null;
        playerNames = null;
        curScreen = new AtomicInteger(0);
        randomize = new AtomicBoolean(false);
        whichWhosPlayingOnScreen = new AtomicInteger(-1);
        shouldStartGame = false;

        AnchorPane numPlayerPane = new AnchorPane();
        VBox box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        Text text = new Text("How many players?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        numPlayerPane.getChildren().add(box);
        Button players1 = new Button("1");
        players1.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                numPlayers = 1;
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setLeftAnchor(players1, 200.0);
        AnchorPane.setBottomAnchor(players1, 400.0);
        numPlayerPane.getChildren().add(players1);
        Button players2 = new Button("2");
        players2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                numPlayers = 2;
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setRightAnchor(players2, 200.0);
        AnchorPane.setBottomAnchor(players2, 400.0);
        numPlayerPane.getChildren().add(players2);
        Button players3 = new Button("3");
        players3.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                numPlayers = 3;
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setLeftAnchor(players3, 200.0);
        AnchorPane.setBottomAnchor(players3, 200.0);
        numPlayerPane.getChildren().add(players3);
        Button players4 = new Button("4");
        players4.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                numPlayers = 4;
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setRightAnchor(players4, 200.0);
        AnchorPane.setBottomAnchor(players4, 200.0);
        numPlayerPane.getChildren().add(players4);
        Button numPlayerBackButton = makeSettingsBackButton();
        numPlayerBackButton.setText("Back to start screen");
        numPlayerPane.getChildren().add(numPlayerBackButton);


        AnchorPane compPlayingPane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Is the computer playing?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        compPlayingPane.getChildren().add(box);
        Button yesButton = new Button("Yes");
        yesButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                computerIsPlaying = true;
                goForwardOneScreen(e);
            }
        });
        Button noButton = new Button("No");
        noButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                computerIsPlaying = false;
                goForwardOneScreen(e);
            }
        });
        Button compPlayingBackButton = makeSettingsBackButton();
        AnchorPane.setLeftAnchor(yesButton, 200.0);
        AnchorPane.setBottomAnchor(yesButton, 200.0);
        AnchorPane.setRightAnchor(noButton, 200.0);
        AnchorPane.setBottomAnchor(noButton, 200.0);
        compPlayingPane.getChildren().add(yesButton);
        compPlayingPane.getChildren().add(noButton);
        compPlayingPane.getChildren().add(compPlayingBackButton);


        AnchorPane feedbackOptionsPane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("What should be displayed on the console during the computer's turn?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        feedbackOptionsPane.getChildren().add(box);
        noButton = new Button("Nothing");
        noButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                feedbackCode = 0;
                goForwardOneScreen(e);
            }
        });
        Button someButton = new Button("Calculations computer uses to\nmake its decisions");
        someButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                feedbackCode = 1;
                goForwardOneScreen(e);
            }
        });
        Button allButton = new Button("Calculations computer uses to\nmake its decisions and errorless\nopponent calculations for comparison");
        allButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                feedbackCode = 2;
                goForwardOneScreen(e);
            }
        });
        Button feedbackChoiceBackButton = makeSettingsBackButton();
        AnchorPane.setLeftAnchor(noButton, 200.0);
        AnchorPane.setTopAnchor(noButton, 300.0);
        AnchorPane.setLeftAnchor(someButton, 200.0);
        AnchorPane.setTopAnchor(someButton, 400.0);
        AnchorPane.setLeftAnchor(allButton, 200.0);
        AnchorPane.setTopAnchor(allButton, 500.0);
        feedbackOptionsPane.getChildren().add(noButton);
        feedbackOptionsPane.getChildren().add(someButton);
        feedbackOptionsPane.getChildren().add(allButton);
        feedbackOptionsPane.getChildren().add(feedbackChoiceBackButton);


        AnchorPane whosPlaying1Pane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's playing?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        whosPlaying1Pane.getChildren().add(box);
        TextField wp1 = new TextField();
        AnchorPane.setTopAnchor(wp1, 200.0);
        AnchorPane.setLeftAnchor(wp1, 200.0);
        whosPlaying1Pane.getChildren().add(wp1);
        Button backButton = makeSettingsBackButton();
        whosPlaying1Pane.getChildren().add(backButton);
        Button skipButton = makeNameSkipButton();
        whosPlaying1Pane.getChildren().add(skipButton);
        Button submitButton = makeSubmitNamesButton();
        AnchorPane.setBottomAnchor(submitButton, 100.0);
        AnchorPane.setLeftAnchor(submitButton, 300.0);
        whosPlaying1Pane.getChildren().add(submitButton);
        whosPlaying1Box = new ArrayList<>();
        whosPlaying1Box.add(wp1);

        AnchorPane whosPlaying2Pane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's playing?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        whosPlaying2Pane.getChildren().add(box);
        TextField wp21 = new TextField();
        TextField wp22 = new TextField();
        AnchorPane.setTopAnchor(wp21, 200.0);
        AnchorPane.setLeftAnchor(wp21, 200.0);
        whosPlaying2Pane.getChildren().add(wp21);
        AnchorPane.setTopAnchor(wp22, 300.0);
        AnchorPane.setLeftAnchor(wp22, 200.0);
        whosPlaying2Pane.getChildren().add(wp22);
        backButton = makeSettingsBackButton();
        whosPlaying2Pane.getChildren().add(backButton);
        skipButton = makeNameSkipButton();
        whosPlaying2Pane.getChildren().add(skipButton);
        submitButton = makeSubmitNamesButton();
        AnchorPane.setBottomAnchor(submitButton, 100.0);
        AnchorPane.setLeftAnchor(submitButton, 300.0);
        whosPlaying2Pane.getChildren().add(submitButton);
        whosPlaying2Boxes = new ArrayList<>();
        whosPlaying2Boxes.add(wp21);
        whosPlaying2Boxes.add(wp22);

        AnchorPane whosPlaying3Pane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's playing?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        whosPlaying3Pane.getChildren().add(box);
        TextField wp31 = new TextField();
        TextField wp32 = new TextField();
        TextField wp33 = new TextField();
        AnchorPane.setTopAnchor(wp31, 200.0);
        AnchorPane.setLeftAnchor(wp31, 200.0);
        whosPlaying3Pane.getChildren().add(wp31);
        AnchorPane.setTopAnchor(wp32, 300.0);
        AnchorPane.setLeftAnchor(wp32, 200.0);
        whosPlaying3Pane.getChildren().add(wp32);
        AnchorPane.setTopAnchor(wp33, 400.0);
        AnchorPane.setLeftAnchor(wp33, 200.0);
        whosPlaying3Pane.getChildren().add(wp33);
        backButton = makeSettingsBackButton();
        whosPlaying3Pane.getChildren().add(backButton);
        skipButton = makeNameSkipButton();
        whosPlaying3Pane.getChildren().add(skipButton);
        submitButton = makeSubmitNamesButton();
        AnchorPane.setBottomAnchor(submitButton, 100.0);
        AnchorPane.setLeftAnchor(submitButton, 300.0);
        whosPlaying3Pane.getChildren().add(submitButton);
        whosPlaying3Boxes = new ArrayList<>();
        whosPlaying3Boxes.add(wp31);
        whosPlaying3Boxes.add(wp32);
        whosPlaying3Boxes.add(wp33);

        AnchorPane whosPlaying4Pane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's playing?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        whosPlaying4Pane.getChildren().add(box);
        TextField wp41 = new TextField();
        TextField wp42 = new TextField();
        TextField wp43 = new TextField();
        TextField wp44 = new TextField();
        AnchorPane.setTopAnchor(wp41, 200.0);
        AnchorPane.setLeftAnchor(wp41, 200.0);
        whosPlaying4Pane.getChildren().add(wp41);
        AnchorPane.setTopAnchor(wp42, 300.0);
        AnchorPane.setLeftAnchor(wp42, 200.0);
        whosPlaying4Pane.getChildren().add(wp42);
        AnchorPane.setTopAnchor(wp43, 400.0);
        AnchorPane.setLeftAnchor(wp43, 200.0);
        whosPlaying4Pane.getChildren().add(wp43);
        AnchorPane.setTopAnchor(wp44, 500.0);
        AnchorPane.setLeftAnchor(wp44, 200.0);
        whosPlaying4Pane.getChildren().add(wp44);
        backButton = makeSettingsBackButton();
        whosPlaying4Pane.getChildren().add(backButton);
        skipButton = makeNameSkipButton();
        whosPlaying4Pane.getChildren().add(skipButton);
        submitButton = makeSubmitNamesButton();
        AnchorPane.setBottomAnchor(submitButton, 100.0);
        AnchorPane.setLeftAnchor(submitButton, 300.0);
        whosPlaying4Pane.getChildren().add(submitButton);
        whosPlaying4Boxes = new ArrayList<>();
        whosPlaying4Boxes.add(wp41);
        whosPlaying4Boxes.add(wp42);
        whosPlaying4Boxes.add(wp43);
        whosPlaying4Boxes.add(wp44);

        AnchorPane whosPlaying2CompPane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's playing besides the computer?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        whosPlaying2CompPane.getChildren().add(box);
        TextField wp2Comp = new TextField();
        AnchorPane.setTopAnchor(wp2Comp, 200.0);
        AnchorPane.setLeftAnchor(wp2Comp, 200.0);
        whosPlaying2CompPane.getChildren().add(wp2Comp);
        backButton = makeSettingsBackButton();
        whosPlaying2CompPane.getChildren().add(backButton);
        skipButton = makeNameSkipButton();
        whosPlaying2CompPane.getChildren().add(skipButton);
        submitButton = makeSubmitNamesButton();
        AnchorPane.setBottomAnchor(submitButton, 100.0);
        AnchorPane.setLeftAnchor(submitButton, 300.0);
        whosPlaying2CompPane.getChildren().add(submitButton);
        whosPlaying2CompBoxes = new ArrayList<>();
        whosPlaying2CompBoxes.add(wp2Comp);

        AnchorPane whosPlaying3CompPane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's playing besides the computer?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        whosPlaying3CompPane.getChildren().add(box);
        TextField wp3Comp1 = new TextField();
        TextField wp3Comp2 = new TextField();
        AnchorPane.setTopAnchor(wp3Comp1, 200.0);
        AnchorPane.setLeftAnchor(wp3Comp1, 200.0);
        whosPlaying3CompPane.getChildren().add(wp3Comp1);
        AnchorPane.setTopAnchor(wp3Comp2, 300.0);
        AnchorPane.setLeftAnchor(wp3Comp2, 200.0);
        whosPlaying3CompPane.getChildren().add(wp3Comp2);
        backButton = makeSettingsBackButton();
        whosPlaying3CompPane.getChildren().add(backButton);
        skipButton = makeNameSkipButton();
        whosPlaying3CompPane.getChildren().add(skipButton);
        submitButton = makeSubmitNamesButton();
        AnchorPane.setBottomAnchor(submitButton, 100.0);
        AnchorPane.setLeftAnchor(submitButton, 300.0);
        whosPlaying3CompPane.getChildren().add(submitButton);
        whosPlaying3CompBoxes = new ArrayList<>();
        whosPlaying3CompBoxes.add(wp3Comp1);
        whosPlaying3CompBoxes.add(wp3Comp2);

        AnchorPane whosPlaying4CompPane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's playing besides the computer?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        whosPlaying4CompPane.getChildren().add(box);
        TextField wp4Comp1 = new TextField();
        TextField wp4Comp2 = new TextField();
        TextField wp4Comp3 = new TextField();
        AnchorPane.setTopAnchor(wp4Comp1, 200.0);
        AnchorPane.setLeftAnchor(wp4Comp1, 200.0);
        whosPlaying4CompPane.getChildren().add(wp4Comp1);
        AnchorPane.setTopAnchor(wp4Comp2, 300.0);
        AnchorPane.setLeftAnchor(wp4Comp2, 200.0);
        whosPlaying4CompPane.getChildren().add(wp4Comp2);
        AnchorPane.setTopAnchor(wp4Comp3, 400.0);
        AnchorPane.setLeftAnchor(wp4Comp3, 200.0);
        whosPlaying4CompPane.getChildren().add(wp4Comp3);
        backButton = makeSettingsBackButton();
        whosPlaying4CompPane.getChildren().add(backButton);
        skipButton = makeNameSkipButton();
        whosPlaying4CompPane.getChildren().add(skipButton);
        submitButton = makeSubmitNamesButton();
        AnchorPane.setBottomAnchor(submitButton, 100.0);
        AnchorPane.setLeftAnchor(submitButton, 300.0);
        whosPlaying4CompPane.getChildren().add(submitButton);
        whosPlaying4CompBoxes = new ArrayList<>();
        whosPlaying4CompBoxes.add(wp4Comp1);
        whosPlaying4CompBoxes.add(wp4Comp2);
        whosPlaying4CompBoxes.add(wp4Comp3);

        AnchorPane order2Pane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's going next (or first, if no one's been chosen yet)?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        order2Pane.getChildren().add(box);
        final Button order2Button1 = new Button("Button 1");
        final Button order2Button2 = new Button("Button 2");
        order2Button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order2Button1.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 2] = i;
                    }
                }
                for (int j = 0; j < numPlayers; j++) {
                    boolean found = false;
                    for (int k = 0; k < numPlayers - 1; k++) {
                        if (orderOfPlayers[k] == j) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        orderOfPlayers[orderOfPlayers.length - 1] = j;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        order2Button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order2Button2.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 2] = i;
                    }
                }
                for (int j = 0; j < numPlayers; j++) {
                    boolean found = false;
                    for (int k = 0; k < numPlayers - 1; k++) {
                        if (orderOfPlayers[k] == j) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        orderOfPlayers[orderOfPlayers.length - 1] = j;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setTopAnchor(order2Button1, 250.0);
        AnchorPane.setLeftAnchor(order2Button1, 200.0);
        order2Pane.getChildren().add(order2Button1);
        AnchorPane.setTopAnchor(order2Button2, 250.0);
        AnchorPane.setRightAnchor(order2Button2, 200.0);
        order2Pane.getChildren().add(order2Button2);
        backButton = makeSettingsBackButton();
        order2Pane.getChildren().add(backButton);
        skipButton = makeOrderSkipButton();
        order2Pane.getChildren().add(skipButton);
        order2Buttons = new ArrayList<>();
        order2Buttons.add(order2Button1);
        order2Buttons.add(order2Button2);
        order2Buttons.add(skipButton);

        AnchorPane order3Pane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's going next (or first, if no one's been chosen yet)?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        order3Pane.getChildren().add(box);
        final Button order3Button1 = new Button("Button 1");
        final Button order3Button2 = new Button("Button 2");
        final Button order3Button3 = new Button("Button 3");
        order3Button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order3Button1.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 3] = i;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        order3Button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order3Button2.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 3] = i;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        order3Button3.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order3Button3.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 3] = i;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setTopAnchor(order3Button1, 200.0);
        AnchorPane.setLeftAnchor(order3Button1, 200.0);
        order3Pane.getChildren().add(order3Button1);
        AnchorPane.setTopAnchor(order3Button2, 200.0);
        AnchorPane.setRightAnchor(order3Button2, 200.0);
        order3Pane.getChildren().add(order3Button2);
        AnchorPane.setTopAnchor(order3Button3, 400.0);
        AnchorPane.setLeftAnchor(order3Button3, 350.0);
        order3Pane.getChildren().add(order3Button3);
        backButton = makeSettingsBackButton();
        order3Pane.getChildren().add(backButton);
        skipButton = makeOrderSkipButton();
        order3Pane.getChildren().add(skipButton);
        order3Buttons = new ArrayList<>();
        order3Buttons.add(order3Button1);
        order3Buttons.add(order3Button2);
        order3Buttons.add(order3Button3);
        order3Buttons.add(skipButton);

        AnchorPane order4Pane = new AnchorPane();
        box = new VBox();
        box.setPrefWidth(screenWidth);
        box.setAlignment(Pos.CENTER);
        text = new Text("Who's going next (or first, if no one's been chosen yet)?");
        text.setFont(Font.font("Arial", 20.0));
        box.getChildren().add(text);
        AnchorPane.setTopAnchor(box, 100.0);
        order4Pane.getChildren().add(box);
        final Button order4Button1 = new Button("Button 1");
        final Button order4Button2 = new Button("Button 2");
        final Button order4Button3 = new Button("Button 3");
        final Button order4Button4 = new Button("Button 4");
        order4Button1.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order4Button1.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 4] = i;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        order4Button2.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order4Button2.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 4] = i;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        order4Button3.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order4Button3.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 4] = i;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        order4Button4.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                String name = order4Button4.getText();
                for (int i = 0; i < playerNames.length; i++) {
                    if (playerNames[i].equals(name)) {
                        orderOfPlayers[orderOfPlayers.length - 4] = i;
                    }
                }
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setTopAnchor(order4Button1, 200.0);
        AnchorPane.setLeftAnchor(order4Button1, 200.0);
        order4Pane.getChildren().add(order4Button1);
        AnchorPane.setTopAnchor(order4Button2, 200.0);
        AnchorPane.setRightAnchor(order4Button2, 200.0);
        order4Pane.getChildren().add(order4Button2);
        AnchorPane.setTopAnchor(order4Button3, 400.0);
        AnchorPane.setLeftAnchor(order4Button3, 200.0);
        order4Pane.getChildren().add(order4Button3);
        AnchorPane.setTopAnchor(order4Button4, 400.0);
        AnchorPane.setRightAnchor(order4Button4, 200.0);
        order4Pane.getChildren().add(order4Button4);
        backButton = makeSettingsBackButton();
        order4Pane.getChildren().add(backButton);
        skipButton = makeOrderSkipButton();
        order4Pane.getChildren().add(skipButton);
        order4Buttons = new ArrayList<>();
        order4Buttons.add(order4Button1);
        order4Buttons.add(order4Button2);
        order4Buttons.add(order4Button3);
        order4Buttons.add(order4Button4);
        order4Buttons.add(skipButton);

        numPlayerScene = new Scene(numPlayerPane, screenWidth, screenHeight);
        computerPlayingScene = new Scene(compPlayingPane, screenWidth, screenHeight);
        feedbackChoiceScene = new Scene(feedbackOptionsPane, screenWidth, screenHeight);
        whosPlaying1 = new Scene(whosPlaying1Pane, screenWidth, screenHeight);
        whosPlaying2 = new Scene(whosPlaying2Pane, screenWidth, screenHeight);
        whosPlaying3 = new Scene(whosPlaying3Pane, screenWidth, screenHeight);
        whosPlaying4 = new Scene(whosPlaying4Pane, screenWidth, screenHeight);
        whosPlaying2Comp = new Scene(whosPlaying2CompPane, screenWidth, screenHeight);
        whosPlaying3Comp = new Scene(whosPlaying3CompPane, screenWidth, screenHeight);
        whosPlaying4Comp = new Scene(whosPlaying4CompPane, screenWidth, screenHeight);
        order2 = new Scene(order2Pane, screenWidth, screenHeight);
        order3 = new Scene(order3Pane, screenWidth, screenHeight);
        order4 = new Scene(order4Pane, screenWidth, screenHeight);
    }

    private Button makeSettingsBackButton() {
        ImageView icon = new ImageView("images/back.png");
        icon.setFitHeight(backForwardIconSize);
        icon.setFitWidth(backForwardIconSize);
        Button backButton = new Button("Back", icon);
        backButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                goBackOneScreen(e);
            }
        });
        AnchorPane.setTopAnchor(backButton, 10.0);
        AnchorPane.setLeftAnchor(backButton, 10.0);
        return backButton;
    }

    private Button makeNameSkipButton() {
        ImageView icon = new ImageView("images/skip.png");
        icon.setFitHeight(backForwardIconSize);
        icon.setFitWidth(backForwardIconSize);
        Button skipButton = new Button("Skip", icon);
        skipButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                fillInRemainingNames();
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setTopAnchor(skipButton, 10.0);
        AnchorPane.setRightAnchor(skipButton, 10.0);
        return skipButton;
    }

    private Button makeSubmitNamesButton() {
        Button submitButton = new Button("Enter names");
        submitButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                collectAnyGivenNamesFromNameScreen();
                goForwardOneScreen(e);
            }
        });
        return submitButton;
    }

    private Button makeOrderSkipButton() {
        ImageView icon = new ImageView("images/skip.png");
        icon.setFitHeight(backForwardIconSize);
        icon.setFitWidth(backForwardIconSize);
        Button skipButton = new Button("Never mind, randomize order", icon);
        skipButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                randomize.set(true);
                goForwardOneScreen(e);
            }
        });
        AnchorPane.setTopAnchor(skipButton, 10.0);
        AnchorPane.setRightAnchor(skipButton, 10.0);
        return skipButton;
    }

    private void collectAnyGivenNamesFromNameScreen() {
        playerNames = new String[numPlayers];
        for (int i = 0; i < numPlayers; i++) {
            if (i == 0 && computerIsPlaying) {
                playerNames[i] = "Computer";
            } else {
                playerNames[i] = null;
            }
        }
        String text;
        if (numPlayers == 1 && !computerIsPlaying) {
            text = whosPlaying1Box.get(0).getText().trim();
            if (!text.isEmpty()) {
                playerNames[0] = text;
            } else {
                playerNames[0] = null;
            }
        } else if (numPlayers == 2 && !computerIsPlaying) {
            for (int i = 0; i < 2; i++) {
                text = whosPlaying2Boxes.get(i).getText().trim();
                if (!text.isEmpty()) {
                    playerNames[i] = text;
                } else {
                    playerNames[i] = null;
                }
            }
        } else if (numPlayers == 3 && !computerIsPlaying) {
            for (int i = 0; i < 3; i++) {
                text = whosPlaying3Boxes.get(i).getText().trim();
                if (!text.isEmpty()) {
                    playerNames[i] = text;
                } else {
                    playerNames[i] = null;
                }
            }
        } else if (numPlayers == 4 && !computerIsPlaying) {
            for (int i = 0; i < 4; i++) {
                text = whosPlaying4Boxes.get(i).getText().trim();
                if (!text.isEmpty()) {
                    playerNames[i] = text;
                } else {
                    playerNames[i] = null;
                }
            }
        } else if (numPlayers == 2 && computerIsPlaying) {
            for (int i = 1; i < 2; i++) {
                text = whosPlaying2CompBoxes.get(i - 1).getText().trim();
                if (!text.isEmpty()) {
                    playerNames[i] = text;
                } else {
                    playerNames[i] = null;
                }
            }
        } else if (numPlayers == 3 && computerIsPlaying) {
            for (int i = 1; i < 3; i++) {
                text = whosPlaying3CompBoxes.get(i - 1).getText().trim();
                if (!text.isEmpty()) {
                    playerNames[i] = text;
                } else {
                    playerNames[i] = null;
                }
            }
        } else if (numPlayers == 4 && computerIsPlaying) {
            for (int i = 1; i < 4; i++) {
                text = whosPlaying4CompBoxes.get(i - 1).getText().trim();
                if (!text.isEmpty()) {
                    playerNames[i] = text;
                } else {
                    playerNames[i] = null;
                }
            }
        }
        fillInRemainingNames();
    }

    private void fillInRemainingNames() {
        if (playerNames == null) {
            playerNames = new String[numPlayers];
            for (int i = 0; i < playerNames.length; i++) {
                playerNames[i] = null;
            }
        }
        for (int i = 0; i < numPlayers; i++) {
            if (computerIsPlaying && i == 0) {
                playerNames[i] = "Computer";
            } else if (playerColorsInOrder[i].equals("green") && playerNames[i] == null) {
                playerNames[i] = "Green";
            } else if (playerColorsInOrder[i].equals("yellow") && playerNames[i] == null) {
                playerNames[i] = "Yellow";
            } else if (playerColorsInOrder[i].equals("orange") && playerNames[i] == null) {
                playerNames[i] = "Orange";
            } else if (playerNames[i] == null) {
                playerNames[i] = "Blue";
            }
        }
    }

    public void goBackOneScreen(ActionEvent e) {
        if (curScreen.get() == 4 && !computerIsPlaying) {
            curScreen.set(curScreen.get() - 1);
        }
        curScreen.set(curScreen.get() - 1);
        goToNewCurScreen();
        readyToMoveOn.set(true);
    }

    public void goForwardOneScreen(ActionEvent e) {
        if (curScreen.get() == 2 && !computerIsPlaying) {
            curScreen.set(curScreen.get() + 1);
        }
        curScreen.set(curScreen.get() + 1);
        goToNewCurScreen();
        readyToMoveOn.set(true);
    }

    public void randomizeOrderAndStartGame() {
        orderOfPlayers = null;
        submitInformationAndStartGame();
    }

    public void submitInformationAndStartGame() {
        game.setCompIsPlayingNumPlayersAndOrder(computerIsPlaying, numPlayers, orderOfPlayers, feedbackCode);
        startGame();
    }

    private void startGame() {
        curScreen.set(1);
        shouldStartGame = true;
        readyToMoveOn.set(true);
    }

    public void runSettingsScreenSequence() {
        shouldStartGame = false;
        while (!shouldStartGame) {
            setUpCurScreen();
            goToNewCurScreen();

            if (!shouldStartGame) {
                readyToMoveOn.set(false);
                waitForRunningTaskToFinish(false);
            }
        }
        shouldStartGame = false;
        runSettingsScreens.set(false);
    }

    private void setUpCurScreen() {
        final int screen = curScreen.get();
        readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (screen == 4) { // name screen
                    if (numPlayers == 2 && computerIsPlaying) {
                        for (int i = 0; i < numPlayers - 1; i++) {
                            whosPlaying2CompBoxes.get(i).setPromptText(playerColorsInOrder[i + 1].substring(0, 1).toUpperCase() +
                                                                       playerColorsInOrder[i + 1].substring(1, playerColorsInOrder[i + 1].length()));
                        }
                    } else if (numPlayers == 3 && computerIsPlaying) {
                        for (int i = 0; i < numPlayers - 1; i++) {
                            whosPlaying3CompBoxes.get(i).setPromptText(playerColorsInOrder[i + 1].substring(0, 1).toUpperCase() +
                                playerColorsInOrder[i + 1].substring(1, playerColorsInOrder[i + 1].length()));
                        }
                    } else if (numPlayers == 4 && computerIsPlaying) {
                        for (int i = 0; i < numPlayers - 1; i++) {
                            whosPlaying4CompBoxes.get(i).setPromptText(playerColorsInOrder[i + 1].substring(0, 1).toUpperCase() +
                                playerColorsInOrder[i + 1].substring(1, playerColorsInOrder[i + 1].length()));
                        }
                    } else if (numPlayers == 1 && !computerIsPlaying) {
                        for (int i = 0; i < numPlayers; i++) {
                            whosPlaying1Box.get(i).setPromptText(playerColorsInOrder[i].substring(0, 1).toUpperCase() +
                                playerColorsInOrder[i].substring(1, playerColorsInOrder[i].length()));
                        }
                    } else if (numPlayers == 2 && !computerIsPlaying) {
                        for (int i = 0; i < numPlayers; i++) {
                            whosPlaying2Boxes.get(i).setPromptText(playerColorsInOrder[i].substring(0, 1).toUpperCase() +
                                playerColorsInOrder[i].substring(1, playerColorsInOrder[i].length()));
                        }
                    } else if (numPlayers == 3 && !computerIsPlaying) {
                        for (int i = 0; i < numPlayers; i++) {
                            whosPlaying3Boxes.get(i).setPromptText(playerColorsInOrder[i].substring(0, 1).toUpperCase() +
                                playerColorsInOrder[i].substring(1, playerColorsInOrder[i].length()));
                        }
                    } else if (numPlayers == 4 && !computerIsPlaying) {
                        for (int i = 0; i < numPlayers; i++) {
                            whosPlaying4Boxes.get(i).setPromptText(playerColorsInOrder[i].substring(0, 1).toUpperCase() +
                                playerColorsInOrder[i].substring(1, playerColorsInOrder[i].length()));
                        }
                    }
                } else if ((screen == 5 && numPlayers == 2) || (screen == 6 && numPlayers == 3) || (screen == 7 && numPlayers == 4)) {
                    String[] namesLeft = null;
                    if (numPlayers == 2) {
                        order2Buttons.get(order2Buttons.size() - 1).setText("Randomize order");
                        orderOfPlayers = new int[2];
                        namesLeft = playerNames;
                    } else {
                        order2Buttons.get(order2Buttons.size() - 1).setText("Never mind, randomize order");
                        if (numPlayers == 3) {
                            namesLeft = getUnpickedNames(0);
                        } else if (numPlayers == 4) {
                            namesLeft = getUnpickedNames(1);
                        }
                    }
                    for (int i = 0; i < order2Buttons.size() - 1; i++) {
                        order2Buttons.get(i).setText(namesLeft[i]);
                    }
                } else if ((screen == 5 && numPlayers == 3) || (screen == 6 && numPlayers == 4)) {
                    String[] namesLeft = null;
                    if (numPlayers == 3) {
                        order3Buttons.get(order3Buttons.size() - 1).setText("Randomize order");
                        orderOfPlayers = new int[3];
                        namesLeft = playerNames;
                    } else {
                        order3Buttons.get(order3Buttons.size() - 1).setText("Never mind, randomize order");
                        namesLeft = getUnpickedNames(0);
                    }
                    for (int i = 0; i < order3Buttons.size() - 1; i++) {
                        order3Buttons.get(i).setText(namesLeft[i]);
                    }
                } else if (screen == 5 && numPlayers == 4) {
                    for (int i = 0; i < order4Buttons.size() - 1; i++) {
                        order4Buttons.get(i).setText(playerNames[i]);
                    }
                    if (numPlayers == 4) {
                        orderOfPlayers = new int[4];
                        order4Buttons.get(order4Buttons.size() - 1).setText("Randomize order");
                    }
                }
                readyToMoveOn.set(true);
            }
        });
        waitForRunningTaskToFinish(false);
    }

    private String[] getUnpickedNames(int trustUpToThisIndex) {
        if (orderOfPlayers == null) {
            return playerNames;
        } else {
            ArrayList<String> curNames = new ArrayList<>();
            for (int i = 0; i < numPlayers; i++) {
                curNames.add(playerNames[i]);
            }
            for (int i = 0; i <= trustUpToThisIndex; i++) {
                curNames.remove(playerNames[orderOfPlayers[i]]);
            }
            return curNames.toArray(new String[curNames.size()]);
        }
    }

    private void goToNewCurScreen() {
        final AtomicBoolean moveOn = new AtomicBoolean(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                int screen = curScreen.get();
                if (screen == 0) {
                    stage.setScene(startScene);
                } else if (screen == 1) {
                    stage.setScene(numPlayerScene);
                } else if (screen == 2) {
                    stage.setScene(computerPlayingScene);
                } else if (screen == 3) { // Feedback-choosing screen
                    stage.setScene(feedbackChoiceScene);
                } else if (screen == 4) { // Names
                    if (numPlayers == 1 && !computerIsPlaying) {
                        stage.setScene(whosPlaying1);
                    } else if (numPlayers == 2 && !computerIsPlaying) {
                        stage.setScene(whosPlaying2);
                    } else if (numPlayers == 3 && !computerIsPlaying) {
                        stage.setScene(whosPlaying3);
                    } else if (numPlayers == 4 && !computerIsPlaying) {
                        stage.setScene(whosPlaying4);
                    } else if (numPlayers == 2 && computerIsPlaying) {
                        stage.setScene(whosPlaying2Comp);
                    } else if (numPlayers == 3 && computerIsPlaying) {
                        stage.setScene(whosPlaying3Comp);
                    } else if (numPlayers == 4 && computerIsPlaying) {
                        stage.setScene(whosPlaying4Comp);
                    } else {
                        // randomizing the order is a little irrelevant in the one-player computer case,
                        // but that's okay
                        randomizeOrderAndStartGame();
                    }
                } else if (screen == 5) { // in what order?
                    if (numPlayers == 1) {
                        randomizeOrderAndStartGame();
                    } else if (numPlayers == 2) {
                        stage.setScene(order2);
                    } else if (numPlayers == 3) {
                        stage.setScene(order3);
                    } else if (numPlayers == 4) {
                        stage.setScene(order4);
                    }
                } else if (screen == 6) {
                    if (randomize.get()) {
                        randomizeOrderAndStartGame();
                    } else if (numPlayers == 2) {
                        submitInformationAndStartGame();
                    } else if (numPlayers == 3) {
                        stage.setScene(order2);
                    } else if (numPlayers == 4) {
                        stage.setScene(order3);
                    }
                } else if (screen == 7) {
                    if (randomize.get()) {
                        randomizeOrderAndStartGame();
                    } else if (numPlayers == 3) {
                        submitInformationAndStartGame();
                    } else if (numPlayers == 4) {
                        stage.setScene(order2);
                    }
                } else if (screen == 8) {
                    if (randomize.get()) {
                        randomizeOrderAndStartGame();
                    } else if (numPlayers == 4) {
                        submitInformationAndStartGame();
                    }
                }
                moveOn.set(true);
            }
        });
        staller.submit(new WaitingTask(moveOn, false));
    }

    private void setUpStartScreen() {
        this.startPane = new StackPane();
        ImageView image = new ImageView("images/boardgameimage.jpg");
        image.setFitHeight(screenHeight);
        image.setFitWidth(screenWidth);
        startPane.getChildren().add(image);

        AnchorPane buttonPane = new AnchorPane();
        buttonPane.setPrefHeight(screenHeight);
        buttonPane.setPrefWidth(screenWidth);

        this.startButton = new Button("Start Game");
        startButton.setStyle("-fx-text-fill: white; -fx-font: 28 \"Arial Black\"; -fx-base: #68c748;");
        this.startButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                onStartButton(e);
            }
        });
        this.startButton.setPrefWidth(350);
        this.startButton.setPrefHeight(75);
        AnchorPane.setLeftAnchor(startButton, 275.0);
        AnchorPane.setTopAnchor(startButton, 350.0);
        buttonPane.getChildren().add(startButton);

        Button gameSettingsButton = new Button("Game Settings");
        gameSettingsButton.setStyle("-fx-text-fill: white; -fx-font: 22 \"Arial Black\"; -fx-base: #13a0cf;");
        gameSettingsButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                runSettingsScreens.set(true);
                goForwardOneScreen(e);
            }
        });
        gameSettingsButton.setPrefWidth(350);
        gameSettingsButton.setPrefHeight(75);
        AnchorPane.setLeftAnchor(gameSettingsButton, 275.0);
        AnchorPane.setTopAnchor(gameSettingsButton, 450.0);
        buttonPane.getChildren().add(gameSettingsButton);

        this.startPane.getChildren().add(buttonPane);
        this.startScene = new Scene(startPane, screenWidth, screenHeight);
        startScene.setOnKeyPressed(this);
    }

    private void setUpMainGameScreens() {
        Button backToSettingsScreensButton = new Button("Choose new game settings");
        backToSettingsScreensButton.setStyle("-fx-base: #b6d8e3;");
        backToSettingsScreensButton.setPrefWidth(197);
        backToSettingsScreensButton.setPrefHeight(35);
        backToSettingsScreensButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("");
                alert.setHeaderText("Really quit?");
                alert.setContentText("Current game will be quit. Are you sure you want to proceed?");

                Optional<ButtonType> result = alert.showAndWait();
                //noinspection Since15,Since15
                if (result.get() == ButtonType.OK){
                    runSettingsScreens.set(true);
                    stopGame();
                }
            }
        });
        AnchorPane.setBottomAnchor(backToSettingsScreensButton, 30.0);
        AnchorPane.setRightAnchor(backToSettingsScreensButton, 46.0);
        gamePane.getChildren().add(backToSettingsScreensButton);

        restartGameButton = new Button("Restart game");
        restartGameButton.setStyle("-fx-base: #b6d8e3;");
        restartGameButton.setPrefWidth(197);
        restartGameButton.setPrefHeight(35);
        restartGameButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("");
                alert.setHeaderText("Really quit?");
                alert.setContentText("Current game will be quit. Are you sure you want to proceed?");

                Optional<ButtonType> result = alert.showAndWait();
                //noinspection Since15,Since15
                if (result.get() == ButtonType.OK){
                 runSettingsScreens.set(false);
                 stopGame();
                }
            }
        });
        AnchorPane.setBottomAnchor(restartGameButton, 75.0);
        AnchorPane.setRightAnchor(restartGameButton, 46.0);
        gamePane.getChildren().add(restartGameButton);

        this.turnDisplayBox = new VBox();
        AnchorPane.setTopAnchor(turnDisplayBox, 0.0);
        AnchorPane.setLeftAnchor(turnDisplayBox, 0.0);
        this.turnDisplayBox.setPrefWidth(screenWidth);
        this.turnDisplayBox.setPrefHeight(40.0);
        this.turnDisplayBox.setBackground(new Background(new BackgroundFill(Color.ORANGE,
            CornerRadii.EMPTY, Insets.EMPTY)));
        this.turnDisplayBox.setAlignment(Pos.CENTER);
        this.turnDisplayText = new Text(0,0,"");
        this.turnDisplayText.setFont(Font.font("Arial", 20.0));
        this.turnDisplayBox.getChildren().add(turnDisplayText);
        this.gamePane.getChildren().add(this.turnDisplayBox);

        VBox instructionsBox = new VBox();
        instructionsBox.setPrefWidth(250);
        instructionsBox.setPrefHeight(160);
        instructionsBox.setAlignment(Pos.BOTTOM_LEFT);
        //instructionsBox.setStyle("-fx-border-width: 2px; -fx-border-color: #2e8b57;");
        AnchorPane.setTopAnchor(instructionsBox, 45.0);
        AnchorPane.setRightAnchor(instructionsBox, 5.0);
        this.instructionsText = new Text(0,0,"");
        this.instructionsText.setFont(Font.font("Arial", 20.0));
        instructionsBox.getChildren().add(this.instructionsText);
        this.gamePane.getChildren().add(instructionsBox);


        StackPane boardRegion = new StackPane();
        boardRegion.setPrefHeight(600);
        boardRegion.setPrefWidth(600);
        AnchorPane.setLeftAnchor(boardRegion, 10.0);
        AnchorPane.setTopAnchor(boardRegion, 50.0);
        this.gamePane.getChildren().add(boardRegion);

        AnchorPane boardPane = new AnchorPane();
        boardPane.setPrefHeight(600);
        boardPane.setPrefWidth(600);
        //boardPane.setStyle("-fx-border-width: 2px; -fx-border-color: #2e8b57;");
        Node board = imageHolder.getBoard();
        boardPane.getChildren().add(board);
        boardRegion.getChildren().add(boardPane);

        pieceLayer = new AnchorPane();
        pieceLayer.setPrefHeight(600);
        pieceLayer.setPrefHeight(600);
        boardRegion.getChildren().add(pieceLayer);

        whitePieceLayer = new AnchorPane();
        whitePieceLayer.setPrefHeight(600);
        whitePieceLayer.setPrefHeight(600);
        boardRegion.getChildren().add(whitePieceLayer);

        this.diceList = new ArrayList<VisibleDie>();
        for (int i = 1; i <= 4; i++) {
            this.diceList.add(new VisibleDie(imageHolder, i));
        }
        HBox diceLevel1 = new HBox();
        int diceSpacing = 10;
        diceLevel1.setSpacing(diceSpacing);
        for (int i = 0; i < 2; i++) {
            diceLevel1.getChildren().add(this.diceList.get(i));
        }
        HBox diceLevel2 = new HBox();
        diceLevel2.setSpacing(diceSpacing);
        for (int i = 2; i < 4; i++) {
            diceLevel2.getChildren().add(this.diceList.get(i));
        }
        this.dicePanel = new VBox();
        this.dicePanel.setAlignment(Pos.CENTER);
        this.dicePanel.setSpacing(10);
        diceLevel1.setAlignment(Pos.CENTER);
        diceLevel2.setAlignment(Pos.CENTER);
        this.dicePanel.getChildren().add(diceLevel1);
        this.dicePanel.getChildren().add(diceLevel2);
        this.dicePanel.setPrefWidth(270);
        diceButton = new Button("Enter Choice");
        diceButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                enterChoice(e);
            }
        });
        diceButton.setStyle("-fx-base: #b6e7c9;");
        diceButton.setPrefWidth(197);
        diceButton.setPrefHeight(249 - 2.0 * diceList.get(0).getHeightAndWidth() - 2 * diceSpacing);
        this.dicePanel.getChildren().add(diceButton);
        AnchorPane.setRightAnchor(this.dicePanel, 10.0);
        AnchorPane.setTopAnchor(this.dicePanel, 214.0);
        //dicePanel.setStyle("-fx-border-width: 2px; -fx-border-color: #2e8b57;");

        this.rollAgainOrStopBox = new VBox();
        this.rollAgainOrStopBox.setPrefWidth(270);
        rollAgainOrStopBox.setSpacing(10);
        rollAgainOrStopBox.setAlignment(Pos.CENTER);
        rollAgainButton = new Button("Roll Again");
        rollAgainButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                rollAgainButtonPressed(e);
            }
        });
        rollAgainButton.setStyle("-fx-base: #b6e7c9;");
        rollAgainButton.setPrefWidth(197);
        rollAgainButton.setPrefHeight(130);
        rollAgainOrStopBox.getChildren().add(rollAgainButton);
        stopButton = new Button("Stop Rolling");
        stopButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                stopRollingButtonPressed(e);
            }
        });
        stopButton.setStyle("-fx-base: #ff6666;");
        stopButton.setPrefWidth(197);
        stopButton.setPrefHeight(130);
        AnchorPane.setRightAnchor(this.rollAgainOrStopBox, 10.0);
        AnchorPane.setTopAnchor(this.rollAgainOrStopBox, 214.0);
        rollAgainOrStopBox.getChildren().add(stopButton);

        this.columnChoiceBox = new VBox();
        this.columnChoiceBox.setPrefWidth(270);
        columnChoiceBox.setSpacing(10);
        columnChoiceBox.setAlignment(Pos.CENTER);
        columnOneButton = new Button("");
        columnOneButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                columnOneButtonPressed(e);
            }
        });
        columnOneButton.setStyle("-fx-base: #ff6666;");
        columnOneButton.setPrefWidth(197);
        columnOneButton.setPrefHeight(130);
        columnChoiceBox.getChildren().add(columnOneButton);
        columnTwoButton = new Button("");
        columnTwoButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                columnTwoButtonPressed(e);
            }
        });
        columnTwoButton.setStyle("-fx-base: #33adff;");
        columnTwoButton.setPrefWidth(197);
        columnTwoButton.setPrefHeight(130);
        AnchorPane.setRightAnchor(this.columnChoiceBox, 10.0);
        AnchorPane.setTopAnchor(this.columnChoiceBox, 214.0);
        columnChoiceBox.getChildren().add(columnTwoButton);

        computerOKPane = new AnchorPane();
        computerOKPane.setPrefWidth(270);
        computerOKPane.setPrefHeight(270);
        computerOKButton = new Button("OK");
        computerOKButton.setStyle("-fx-base: #b6e7c9;");
        computerOKButton.setPrefWidth(197);
        computerOKButton.setPrefHeight(130);
        AnchorPane.setLeftAnchor(computerOKButton, 32.0);
        AnchorPane.setTopAnchor(computerOKButton, 67.0);
        computerOKButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                enterChoice(e);
            }
        });
        AnchorPane.setRightAnchor(computerOKPane, 10.0);
        AnchorPane.setTopAnchor(computerOKPane, 214.0);
        computerOKPane.getChildren().add(computerOKButton);

        playAgainPane = new AnchorPane();
        playAgainPane.setPrefWidth(270);
        playAgainPane.setPrefHeight(270);
        playAgainButton = new Button(" Play\nAgain");
        playAgainButton.setStyle("-fx-font: 28 \"Arial Black\"; -fx-base: #b6e7c9;");
        playAgainButton.setPrefWidth(197);
        playAgainButton.setPrefHeight(130);
        AnchorPane.setLeftAnchor(playAgainButton, 32.0);
        AnchorPane.setTopAnchor(playAgainButton, 67.0);
        playAgainButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override public void handle(ActionEvent e) {
                runSettingsScreens.set(false);
                enterChoice(e);
            }
        });
        AnchorPane.setRightAnchor(playAgainPane, 10.0);
        AnchorPane.setTopAnchor(playAgainPane, 214.0);
        playAgainPane.getChildren().add(playAgainButton);
    }

    /**
     *
     * @param piece
     * @param col (int) : goes from 0 to 10
     * @param positionOnCol (int) : highest is 0, lowest (while still being on board) is the
     *                      corresponding default position minus 1
     */
    private void positionPieceOnBoard(Node piece, boolean isWhitePiece, int col, int positionOnCol) {
        double offsetFromTop = 0;
        double offsetFromLeft = 0;
        if (col == 0) {
            offsetFromLeft = 15;
            offsetFromTop = 201 + positionOnCol * 77;
        } else if (col == 1) {
            offsetFromLeft = 68.25;
            offsetFromTop = 148 + positionOnCol * 66.75;
            if (positionOnCol >= 2) {
                offsetFromTop -= (3 * positionOnCol - 1);
            }
            if (positionOnCol == 4) {
                offsetFromTop -= 2;
            }
        } else if (col == 2) {
            offsetFromLeft = 122.25;
            offsetFromTop = 107 + positionOnCol * 57;
            if (positionOnCol == 6) {
                offsetFromTop -= 3;
            }
        } else if (col == 3) {
            offsetFromLeft = 174.75;
            offsetFromTop = 70 + positionOnCol * 52.5;
            if (positionOnCol >= 4) {
                offsetFromTop = offsetFromTop - 2 - (positionOnCol - 4) * 1.5;
            }
            if (positionOnCol == 8) {
                offsetFromTop -= 1;
            }
        } else if (col == 4) {
            offsetFromLeft = 229;
            offsetFromTop = 34.5 + positionOnCol * 47;
            if (positionOnCol >= 3) {
                offsetFromTop += 3 * (positionOnCol - 2);
            }
            if (positionOnCol >= 5) {
                offsetFromLeft -= 1;
            }
            if (positionOnCol >= 6) {
                offsetFromTop -= positionOnCol - 5;
            }
            if (positionOnCol >= 8) {
                offsetFromTop -= 2;
            }
            if (positionOnCol >= 9) {
                offsetFromTop -= 1;
            }
        } else if (col == 5) {
            offsetFromLeft = 281;
            offsetFromTop = 13 + positionOnCol * 44;
            if (positionOnCol >= 5) {
                offsetFromTop += 2;
            }
            if (positionOnCol >= 10) {
                offsetFromTop += 2;
            }
        } else if (col == 6) {
            offsetFromLeft = 333.75;
            offsetFromTop = 33 + positionOnCol * 49;
            if (positionOnCol >= 4) {
                offsetFromTop += positionOnCol - 3;
            }
            if (positionOnCol >= 6) {
                offsetFromTop -= positionOnCol - 5;
            }
            if (positionOnCol >= 8) {
                offsetFromTop -= 3;
            }
            if (positionOnCol >= 9) {
                offsetFromTop -= (positionOnCol - 8);
            }
        } else if (col == 7) {
            offsetFromLeft = 386;
            offsetFromTop = 70 + positionOnCol * 53.5;
            if (positionOnCol >= 3) {
                offsetFromTop = offsetFromTop - 2 - (positionOnCol - 4) * 1.5;
            }
            if (positionOnCol >= 4) {
                offsetFromTop -= positionOnCol - 3;
            }
            if (positionOnCol >= 6) {
                offsetFromTop -= 2;
            }
            if (positionOnCol == 8) {
                offsetFromTop -= 2;
            }
        } else if (col == 8) {
            offsetFromLeft = 438;
            offsetFromTop = 109 + positionOnCol * 57;
            if (positionOnCol >= 4) {
                offsetFromTop -= 3;
            }
            if (positionOnCol == 6) {
                offsetFromTop -= 3;
            }
        } else if (col == 9) {
            offsetFromLeft = 489.75;
            offsetFromTop = 155 + positionOnCol * 62.75;
            if (positionOnCol >= 3) {
                offsetFromTop -= 2 * (positionOnCol - 2);
            }
            if (positionOnCol == 4) {
                offsetFromTop -= 1;
            }
        } else if (col == 10) {
            offsetFromLeft = 543.75;
            offsetFromTop = 206 + positionOnCol * 75;
            if (positionOnCol == 2) {
                offsetFromTop -= 2;
            }
        }

        AnchorPane.setTopAnchor(piece, offsetFromTop);
        AnchorPane.setLeftAnchor(piece, offsetFromLeft);
        if (piece.sceneProperty().getValue() == null) {
            if (isWhitePiece) {
                whitePieceLayer.getChildren().add(piece);
            } else {
                pieceLayer.getChildren().add(piece);
            }
        }
    }

    public void initializeDisplay() {
        startPane.setFocusTraversable(true);
        stage.setTitle("Can't Stop");
        stage.setResizable(false);

        StackPane loading = new StackPane();
        ImageView image = new ImageView("images/boardgameimage.jpg");
        image.setFitHeight(screenHeight);
        image.setFitWidth(screenWidth);
        loading.getChildren().add(image);
        stage.setScene(new Scene(loading, screenWidth, screenHeight));

        stage.show();

        if (notAlreadyStarted) {
            notAlreadyStarted = false;
            Thread t = new Thread(new ProgramRunnable());
            t.start();
        }
    }

    /**
     * assumes that curPlayer is 0, 1, 2, or 3
     * @param board
     * @param curPlayer
     * @param placeholders
     * @param negativeStartingPositions
     */
    public void updateDisplayOfBoard(int[][] board, final int curPlayer,
                                     final List<Integer> placeholders, int[] negativeStartingPositions) {
        if (GENERATE_TRANSCRIPT) {
            transcript.updateDisplayOfBoard(board, curPlayer, placeholders, negativeStartingPositions);
        }
        int[][] newBoard = new int[board.length][11];
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                newBoard[i][j] = -1 * board[i][j];
            }
        }
        int[] tempStartingPositions = new int[negativeStartingPositions.length];
        for (int i = 0; i < negativeStartingPositions.length; i++) {
            tempStartingPositions[i] = -1 * negativeStartingPositions[i];
        }
        final int[] startingPositions = tempStartingPositions;

        final int[][] arrayOfPlayerPositionArrays = newBoard;
        final int[] defaultValues = this.defaultBoardValues;
        readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                pieceLayer.getChildren().clear();
                whitePieceLayer.getChildren().clear();

                for (int i = 0; i < arrayOfPlayerPositionArrays.length; i++) {
                    int[] playerPositionArray = arrayOfPlayerPositionArrays[i];
                    int placeholdersPassedSoFar = 0;
                    for (int col = 0; col < playerPositionArray.length; col++) {
                        if (playerPositionArray[col] != defaultValues[col]) {
                            if (curPlayer == i) {
                                if (placeholders.contains(new Integer(col + 2)) &&
                                    startingPositions[col] > playerPositionArray[col]) {
                                    positionPieceOnBoard(imageHolder.getPlaceholder(placeholdersPassedSoFar),
                                        true, col, playerPositionArray[col]);
                                    if (startingPositions[col] != defaultValues[col]) {
                                        positionPieceOnBoard(imageHolder.getPiece(playerColorsInOrder[curPlayer], col + 2),
                                            false, col, startingPositions[col]);
                                    }
                                    placeholdersPassedSoFar++;
                                } else if (!placeholders.contains(new Integer(col + 2)) &&
                                    startingPositions[col] != playerPositionArray[col]) {
                                    positionPieceOnBoard(imageHolder.getPiece(playerColorsInOrder[curPlayer], col + 2),
                                        false, col, playerPositionArray[col]);
                                }
                            } else {
                                positionPieceOnBoard(imageHolder.getPiece(playerColorsInOrder[i], col + 2),
                                    false, col, playerPositionArray[col]);
                            }
                        }
                    }
                }

                // and now just update the position of the pieces from previous turns for curPlayer
                // (just the ones that were never, and aren't, associated with a placeholder this turn
                for (int col = 0; col < startingPositions.length; col++) {
                    if (startingPositions[col] != defaultValues[col] &&
                        startingPositions[col] == arrayOfPlayerPositionArrays[curPlayer][col]) {
                        positionPieceOnBoard(imageHolder.getPiece(playerColorsInOrder[curPlayer], col + 2),
                            false, col, startingPositions[col]);
                    }
                }
                readyToMoveOn.set(true);
            }
        });
        waitForRunningTaskToFinish(false);
    }

    public void showWhoseTurnItIs(final int player, boolean computerPlaying) {
        if (GENERATE_TRANSCRIPT) {
            transcript.showWhoseTurnItIs(player, computerPlaying);
        }
        readyToMoveOn.set(false);
        if (player == 0 && computerPlaying) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    setTurnDisplayBoxColor(playerColorsInOrder[player]);
                    turnDisplayText.setText("Computer's Turn");
                    readyToMoveOn.set(true);
                }
            });
        } else if (!computerPlaying) {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    setTurnDisplayBoxColor(playerColorsInOrder[player]);
                    turnDisplayText.setText(getNameForPlayer(player) + "'s turn:");
                    readyToMoveOn.set(true);
                }
            });
        } else if ((playerNames == null || playerNames[player] == null ||
                    playerNames[player].equals(playerColorsInOrder[player].substring(0, 1).toUpperCase() +
                                               playerColorsInOrder[player].substring(1))) &&
                   (numPlayers == 2 || numPlayers == 1)){
            // if they didn't provide a name and there's either just one person, or one person playing
            // against the computer
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    setTurnDisplayBoxColor(playerColorsInOrder[player]);
                    turnDisplayText.setText("Your Turn");
                    readyToMoveOn.set(true);
                }
            });
        } else {
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    setTurnDisplayBoxColor(playerColorsInOrder[player]);
                    turnDisplayText.setText(getNameForPlayer(player) + "'s Turn");
                    readyToMoveOn.set(true);
                }
            });
        }
        waitForRunningTaskToFinish(false);
    }

    private String getNameForPlayer(int i) {
        if (playerNames == null || playerNames[i] == null) {
            return (playerColorsInOrder[i].substring(0, 1).toUpperCase() + playerColorsInOrder[i].substring(1));
        } else {
            return playerNames[i];
        }
    }

    private void waitForRunningTaskToFinish(boolean isComputerPadding) {
        Future<Boolean> future = staller.submit(new WaitingTask(this.readyToMoveOn, isComputerPadding));
        try {
            future.get();
        } catch (Exception e) {
        }
    }

    /**
     *
     * @param roll
     * @param possibleMoves
     * @param placeholders
     * @param computersChoice: if this is -1, it's not actually the computer's choice (it's the user's turn)
     * @return
     */
    public int getChoiceAboutDicePairing(final int[] roll, final List<int[]> possibleMoves, final List<Integer> placeholders,
                                         final int computersChoice) {
        if (GENERATE_TRANSCRIPT) {
            transcript.getChoiceAboutDicePairing(roll, possibleMoves, placeholders, computersChoice);
        }
        this.readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (diceOnScreenForDisplayRoll.get()) {
                    for (int i = 0; i < 4; i++) {
                        diceList.get(i).setAcceptsInput(true);
                    }
                    dicePanel.getChildren().add(diceButton);
                    gamePane.getChildren().remove(dicePanel);
                    diceOnScreenForDisplayRoll.set(false);
                }
                // set up the screen to accept choices
                for (int i = 0; i < 4; i++) {
                    diceList.get(i).setFaceValue(roll[i]);
                    resetDiceToUnclicked();
                }
                gamePane.getChildren().add(dicePanel);
                if (computersChoice == -1) {
                    instructionsText.setText("Pair the dice by color.");
                    diceButton.setText("Enter Choice");
                }
                readyToMoveOn.set(true);
            }
        });
        waitForRunningTaskToFinish(false);

        //System.out.println("curScreen: " + curScreen.get());

        int optionUserPicked = -1;
        if (computersChoice == -1) {
            // wait for user input, process it, and make sure it's valid
            boolean userHasGivenValidAnswer = false;
            while (!userHasGivenValidAnswer) {
                // wait until user has entered choice
                this.readyToMoveOn.set(false);
                waitForRunningTaskToFinish(false);

                boolean somethingWrongWithUsersCurrentAnswer = false;
                int numOfClickedDice = 0;
                int numOfUnclickedDice = 0;
                for (int i = 0; i < diceList.size(); i++) {
                    if (diceList.get(i).isSelected()) {
                        numOfClickedDice++;
                    } else {
                        numOfUnclickedDice++;
                    }
                }
                if (stopGame.get()) {
                    userHasGivenValidAnswer = true;
                } else if ((numOfClickedDice != 2 || numOfUnclickedDice != 2) && !stopGame.get()) {
                    somethingWrongWithUsersCurrentAnswer = true;
                    displayMessage("Dice must be paired into\ntwo groups of two.");
                    this.readyToMoveOn.set(false);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            resetDiceToUnclicked();
                            readyToMoveOn.set(true);
                        }
                    });
                    waitForRunningTaskToFinish(false);
                }
                if (!somethingWrongWithUsersCurrentAnswer && !stopGame.get()) {
                    int[] userSelection = new int[2];
                    for (VisibleDie die : diceList) {
                        if (die.isSelected()) {
                            userSelection[0] += die.getFaceValue();
                        } else {
                            userSelection[1] += die.getFaceValue();
                        }
                    }
                    // check for exact match
                    for (int i = 0; i < possibleMoves.size(); i++) {
                        int[] validMove = possibleMoves.get(i);
                        if ((validMove[0] == userSelection[0] && validMove[1] == userSelection[1]) ||
                            (validMove[0] == userSelection[1] && validMove[1] == userSelection[0])) {
                            optionUserPicked = i;
                            userHasGivenValidAnswer = true;
                            break;
                        }
                    }
                    // if no exact match exists, check for partial match
                    if (!userHasGivenValidAnswer) {
                        int[] partialMatchIndices = {-1, -1};
                        for (int i = 0; i < possibleMoves.size(); i++) {
                            int[] validMove = possibleMoves.get(i);
                            if (validMove[0] == userSelection[0] || validMove[1] == userSelection[1] ||
                                validMove[0] == userSelection[1] || validMove[1] == userSelection[0]) {
                                if (partialMatchIndices[0] == -1) {
                                    partialMatchIndices[0] = i;
                                } else {
                                    partialMatchIndices[1] = i;
                                }
                                userHasGivenValidAnswer = true;
                            }
                        }
                        if (userHasGivenValidAnswer && partialMatchIndices[1] == -1) {
                            optionUserPicked = partialMatchIndices[0];
                        } else if (userHasGivenValidAnswer) {
                            // there are two partial matches. Figure out which one the user wants.
                            int columnFromFirst = -1;
                            int columnFromSecond = -1;
                            if (possibleMoves.get(partialMatchIndices[0])[0] != -1) {
                                columnFromFirst = possibleMoves.get(partialMatchIndices[0])[0];
                            } else {
                                columnFromFirst = possibleMoves.get(partialMatchIndices[0])[1];
                            }
                            if (possibleMoves.get(partialMatchIndices[1])[0] != -1) {
                                columnFromSecond = possibleMoves.get(partialMatchIndices[1])[0];
                            } else {
                                columnFromSecond = possibleMoves.get(partialMatchIndices[1])[1];
                            }
                            if (columnFromFirst == columnFromSecond) {
                                optionUserPicked = partialMatchIndices[0];
                            } else {
                                this.columnOneButton.setText("Column " + columnFromFirst);
                                this.columnTwoButton.setText("Column " + columnFromSecond);

                                // put column choice box onto screen
                                this.readyToMoveOn.set(false);
                                Platform.runLater(new Runnable() {
                                    @Override
                                    public void run() {
                                        gamePane.getChildren().remove(dicePanel);
                                        instructionsText.setText("Put piece on which column?");
                                        gamePane.getChildren().add(columnChoiceBox);
                                        readyToMoveOn.set(true);
                                    }
                                });
                                waitForRunningTaskToFinish(false);

                                // wait for user input
                                this.readyToMoveOn.set(false);
                                waitForRunningTaskToFinish(false);
                                if (columnOnePressed.get()) {
                                    optionUserPicked = partialMatchIndices[0];
                                } else {
                                    optionUserPicked = partialMatchIndices[1];
                                }
                            }
                        }
                    }
                    if (userHasGivenValidAnswer) {
                        displayMessage(userSelection[0] + " and " + userSelection[1] + "\nchosen.");
                    } else {
                        displayMessage("Dice pairing cannot be\nchosen to miss when\nmaking progress is possible.");
                        this.readyToMoveOn.set(false);
                        Platform.runLater(new Runnable() {
                            @Override
                            public void run() {
                                resetDiceToUnclicked();
                                readyToMoveOn.set(true);
                            }
                        });
                        waitForRunningTaskToFinish(false);
                    }
                }
            }
        } else {
            // computer has already picked dice pairing and other column, so just present it

            readyToMoveOn.set(false);
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    int[] onlyChoice = possibleMoves.get(computersChoice);
                    // figure out how to pair off the dice and visually pair them by color
                    int[] diceToUse = new int[2];
                    boolean foundTheDice = false;
                    boolean onlyOneProgress = true;
                    // figure out the dice that is involved in producing onlyChoice
                    for (int i = 0; i < 3; i++) {
                        int curVal = diceList.get(i).getFaceValue();
                        for (int j = i + 1; j < 4; j++) {
                            int[] otherTwoDice = {-1, -1};
                            for (int k = 0; k < 4; k++) {
                                if (i != k && j != k) {
                                    if (otherTwoDice[0] == -1) {
                                        otherTwoDice[0] = k;
                                    } else {
                                        otherTwoDice[1] = k;
                                    }
                                }
                            }
                            if (curVal + diceList.get(j).getFaceValue() == onlyChoice[0] &&
                                diceList.get(otherTwoDice[0]).getFaceValue() + diceList.get(otherTwoDice[1]).getFaceValue() == onlyChoice[1]) {
                                diceToUse[0] = i;
                                diceToUse[1] = j;
                                foundTheDice = true;
                                onlyOneProgress = false;
                                instructionsText.setText("Computer's choice:");
                                break;
                            }
                        }
                        if (foundTheDice) {
                            break;
                        }
                    }
                    // if we still haven't found the dice, we have only an inexact match that leaves out
                    // onlyChoice[0] or onlyChoice[1], so look for what contains those
                    if (!foundTheDice) {
                        for (int i = 0; i < 3; i++) {
                            int curVal = diceList.get(i).getFaceValue();
                            for (int j = i + 1; j < 4; j++) {
                                if (curVal + diceList.get(j).getFaceValue() == onlyChoice[0]) {
                                    diceToUse[0] = i;
                                    diceToUse[1] = j;
                                    foundTheDice = true;


                                    int[] otherTwoDice = {-1, -1};
                                    for (int k = 0; k < 4; k++) {
                                        if (i != k && j != k) {
                                            if (otherTwoDice[0] == -1) {
                                                otherTwoDice[0] = k;
                                            } else {
                                                otherTwoDice[1] = k;
                                            }
                                        }
                                    }
                                    int sumOfOtherDice = diceList.get(otherTwoDice[0]).getFaceValue() +
                                                         diceList.get(otherTwoDice[1]).getFaceValue();
                                    if (!placeholders.contains(new Integer(onlyChoice[0])) &&
                                        !placeholders.contains(new Integer(sumOfOtherDice))) {
                                        instructionsText.setText("Computer's choice:");
                                    } else {
                                        instructionsText.setText("Computer's choice:");
                                    }
                                    break;
                                }
                            }
                            if (foundTheDice) {
                                break;
                            }
                        }
                    }
                    if (!foundTheDice) {
                        for (int i = 0; i < 3; i++) {
                            int curVal = diceList.get(i).getFaceValue();
                            for (int j = i + 1; j < 4; j++) {
                                if (curVal + diceList.get(j).getFaceValue() == onlyChoice[1]) {
                                    diceToUse[0] = i;
                                    diceToUse[1] = j;
                                    foundTheDice = true;

                                    int[] otherTwoDice = {-1, -1};
                                    for (int k = 0; k < 4; k++) {
                                        if (i != k && j != k) {
                                            if (otherTwoDice[0] == -1) {
                                                otherTwoDice[0] = k;
                                            } else {
                                                otherTwoDice[1] = k;
                                            }
                                        }
                                    }
                                    int sumOfOtherDice = diceList.get(otherTwoDice[0]).getFaceValue() +
                                                         diceList.get(otherTwoDice[1]).getFaceValue();
                                    if (!placeholders.contains(new Integer(onlyChoice[1])) &&
                                        !placeholders.contains(new Integer(sumOfOtherDice))) {
                                        instructionsText.setText("Computer's choice:");
                                    } else {
                                        instructionsText.setText("Computer's choice:");
                                    }

                                    break;
                                }
                            }
                            if (foundTheDice) {
                                break;
                            }
                        }
                    }
                    for (int i = 0; i < 4; i++) {
                        if (i == diceToUse[0] || i == diceToUse[1]) {
                            diceList.get(i).fire();
                        } else if (onlyOneProgress) {
                            diceList.get(i).grayOut();
                        }
                        diceList.get(i).setAcceptsInput(false);
                    }
                    diceButton.setText("OK");
                    readyToMoveOn.set(true);
                }
            });
            waitForRunningTaskToFinish(false);

            readyToMoveOn.set(false);
            waitForRunningTaskToFinish(true);
        }


        // clean up
        this.readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (dicePanel.sceneProperty().getValue() != null) {
                    gamePane.getChildren().remove(dicePanel);
                } else {
                    gamePane.getChildren().remove(columnChoiceBox);
                }
                readyToMoveOn.set(true);
                for (int i = 0; i < diceList.size(); i++) {
                    diceList.get(i).setAcceptsInput(true);
                }
            }
        });
        waitForRunningTaskToFinish(false);
        if (GENERATE_TRANSCRIPT && computersChoice == -1) {
            transcript.print("" + optionUserPicked, true);
        }
        return optionUserPicked;
    }

    private void resetDiceToUnclicked() {
        for (VisibleDie die : diceList) {
            die.ungrayOut();
            if (die.isSelected()) {
                die.fire();
            }
        }
    }

    /**
     * Only to be used outside of UI-modifying code-- otherwise, use instructionsText.setText()
     * @param message
     */
    private void displayMessage(final String message) {
        final AtomicBoolean updatingMessage = new AtomicBoolean();
        updatingMessage.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                instructionsText.setText(message);
                updatingMessage.set(true);
            }
        });
        Future<Boolean> future = staller.submit(new WaitingTask(updatingMessage, false));
        try {
            future.get();
        } catch (Exception e) {
        }
    }

    public void showWhatOnlyDiceOptionIs(final int[] roll, final boolean isMiss, List<Integer> placeholders,
                                         final int[] onlyChoice, final boolean isComputer) {
        if (GENERATE_TRANSCRIPT) {
            transcript.showWhatOnlyDiceOptionIs(roll, isMiss, placeholders, onlyChoice, isComputer);
        }
        this.readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (diceOnScreenForDisplayRoll.get()) {
                    for (int i = 0; i < 4; i++) {
                        diceList.get(i).setAcceptsInput(true);
                    }
                    dicePanel.getChildren().add(diceButton);
                    gamePane.getChildren().remove(dicePanel);
                    diceOnScreenForDisplayRoll.set(false);
                }
                for (int i = 0; i < 4; i++) {
                    diceList.get(i).setFaceValue(roll[i]);
                    resetDiceToUnclicked();
                }
                if (isMiss) {
                    if (!isComputer) {
                        instructionsText.setText("You missed.");
                    } else {
                        instructionsText.setText("The computer missed.");
                    }
                    for (int i = 0; i < 4; i++) {
                        diceList.get(i).setAcceptsInput(false);
                    }
                    for (int i = 0; i < 4; i++) {
                        diceList.get(i).grayOut();
                        diceList.get(i).setAcceptsInput(false);
                    }
                } else {
                    if (!isComputer) {
                        instructionsText.setText("Only possible pairing:");
                    } else {
                        instructionsText.setText("Computer's choice:");
                    }
                    // figure out how to pair off the dice and visually pair them by color
                    int[] diceToUse = new int[2];
                    boolean foundTheDice = false;
                    boolean onlyOneProgress = true;
                    // figure out the dice that are involved in producing onlyChoice
                    for (int i = 0; i < 3; i++) {
                        int curVal = diceList.get(i).getFaceValue();
                        for (int j = i + 1; j < 4; j++) {
                            int[] otherTwoDice = {-1, -1};
                            for (int k = 0; k < 4; k++) {
                                if (i != k && j != k) {
                                    if (otherTwoDice[0] == -1) {
                                        otherTwoDice[0] = k;
                                    } else {
                                        otherTwoDice[1] = k;
                                    }
                                }
                            }
                            if (curVal + diceList.get(j).getFaceValue() == onlyChoice[0] &&
                                diceList.get(otherTwoDice[0]).getFaceValue() + diceList.get(otherTwoDice[1]).getFaceValue() == onlyChoice[1]) {
                                diceToUse[0] = i;
                                diceToUse[1] = j;
                                foundTheDice = true;
                                onlyOneProgress = false;
                                break;
                            }
                        }
                        if (foundTheDice) {
                            break;
                        }
                    }
                    // if we still haven't found the dice, we have only an inexact match that leaves out
                    // onlyChoice[0] or onlyChoice[1], so look for what contains those
                    if (!foundTheDice) {
                        for (int i = 0; i < 3; i++) {
                            int curVal = diceList.get(i).getFaceValue();
                            for (int j = i + 1; j < 4; j++) {
                                if (curVal + diceList.get(j).getFaceValue() == onlyChoice[0]) {
                                    diceToUse[0] = i;
                                    diceToUse[1] = j;
                                    foundTheDice = true;
                                    break;
                                }
                            }
                            if (foundTheDice) {
                                break;
                            }
                        }
                    }
                    if (!foundTheDice) {
                        for (int i = 0; i < 3; i++) {
                            int curVal = diceList.get(i).getFaceValue();
                            for (int j = i + 1; j < 4; j++) {
                                if (curVal + diceList.get(j).getFaceValue() == onlyChoice[1]) {
                                    diceToUse[0] = i;
                                    diceToUse[1] = j;
                                    foundTheDice = true;
                                    break;
                                }
                            }
                            if (foundTheDice) {
                                break;
                            }
                        }
                    }
                    for (int i = 0; i < 4; i++) {
                        if (i == diceToUse[0] || i == diceToUse[1]) {
                            diceList.get(i).fire();
                        } else if (onlyOneProgress) {
                            diceList.get(i).grayOut();
                        }
                        diceList.get(i).setAcceptsInput(false);
                    }
                }
                diceButton.setText("OK");
                gamePane.getChildren().add(dicePanel);
                readyToMoveOn.set(true);
            }
        });
        waitForRunningTaskToFinish(false);


        // wait for them to click OK button
        this.readyToMoveOn.set(false);
        if (isComputer) {
            waitForRunningTaskToFinish(true);
        } else {
            waitForRunningTaskToFinish(false);
        }

        // clean up
        this.readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                instructionsText.setText("Rolling again...");
                gamePane.getChildren().remove(dicePanel);
                for (int i = 0; i < 4; i++) {
                    diceList.get(i).setAcceptsInput(true);
                }
                readyToMoveOn.set(true);
            }
        });
        waitForRunningTaskToFinish(false);
    }

    /**
     *
     * @param computersChoice: -1 if user's turn, 0 if false, 1 if true
     * @return
     */
    public boolean getChoiceWhetherToRollAgain(final int computersChoice) {
        if (GENERATE_TRANSCRIPT) {
            transcript.getChoiceWhetherToRollAgain(computersChoice);
        }
        // set up the screen to accept choices
        this.readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                if (computersChoice == -1) {
                    gamePane.getChildren().add(rollAgainOrStopBox);
                    instructionsText.setText("Roll again?");
                } else if (computersChoice == 0){
                    instructionsText.setText("Computer will stop rolling.");
                } else if (computersChoice == 1) {
                    instructionsText.setText("Computer will roll again.");
                }
                readyToMoveOn.set(true);
            }
        });
        waitForRunningTaskToFinish(false);

        boolean optionUserPicked = false;
        if (computersChoice == -1) {
            // wait for user input, process it, and make sure it's valid
            boolean userHasGivenValidAnswer = false;
            while (!userHasGivenValidAnswer) {
                // wait until user has entered choice
                this.readyToMoveOn.set(false);
                waitForRunningTaskToFinish(false);
                optionUserPicked = rollAgain.get();

                // check that user is allowed to stop (i.e., placeholder not on top of someone else)
                // NOT CURRENTLY IMPLEMENTED
                userHasGivenValidAnswer = true;

                if (userHasGivenValidAnswer) {
                    if (optionUserPicked) {
                        displayMessage("Rolling again...");
                    } else {
                        displayMessage("Ending your turn.");
                    }
                } else {
                    displayMessage("You cannot stop rolling when\none of your placeholders is\non top of another piece.");
                    this.readyToMoveOn.set(false);
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            resetDiceToUnclicked();
                            readyToMoveOn.set(true);
                        }
                    });
                    waitForRunningTaskToFinish(false);
                }
            }

            this.readyToMoveOn.set(false);
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    gamePane.getChildren().remove(rollAgainOrStopBox);
                    readyToMoveOn.set(true);
                }
            });
            waitForRunningTaskToFinish(false);
        } else {
            this.readyToMoveOn.set(false);
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    gamePane.getChildren().add(computerOKPane);
                    readyToMoveOn.set(true);
                }
            });
            waitForRunningTaskToFinish(false);

            this.readyToMoveOn.set(false);
            waitForRunningTaskToFinish(true);

            this.readyToMoveOn.set(false);
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    gamePane.getChildren().remove(computerOKPane);
                    readyToMoveOn.set(true);
                }
            });
            waitForRunningTaskToFinish(false);
        }

        if (GENERATE_TRANSCRIPT && computersChoice == -1) {
            if (optionUserPicked) {
                transcript.print("yes", true);
            } else {
                transcript.print("no", true);
            }
        }
        return optionUserPicked;
    }

    public void displayRoll(final int[] roll) {
        if (GENERATE_TRANSCRIPT) {
            transcript.displayRoll(roll);
        }
        readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 4; i++) {
                    diceList.get(i).setFaceValue(roll[i]);
                    resetDiceToUnclicked();
                    diceList.get(i).setAcceptsInput(false);
                }
                dicePanel.getChildren().remove(diceButton);
                if (!gamePane.getChildren().contains(dicePanel)) {
                    gamePane.getChildren().add(dicePanel);
                }
                readyToMoveOn.set(true);

                diceOnScreenForDisplayRoll.set(true);
            }
        });
        waitForRunningTaskToFinish(false);
    }

    @Override
    public void handle(KeyEvent keyEvent){
        KeyCode code = keyEvent.getCode();
        if (dicePanel.sceneProperty().getValue() != null) {
            if (code == KeyCode.U) {
                diceList.get(0).fire();
            } else if (code == KeyCode.I) {
                diceList.get(1).fire();
            } else if (code == KeyCode.J) {
                diceList.get(2).fire();
            } else if (code == KeyCode.K) {
                diceList.get(3).fire();
            } else if (code == KeyCode.ENTER) {
                diceButton.fire();
            }
        } else if (rollAgainOrStopBox.sceneProperty().getValue() != null) {
            if (code == KeyCode.U) {
                rollAgainButton.fire();
            } else if (code == KeyCode.J) {
                stopButton.fire();
            }
        } else if (columnChoiceBox.sceneProperty().getValue() != null) {
            if (code == KeyCode.U) {
                columnOneButton.fire();
            } else if (code == KeyCode.J) {
                columnTwoButton.fire();
            }
        } else if (computerOKPane.sceneProperty().getValue() != null) {
            if (code == KeyCode.ENTER) {
                computerOKButton.fire();
            }
        } else if (startButton.sceneProperty().getValue() != null) {
            if (code == KeyCode.ENTER) {
                startButton.fire();
            }
        }
        if (playAgainButton.sceneProperty().getValue() != null) {
            if (code == KeyCode.ENTER) {
                playAgainButton.fire();
            }
        }
    }

    public void onStartButton(ActionEvent actionEvent) {
        runSettingsScreens.set(false);
        shouldStartGame = true;
        curScreen.set(1);
        computerIsPlaying = true;
        numPlayers = 2;
        feedbackCode = 1;
        orderOfPlayers = null;
        game.setCompIsPlayingNumPlayersAndOrder(computerIsPlaying, numPlayers, orderOfPlayers, feedbackCode);
        readyToMoveOn.set(true);
    }

    private class ProgramRunnable implements Runnable {
        public void run() {
            stopGame = new AtomicBoolean(false);
            readyToMoveOn.set(false);
            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    stage.setScene(startScene);
                    readyToMoveOn.set(true);
                }
            });
            waitForRunningTaskToFinish(false);

            // wait for user to click a button
            readyToMoveOn.set(false);
            waitForRunningTaskToFinish(false);

            while (true) {
                stopGame.set(false);
                if (runSettingsScreens.get()) {
                    runSettingsScreenSequence();
                }

                // put game scene on screen
                final AtomicBoolean setupIsDone = new AtomicBoolean();
                setupIsDone.set(false);
                Platform.runLater(new Runnable() {
                    @Override
                    public void run() {
                        // PUT RESTART BUTTON BACK ON GAME SCREEN HERE
                        stage.setScene(gameScene);
                        gamePane.setFocusTraversable(true);
                        setupIsDone.set(true);
                    }
                });
                Future<Boolean> future = staller.submit(new WaitingTask(setupIsDone, false));
                try {
                    future.get();
                } catch (Exception e) {
                    System.out.println("1");
                }

                if (GENERATE_TRANSCRIPT) {
                    transcript = new CommandLineGameInterface();
                    transcript.setGame(game);
                    transcript.setAsTranscript();
                }
                System.out.println("Starting new game");
                gameRunner = Executors.newFixedThreadPool(8);
                future = gameRunner.submit(new GameCallable());
                try {
                    future.get();
                } catch (Exception e) {
                    System.err.println("In GameCallable:\n" + e);
                    e.printStackTrace();
                }
                System.out.println("Bypassing");
                if (GENERATE_TRANSCRIPT) {
                    transcript.close();
                }
            }
        }
    }

    private void backToSettings() {
        runSettingsScreens.set(true);
        stopGame();
    }

    private void rematch() {
        runSettingsScreens.set(false);
        stopGame();
    }

    private void stopGame() {
        stopGame.set(true);
    }

    public void rollAgainButtonPressed(ActionEvent e) {
        rollAgain.set(true);
        enterChoice(e);
    }

    public void stopRollingButtonPressed(ActionEvent e) {
        rollAgain.set(false);
        enterChoice(e);
    }

    public void columnOneButtonPressed(ActionEvent e) {
        columnOnePressed.set(true);
        enterChoice(e);
    }

    public void columnTwoButtonPressed(ActionEvent e) {
        columnOnePressed.set(false);
        enterChoice(e);
    }

    public void enterChoice(ActionEvent e) {
        this.readyToMoveOn.set(true);
    }

    private class GameCallable implements Callable<Boolean> {
        @Override
        public Boolean call() throws Exception {
            game.playGame(stopGame);
            return true;
        }
    }

    public void setGame(Game game) {
        this.game = game;
    }

    private void setTurnDisplayBoxColor(String color) {
        if (color.equals("blue")) {
            this.turnDisplayBox.setBackground(new Background(new BackgroundFill(Color.rgb(52, 94, 209),
                CornerRadii.EMPTY, Insets.EMPTY)));
        } else if (color.equals("orange")) {
            this.turnDisplayBox.setBackground(new Background(new BackgroundFill(Color.rgb(250, 103, 5),
                CornerRadii.EMPTY, Insets.EMPTY)));
        } else if (color.equals("green")) {
            this.turnDisplayBox.setBackground(new Background(new BackgroundFill(Color.GREEN,
                CornerRadii.EMPTY, Insets.EMPTY)));
        } else if (color.equals("yellow")) {
            this.turnDisplayBox.setBackground(new Background(new BackgroundFill(Color.rgb(255, 206, 8),
                CornerRadii.EMPTY, Insets.EMPTY)));
        }
    }

    public void displayWin(final int player, final boolean computerPlaying) {
        if (GENERATE_TRANSCRIPT) {
            transcript.displayWin(player, computerPlaying);
        }
        this.readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                gamePane.getChildren().remove(restartGameButton);
                setTurnDisplayBoxColor(playerColorsInOrder[player]);
                instructionsText.setText("");
                if (player == 0 && computerPlaying) {
                    turnDisplayText.setText("The computer won!");
                } else if (computerPlaying) {
                    turnDisplayText.setText("Congratulations, you beat the computer!");
                } else {
                    turnDisplayText.setText("Congratulations player " + (player + 1) + ", you won!");
                }
                gamePane.getChildren().add(playAgainPane);
                readyToMoveOn.set(true);
            }
        });
        waitForRunningTaskToFinish(false);

        // wait for play again button to be clicked
        readyToMoveOn.set(false);
        waitForRunningTaskToFinish(false);

        this.readyToMoveOn.set(false);
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                gamePane.getChildren().remove(playAgainPane);
                gamePane.getChildren().add(restartGameButton);
                readyToMoveOn.set(true);
            }
        });
        waitForRunningTaskToFinish(false);
    }
}