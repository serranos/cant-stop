import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public class Game
{
	public final int[] defaultValues = { 3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3 };
	private final boolean USE_GUI;
	private GameInterface gameInterface;

	private boolean COMPUTER_IS_PLAYING = true;
	private int numPlayers = 2;
	private int[] playerIndices = { 0, 1 };
	
	final int CAPTURED = -50;
	int[][] board;
	private boolean gameHasBeenWon;
	private RolloutDecisionMaker dm;

	public Game(boolean use_gui)
	{
		USE_GUI = use_gui;
		gameHasBeenWon = false;
		board = new int[numPlayers][11];
		if (!USE_GUI)
		{
			gameInterface = new CommandLineGameInterface();
			gameInterface.setGame(this);
		}
		if (COMPUTER_IS_PLAYING)
		{
			dm = new RolloutDecisionMaker();
			//dm = new NeuralNetworkDecisionMaker(false);
			//dm = new RandomDecisionMaker();
		}
	}

	public Game(int[][] b)
	{
		USE_GUI = false;
		numPlayers = b.length;
		board = new int[numPlayers][11];
		for (int i = 0; i < numPlayers; i++)
			for (int j = 0; j < 11; j++)
				board[i][j] = b[i][j];
		gameHasBeenWon = false;
		dm = null;
		playerIndices = null;

	}

	/**
	 *	0: no feedback
	 *  1: feedback but no extra errorless games
	 *  2: everything
	 */
	public void setCompIsPlayingNumPlayersAndOrder(boolean compPlaying, int numPlayers, int[] order, int feedbackCode)
	{
		COMPUTER_IS_PLAYING = compPlaying;
		this.numPlayers = numPlayers;
		if (order != null)
		{
			playerIndices = order;
		}
		else
		{
			randomizePlayerOrder();
		}
		board = new int[numPlayers][11];
		dm.feedbackCode = feedbackCode;
	}

	private void randomizePlayerOrder()
	{
		playerIndices = new int[numPlayers];
		List<Integer> playersWhoHaventBeenPickedYet = new ArrayList<Integer>();
		for (int i = 0; i < numPlayers; i++)
		{
			playersWhoHaventBeenPickedYet.add(i);
		}
		recursivelyFillOutOrder(numPlayers, playersWhoHaventBeenPickedYet);
	}

	private void recursivelyFillOutOrder(int numRemainingPlayers, List<Integer> playersWhoHaventBeenPickedYet)
	{
		if (numRemainingPlayers > 0)
		{
			int indexOfPlayerWhoGoesNext = -1;
			double interval = 1.0 / numRemainingPlayers;
			double randomlyChosenNum = Math.random();
			for (int i = 1; i <= numRemainingPlayers; i++)
			{
				if (randomlyChosenNum >= (i - 1) * interval && randomlyChosenNum <= i * interval)
				{
					indexOfPlayerWhoGoesNext = i - 1;
				}
			}
			playerIndices[numPlayers - numRemainingPlayers] = playersWhoHaventBeenPickedYet
					.get(indexOfPlayerWhoGoesNext);
			playersWhoHaventBeenPickedYet.remove(indexOfPlayerWhoGoesNext);
			recursivelyFillOutOrder(numRemainingPlayers - 1, playersWhoHaventBeenPickedYet);
		}
	}

	public void setGraphicalInterface(GraphicalUserGameInterface gui)
	{
		this.gameInterface = gui;
	}

	public int[] rollDice()
	{
		int[] returnDice = { (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1,
				(int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1 };
		Arrays.sort(returnDice);
		return returnDice;
	}

	public List<int[]> getPossiblePairings(int[] dice)
	{
		List<int[]> ret = new ArrayList<int[]>();
		int totalSum = dice[0] + dice[1] + dice[2] + dice[3];

		for (int i = 1; i < 4; i++)
		{
			int sum1 = dice[0] + dice[i];
			int sum2 = totalSum - sum1;
			int[] newComb = new int[2];
			if (sum1 < sum2)
			{
				newComb[0] = sum1;
				newComb[1] = sum2;
			}
			else
			{
				newComb[0] = sum2;
				newComb[1] = sum1;
			}
			boolean in = false;

			for (int[] comb : ret)
			{
				if (newComb[0] == comb[0] && newComb[1] == comb[1])
				{
					in = true;
					break;
				}
			}
			if (!in)
				ret.add(newComb);
		}
		return ret;
	}

	public void playGame(AtomicBoolean stop)
	{
		if (stop == null)
		{
			stop = new AtomicBoolean(false);
		}
		//initiate error file
		try
		{
			PrintWriter writer = new PrintWriter("cpp/comm/error_data.txt");
			writer.write(0 + " " + 0);
			writer.close();
			System.out.println("error of opponent initiated to 0");
		}
		catch(FileNotFoundException e)
		{
			System.out.println("error file couldn't be created");
		}
		instantiateBoard();
		int player = 0;
		gameHasBeenWon = false;
		boolean firstTimeThrough = true;
		while (!gameHasBeenWon)
		{
			for (int playerI = 0; playerI < numPlayers && !gameHasBeenWon; playerI++)
			{
				player = playerIndices[playerI];
				boolean willRollAgain = true;
				boolean notMissed = true;
				int[] startingPositions = getStartingPositions(player);
				List<Integer> placeholders = new ArrayList<Integer>();
				gameInterface.showWhoseTurnItIs(player, COMPUTER_IS_PLAYING);
				while (willRollAgain && notMissed)
				{
					if (stop.get())
						return;
					if (firstTimeThrough)
					{
						gameInterface.updateDisplayOfBoard(board, player, placeholders, startingPositions);
						firstTimeThrough = false;
					}
					int[] roll = rollDice();
					if ((player == 0) && COMPUTER_IS_PLAYING)
					{
						//System.out.println("Computer's turn -----------------------------");
						List<int[]> validMoves = getValidMoves(roll, placeholders, player);

						//System.out.println();
						if (validMoves.size() > 1)
						{
							gameInterface.displayRoll(roll);
							int maxIndex = dm.decideOnPair(player, validMoves, placeholders, board, startingPositions);
							if (stop.get())
								return;
							gameInterface.getChoiceAboutDicePairing(roll, validMoves, placeholders, maxIndex);
							int[] twoNumbersUserChose = validMoves.get(maxIndex);

							willRollAgain = makeMove(twoNumbersUserChose, player, placeholders);
							gameInterface.updateDisplayOfBoard(board, player, placeholders, startingPositions);
							
							if (!willRollAgain && checkGame(player))
							{
								gameHasBeenWon = true;
								gameInterface.displayWin(player, COMPUTER_IS_PLAYING);
								break;
							}
							if (!willRollAgain)
							{

								willRollAgain = dm.shouldRoll(player, board, placeholders, startingPositions);
								if (stop.get())
									return;
								if (willRollAgain)
								{
									gameInterface.getChoiceWhetherToRollAgain(1);
								}
								else
								{
									gameInterface.getChoiceWhetherToRollAgain(0);
								}
							}
						}
						else if (validMoves.size() == 1)
						{
							
							int[] onlyChoiceForTwoNumbers = validMoves.get(0);
							gameInterface.showWhatOnlyDiceOptionIs(roll, false, placeholders, onlyChoiceForTwoNumbers,
									true);
							willRollAgain = makeMove(onlyChoiceForTwoNumbers, player, placeholders);
							gameInterface.updateDisplayOfBoard(board, player, placeholders, startingPositions);
							if (!willRollAgain && checkGame(player))
							{
								gameHasBeenWon = true;
								gameInterface.displayWin(player, COMPUTER_IS_PLAYING);
								break;
							}
							if (!willRollAgain)
							{

								willRollAgain = dm.shouldRoll(player, board, placeholders, startingPositions);
								if (stop.get())
									return;
								if (willRollAgain)
								{
									gameInterface.getChoiceWhetherToRollAgain(1);
								}
								else
								{
									gameInterface.getChoiceWhetherToRollAgain(0);
								}
							}
						}
						else
						{
							gameInterface.showWhatOnlyDiceOptionIs(roll, true, placeholders, roll, true);
							notMissed = false;
							for (int i = 0; i < 11; i++)
							{
								board[player][i] = startingPositions[i];
							}
							/*gameInterface.updateDisplayOfBoard(board, player, new ArrayList<Integer>(),
									startingPositions);*/
						}
					}
					else
					{
						//System.out.println("Player's turn -----------------");
						List<int[]> validMoves = getValidMoves(roll, placeholders, player);
						if (validMoves.size() > 1)
						{
							int indexOfUserChoice = gameInterface.getChoiceAboutDicePairing(roll, validMoves,
									placeholders, -1);
							if (stop.get())
								return;
							int[] twoNumbersUserChose = validMoves.get(indexOfUserChoice);
							//System.out.println("You chose: " + twoNumbersUserChose[0] + "   "+ twoNumbersUserChose[1]);
							willRollAgain = makeMove(twoNumbersUserChose, player, placeholders);
							gameInterface.updateDisplayOfBoard(board, player, placeholders, startingPositions);
							if (!willRollAgain && checkGame(player))
							{
								gameHasBeenWon = true;
								gameInterface.displayWin(player, COMPUTER_IS_PLAYING);
								break;

							}
							if (!willRollAgain)
							{
								willRollAgain = gameInterface.getChoiceWhetherToRollAgain(-1);
								if (stop.get())
									return;
								if(!willRollAgain)
									dm.reportStop(board, placeholders, startingPositions);
							}
						}
						else if (validMoves.size() == 1)
						{
							int[] onlyChoiceForTwoNumbers = validMoves.get(0);
							gameInterface.showWhatOnlyDiceOptionIs(roll, false, placeholders, onlyChoiceForTwoNumbers,
									false);
							willRollAgain = makeMove(onlyChoiceForTwoNumbers, player, placeholders);
							gameInterface.updateDisplayOfBoard(board, player, placeholders, startingPositions);
							if (!willRollAgain && checkGame(player))
							{
								gameHasBeenWon = true;
								gameInterface.displayWin(player, COMPUTER_IS_PLAYING);
								break;
							}
							if (!willRollAgain)
							{
								willRollAgain = gameInterface.getChoiceWhetherToRollAgain(-1);
								if (stop.get())
									return;
								if(!willRollAgain)
									dm.reportStop(board, placeholders, startingPositions);
							}
						}
						else
						{
							gameInterface.showWhatOnlyDiceOptionIs(roll, true, placeholders, roll, false);
							notMissed = false;
							for (int i = 0; i < 11; i++)
							{
								board[player][i] = startingPositions[i];
							}
							gameInterface.updateDisplayOfBoard(board, player, new ArrayList<Integer>(),
									startingPositions);
						}
					}
					/*if (!willRollAgain)
					{
						gameInterface.updateDisplayOfBoard(board, player, new ArrayList<Integer>(), startingPositions);
					}*/
				}
				setCaptured(player, placeholders);
				gameInterface.updateDisplayOfBoard(board, player, new ArrayList<Integer>(), startingPositions);

			}
		}
	}

	private void setCaptured(int player, List<Integer> placeholders)
	{
		for (Integer p : placeholders)
		{
			if (board[player][p - 2] == 0)// that player won the column
			{
				for (int k = 0; k < board.length; k++)
				{
					if (k != player)
					{
						board[k][p - 2] = CAPTURED;// other players
						// can no longer
						// play on that
						// column
					}
				}
			}
		}

	}

	private boolean checkGame(int player)
	{
		int count = 0;
		for (int i = 0; i < board[player].length; i++)
		{
			if (board[player][i] == 0)
				count++;
		}
		return count >= 3;
	}

	public List<int[]> getValidMoves(int[] roll, List<Integer> placeholders, int player)
	{
		List<int[]> ret = new ArrayList<int[]>();
		List<int[]> possiblePairs = getPossiblePairings(roll);
		for (int[] pos : possiblePairs)
		{
			boolean has0 = placeholders.contains(pos[0]);
			boolean has1 = placeholders.contains(pos[1]);

			if (board[player][pos[0] - 2] == 0)
				has0 = false;
			if (board[player][pos[1] - 2] == 0)
				has1 = false;
			if (has0 && has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = pos[1];
				ret.add(newMove);
			}
			else if (has0)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
						newMove[1] = pos[1];
				}
				ret.add(newMove);
			}
			else if (has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[1];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						newMove[1] = pos[0];
				}
				ret.add(newMove);
			}
			else
			{
				if (placeholders.size() < 2)
				{
					int[] newMove = new int[2];
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						newMove[0] = pos[0];
					else
						newMove[0] = -1;
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
						newMove[1] = pos[1];
					else
						newMove[1] = -1;
					if (newMove[0] != -1 || newMove[1] != -1)
						ret.add(newMove);
				}
				else if (placeholders.size() < 3)
				{
					if (pos[0] != pos[1])
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = -1;
							ret.add(newMove);
						}
						if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
						{
							int[] otherMove = new int[2];
							otherMove[0] = pos[1];
							otherMove[1] = -1;
							ret.add(otherMove);
						}
					}
					else
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = pos[0];
							ret.add(newMove);
						}

					}
				}
			}
		}

		return ret;
	}

	public boolean makeMove(int[] choice, int player, List<Integer> placeholders)
	{
		for (int i = 0; i < 2; i++)
		{
			if (choice[i] != -1)
			{
				if (placeholders.contains(choice[i]))
				{
					if (board[player][choice[i] - 2] != 0)
						board[player][choice[i] - 2]++;
				}
				else
				{
					placeholders.add(choice[i]);
					board[player][choice[i] - 2]++;

				}
			}
		}
		boolean hasToRoll = false;
		for (int p = 0; p < board.length && !hasToRoll; p++)
		{
			for (int p2 = p + 1; p2 < board.length && !hasToRoll; p2++)
			{
				for (int i = 0; i < board[0].length && !hasToRoll; i++)
				{
					if (board[p][i] != -1 * defaultValues[i] && board[p][i] != CAPTURED && board[p2][i] == board[p][i])
					{
						hasToRoll = true;
					}
				}
			}
		}
		return hasToRoll;
	}

	public void instantiateBoard()
	{
		for (int i = 0; i < 11; i++)
		{
			for (int j = 0; j < board.length; j++)
			{
				if (i == 0)
				{
					board[j][i] = -3;
				}
				else if (i == 1)
				{
					board[j][i] = -5;
				}
				else if (i == 2)
				{
					board[j][i] = -7;
				}
				else if (i == 3)
				{
					board[j][i] = -9;
				}
				else if (i == 4)
				{
					board[j][i] = -11;
				}
				else if (i == 5)
				{
					board[j][i] = -13;
				}
				else if (i == 6)
				{
					board[j][i] = -11;
				}
				else if (i == 7)
				{
					board[j][i] = -9;
				}
				else if (i == 8)
				{
					board[j][i] = -7;
				}
				else if (i == 9)
				{
					board[j][i] = -5;
				}
				else
				{
					board[j][i] = -3;
				}
			}
		}
	}

	public int[] getStartingPositions(int player)
	{
		int[] returnArray = new int[11];
		for (int i = 0; i < 11; i++)
		{
			returnArray[i] = board[player][i];
		}
		return returnArray;
	}

	public void encodeBoard(int[] startingPositions)
	{
		Date date = new java.util.Date();
		Timestamp curTimestamp = new Timestamp(date.getTime());
		String time = curTimestamp.toString();
		PrintWriter file;
		try
		{
			file = new PrintWriter(new File("Encodings/Encoding-" + time + ".txt"));
		}
		catch (FileNotFoundException e)
		{
			System.err.println("Encoding creation failed.");
			return;
		}

		int noOfBits = 0;
		boolean first = true;

		for (int i = 0; i < defaultValues.length; i++)
		{
			noOfBits += 3;
			if (!first)
				file.print("\t");
			//file.print("n");//use for testing (easier to read the file)
			if (board[0][i] == 0)
				file.print(0 + "\t" + 0 + "\t" + 1);
			else if (board[1][i] == 0)
				file.print(0 + "\t" + 1 + "\t" + 0);
			else
				file.print(1 + "\t" + 0 + "\t" + 0);
			first = false;

			for (int j = 1; j <= defaultValues[i]; j++)
			{
				file.print("\t");
				file.print("\n");//use for testing (easier to read the file)
				noOfBits += 5;
				if (board[0][i] + defaultValues[i] == j)//one is on
				{
					if (startingPositions[i] == board[0][i])//one permanently on
					{
						file.print(0 + "\t" + 0 + "\t" + 0 + "\t" + 0 + "\t" + 1);
					}
					else //one got on this turn
					{
						if (board[1][i] + defaultValues[i] == j)//two permanently on
						{
							file.print(0 + "\t" + 0 + "\t" + 0 + "\t" + 1 + "\t" + 0);
						}
						else
						{
							file.print(0 + "\t" + 0 + "\t" + 1 + "\t" + 0 + "\t" + 0);
						}
					}
				}
				else//one is off
				{
					if (board[1][i] + defaultValues[i] == j)//two permanently on
					{
						file.print(0 + "\t" + 1 + "\t" + 0 + "\t" + 0 + "\t" + 0);
					}
					else
					{
						file.print(1 + "\t" + 0 + "\t" + 0 + "\t" + 0 + "\t" + 0);
					}
				}
			}
		}
		file.close();
		System.out.println(noOfBits);
	}
}
