import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.ArrayList;
import java.util.List;

public class RolloutDecisionMaker extends DecisionMaker
{
	private void createFile(boolean rollReport, int action, List<int[]> availableMoves, int[][] board, List<Integer> placeholders,
			int[] startingPositions, int feedbackCode)
	{
		try
		{
			PrintWriter writer = new PrintWriter("cpp/comm/call_to_cpp.txt");
			writer.print(action + " ");
			for (int i = 0; i < 2; i++)
				for (int j = 0; j < 11; j++)
					writer.print(board[i][j] + " ");
			writer.print(placeholders.size() + " ");
			for (int i : placeholders)
				writer.print(i + " ");
			if(!rollReport)
			{
				writer.print((availableMoves.size() * 2) + " ");
				for (int[] pair : availableMoves)
					writer.print(pair[0] + " " + pair[1] + " ");
				if(feedbackCode == 2)
					writer.print(1 + " ");
				else
					writer.print(0 + " ");
			}		
			for (int i = 0; i < 11; i++)
				writer.print(startingPositions[i] + " ");
			writer.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
	}

	private int callCpp()
	{
		ProcessBuilder pb;
		if(new File("call").isFile())
			pb = new ProcessBuilder("./call");
		else
			pb = new ProcessBuilder(".\\call.exe");
		if(feedbackCode != 0)
		{
			pb.redirectOutput(Redirect.INHERIT);
			pb.redirectError(Redirect.INHERIT);
		}
		Process p;
		int retValue = -1;
		try
		{
			p = pb.start();
			retValue = p.waitFor();
			if(feedbackCode != 0)
				System.out.println("exited with: " + retValue);
		}
		catch (IOException|InterruptedException e)
		{
			e.printStackTrace();
		}
		return retValue;        
	}

	private int reportCpp()
	{
		ProcessBuilder pb;
		if(new File("exam").isFile())
			pb = new ProcessBuilder("./exam");
		else
			pb = new ProcessBuilder(".\\exam.exe");
		
		if(feedbackCode != 0)
		{
			pb.redirectOutput(Redirect.INHERIT);
			pb.redirectError(Redirect.INHERIT);
		}

		Process p;
		int retValue = -1;
		try
		{
			p = pb.start();
			retValue = p.waitFor();
			if(feedbackCode != 0)
				System.out.println("exited with: " + retValue);
		}
		catch (IOException|InterruptedException e)
		{
			e.printStackTrace();
		}

		return retValue;        
	}

	@Override
	public boolean shouldRoll(int player, int[][] board, List<Integer> placeholders, int[] startingPositions)
	{
		createFile(false, 0, new ArrayList<int[]>(), board, placeholders, startingPositions, feedbackCode);
		int val = callCpp();	
		return (val == 1);
	}

	@Override
	public int decideOnPair(int player, List<int[]> availableMoves, List<Integer> placeholders, int[][] board,
			int[] startingPos)
	{
		createFile(false, 1, availableMoves, board, placeholders, startingPos, feedbackCode);
		int val = callCpp();
		if(val/2 < availableMoves.size())	
			return val / 2;
		System.out.println("Error!");
		return 0;
	}

	public void reportStop(int[][] board, List<Integer> placeholders, int[] startingPos)
	{
		createFile(true, 0, null, board, placeholders, startingPos, feedbackCode);
		reportCpp();
	}
}
