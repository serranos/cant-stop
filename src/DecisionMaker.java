import java.util.ArrayList;
import java.util.List;

public abstract class DecisionMaker
{
	public int feedbackCode;
	
	public List<int[]> getPossiblePairings(int[] dice)
	{
		List<int[]> ret = new ArrayList<int[]>();
		int totalSum = dice[0] + dice[1] + dice[2] + dice[3];

		for (int i = 1; i < 4; i++)
		{
			int sum1 = dice[0] + dice[i];
			int sum2 = totalSum - sum1;
			int[] newComb = new int[2];
			if (sum1 < sum2)
			{
				newComb[0] = sum1;
				newComb[1] = sum2;
			}
			else
			{
				newComb[0] = sum2;
				newComb[1] = sum1;
			}
			boolean in = false;

			for (int[] comb : ret)
			{
				if (newComb[0] == comb[0] && newComb[1] == comb[1])
				{
					in = true;
					break;
				}
			}
			if (!in)
				ret.add(newComb);
		}
		return ret;
	}
	
	public abstract boolean shouldRoll(int player, int[][] board, List<Integer> placeholders, int[] startingPositions);

	
	public abstract int decideOnPair(int player, List<int[]> availableMoves, List<Integer> placeholders, int[][] board, int[] startingPos);

	public void reportStop(int[][] board, List<Integer> placeholders, int[] startingPositions) {}
	
}
