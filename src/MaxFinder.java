import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class MaxFinder
{
	Scanner scan;

	public MaxFinder()
	{
		scan = null;
		try
		{
			scan = new Scanner(new File("SimulationResultsPairingChoices.txt"));
		}
		catch (FileNotFoundException e)
		{
			System.out.println("No luck loading file.");
		}
	}

	public void findAndPrintMaxRow()
	{
		String maxRow = null;
		int maxSum = -1;
		String curRow;
		String curSubstring;
		int curSum;
		while (scan.hasNextLine())
		{
			curRow = scan.nextLine();
			curSubstring = curRow;
			for (int i = 0; i < 4; i++)
			{
				curSubstring = curSubstring.substring(curSubstring.indexOf(",") + 2);
			}
			curSum = Integer.parseInt(curSubstring);
			if (curSum > maxSum)
			{
				maxRow = curRow;
				maxSum = curSum;
			}
		}
		System.out.println(maxRow);
	}

	public static void main(String[] args)
	{
		MaxFinder mf = new MaxFinder();
		mf.findAndPrintMaxRow();
	}
}