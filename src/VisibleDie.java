import javafx.scene.control.ToggleButton;

/**
 * Created by sofiaserrano on 6/19/16.
 */
public class VisibleDie extends ToggleButton {
    private ImageHolder imageHolder;
    private int faceValue;
    private final int dieNumber;
    private boolean acceptsInput;

    private String red = "#ff6666";
    private String blue = "#33adff";

    private String clickedColor = blue;
    private String unclickedColor = red;

    public VisibleDie(ImageHolder imageHolder, int dieNumber) {
        super("", imageHolder.getNextAvailableDieImage(1, dieNumber));
        this.imageHolder = imageHolder;
        faceValue = 1;
        this.setStyle("-fx-base: " + unclickedColor + ";");
        this.dieNumber = dieNumber;
        acceptsInput = true;
    }

    public void setFaceValue(int face) {
        if (isSelected()) {
            fire();
        }
        if (face == 1) {
            this.setGraphic(imageHolder.getNextAvailableDieImage(1, dieNumber));
            faceValue = 1;
        } else if (face == 2) {
            this.setGraphic(imageHolder.getNextAvailableDieImage(2, dieNumber));
            faceValue = 2;
        } else if (face == 3) {
            this.setGraphic(imageHolder.getNextAvailableDieImage(3, dieNumber));
            faceValue = 3;
        } else if (face == 4) {
            this.setGraphic(imageHolder.getNextAvailableDieImage(4, dieNumber));
            faceValue = 4;
        } else if (face == 5) {
            this.setGraphic(imageHolder.getNextAvailableDieImage(5, dieNumber));
            faceValue = 5;
        } else if (face == 6) {
            this.setGraphic(imageHolder.getNextAvailableDieImage(6, dieNumber));
            faceValue = 6;
        }
    }

    public int getFaceValue() {
        return faceValue;
    }

    @Override
    public void fire() {
        if (acceptsInput) {
            super.fire();
            if (isSelected()) {
                this.setStyle("-fx-base: " + clickedColor + ";");
            } else {
                this.setStyle("-fx-base: " + unclickedColor + ";");
            }
        }
    }

    public void setAcceptsInput(boolean accept) {
        acceptsInput = accept;
        if (accept) {
            this.setDisable(false);
        }
    }

    public boolean getAcceptsInput() {
        return acceptsInput;
    }

    public void grayOut() {
        this.setStyle("-fx-base: #f2f2f2;");
        this.setDisable(true);
    }

    public void ungrayOut() {
        if (isSelected()) {
            this.setStyle("-fx-base: " + clickedColor + ";");
        } else {
            this.setStyle("-fx-base: " + unclickedColor + ";");
        }
    }

    public double getHeightAndWidth() {
        return this.imageHolder.getDieHeightAndWidth();
    }
}
