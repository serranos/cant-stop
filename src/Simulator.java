import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Simulator
{
	int weight68 = 834;
	int weight7 = 727;
	
	public int[] rollDice()
	{
		int[] returnDice = { (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1,
				(int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1 };
		return returnDice;
	}

	public List<int[]> getPossiblePairings(int[] dice)
	{
		List<int[]> ret = new ArrayList<int[]>();
		int totalSum = dice[0] + dice[1] + dice[2] + dice[3];

		for (int i = 1; i < 4; i++)
		{
			int sum1 = dice[0] + dice[i];
			int sum2 = totalSum - sum1;
			int[] newComb = new int[2];
			if (sum1 < sum2)
			{
				newComb[0] = sum1;
				newComb[1] = sum2;
			}
			else
			{
				newComb[0] = sum2;
				newComb[1] = sum1;
			}
			boolean in = false;

			for (int[] comb : ret)
			{
				if (newComb[0] == comb[0] && newComb[1] == comb[1])
				{
					in = true;
					break;
				}
			}
			if (!in)
				ret.add(newComb);
		}
		return ret;
	}

	/**
	 * Here's what I think we had in mind for the hierarchy of moves:
	 * Best:  progress two in columns {6, 8} (possibly the same one of those)
	 *        progress one in column {6, 8}, progress one in column 7
	 *        progress two in column 7
	 * Worst: all other moves
	 * BUT I think there are a few bugs in making this happen, particularly around lines
	 * 91 and 92 (I suspect references to curMove are supposed to be to selectedMove there).
	 * Returns -1 if no move making progress in column 6, 7, or 8 is found.
	 */
	private int[] chooseHowToPairDice(List<int[]> possibleMoves) {
		int selectedIndex = -1;
		for (int i = 0; i < moves.size(); i++)
		{
			int[] curMove = moves.get(i);
			if (curMove[0] == 6 || curMove[0] == 7 || curMove[0] == 8 || curMove[1] == 6 || curMove[1] == 7
					|| curMove[1] == 8)
			{
				// if they're the same (or they're both in {6, 8})
				if (curMove[0] == curMove[1] || (curMove[0] == 6 && curMove[1] == 8)
						|| (curMove[0] == 8 && curMove[1] == 6))
				{
					if (selectedIndex == -1)
						selectedIndex = i;
					else if (moves.get(selectedIndex)[0] == 7 || moves.get(selectedIndex)[1] == 7)
					{
						selectedIndex = i;
					}
				}
				// they're not the same, but one of the things that got our attention was a 6 or 8
				else if (curMove[0] == 6 || curMove[0] == 8 || curMove[1] == 6 || curMove[1] == 8)
				{
					if (selectedIndex == -1)
						selectedIndex = i;
					else
					{
						int[] selectedMove = moves.get(selectedIndex);
						if (selectedMove[0] != selectedMove[1] && !((curMove[0] == 6 && curMove[1] == 8)
								|| (curMove[0] == 8 && curMove[1] == 6)))
						{
							boolean onlySeven = true;
							for (int j = 0; j < 2; j++)
							{
								if (selectedMove[j] == 6 || selectedMove[j] == 8)
								{
									onlySeven = false; // there's at least one 6 or 8 in selectedMove,
									                   // and selectedMove isn't two of the same thing
									if ((j == 0 && selectedMove[1] != 7) || (j == 1 && selectedMove[0] != 7))
										selectedIndex = i;
								}
							}
							if (onlySeven)
								selectedIndex = i;
						}
					}
				}
				// a move with one 7 is still better than nothing
				else if (selectedIndex == -1)
				{
					selectedIndex = i;
				}
			}
		}
	}
	
	
	
	/**
	 * We don't allow any individual turn to make more progress than allowed by the
	 * following cap:
	 *      (limit7 * weight7) + (limit68 * weight68)
	 */
	public int[] playTurn(int limit7, int limit68)
	{
		int currentProgress7 = 0, currentProgress68 = 0;
		boolean done = false;
		while (!done)
		{
			if (currentProgress7*weight7 + currentProgress68*weight68 < limit7*weight7 + limit68*weight68)
			{
				List<int[]> moves = getPossiblePairings(rollDice());
				int selectedIndex = chooseHowToPairDice(moves);
				if (selectedIndex == -1) // i.e., if this turn would make no progress on 7, 6, or 8
				{
					done = true;
					currentProgress7 = 0;
					currentProgress68 = 0;
				}
				else
				{
					int[] selectedMove = moves.get(selectedIndex);
					for (int j = 0; j < 2; j++)
					{
						if (selectedMove[j] == 7)
							currentProgress7++;
						else if (selectedMove[j] == 6 || selectedMove[j] == 8)
							currentProgress68++;
					}
				}
			}
			else
			{
				done = true;
			}
		}
		int[] ret = { currentProgress7, currentProgress68 };
		return ret;
	}

	public long[] playSimulatedGame(int limit7, int limit68)
	{
		long progress7 = 0;
		long progress68 = 0;
		for (int i = 0; i < 1000000; i++)
		{
			System.out.println(i);
			int[] turnProgress = playTurn(limit7, limit68);
			progress7 += turnProgress[0];
			progress68 += turnProgress[1];
		}
		long[] ret = { progress7, progress68 };
		return ret;
	}

	/**
	 * Prints to a file of length 23*14; each line is four numbers - first num
	 * is limit to progress in 7 - second num is limit to progress in 6 and 8
	 * combined - third num is progress in 7 - fourth num is progress in 6 and 8
	 * combined
	 */
	public void runFullSimulation()
	{
		int upperLim7 = 14;
		PrintWriter toFile = null;
		try
		{
			toFile = new PrintWriter("SimulationResultsPairingChoices.txt");
		}
		catch (FileNotFoundException e)
		{
			System.out.println("Error in starting output file.");
			return;
		}

		long[] singleGameResults;
		
		int maxIndex = 23;
		for (int limit7 = 1; limit7 <= upperLim7; limit7++)
		{
			System.out.println(maxIndex);
			long[] maxGameResults = playSimulatedGame(limit7, maxIndex);
			for (int limit68 = maxIndex - 1; limit68 > 0; limit68--)
			{
				singleGameResults = playSimulatedGame(limit7, limit68);
				if(weight68*singleGameResults[1] + weight7*singleGameResults[0] > weight68*maxGameResults[1] + weight7*maxGameResults[0])
				{
					maxIndex = limit68;
					maxGameResults = singleGameResults;
				}
					
			}
			toFile.println(limit7 + ", " + maxIndex + ", " + maxGameResults[0] + ", " + maxGameResults[1]
					+ ", " + (((double)(weight7*maxGameResults[0] + weight68*maxGameResults[1]))/(weight7+weight68)));
		}
		
		//long[] maxGameResults = playSimulatedGame(0, 0);
		//toFile.println(0 + ", " + 0 + ", " + maxGameResults[0] + ", " + maxGameResults[1]
		//	+ ", " + (((double)(w7*maxGameResults[0] + w68*maxGameResults[1]))/(w7+w68)));

		toFile.close();
	}

	public static void main(String[] args)
	{
		Simulator test = new Simulator();
		test.runFullSimulation();
	}
}