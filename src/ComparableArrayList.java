import java.util.ArrayList;

/**
 * Created by sofiaserrano on 6/29/16.
 */
public class ComparableArrayList<E extends Comparable<E>> extends ArrayList<E> implements Comparable<ComparableArrayList<E>> {
    /**
	 * 
	 */
	private static final long serialVersionUID = 7136978145188441110L;

	public ComparableArrayList() {
        super();
    }

    /**
     * Sorts the lists in "alphabetical" order (the first one to have a thing that comes later goes later,
     * and if they're exactly the same except for length,
     * @param otherList
     * @return
     * @throws NullPointerException
     */
    public int compareTo(ComparableArrayList<E> otherList) throws NullPointerException {
        if (otherList == null) {
            throw new NullPointerException();
        }
        if (otherList.size() >= this.size()) {
            for (int i = 0; i < this.size(); i++) {
                if (this.get(i).compareTo(otherList.get(i)) > 0) {
                    return 1;
                } else if (this.get(i).compareTo(otherList.get(i)) < 0) {
                    return -1;
                }
            }
            if (otherList.size() > this.size()) {
                return -1;
            } else {
                return 0;
            }
        } else {
            for (int i = 0; i < otherList.size(); i++) {
                if (this.get(i).compareTo(otherList.get(i)) > 0) {
                    return 1;
                } else if (this.get(i).compareTo(otherList.get(i)) < 0) {
                    return -1;
                }
            }
            return 1;
        }
    }
}
