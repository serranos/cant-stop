import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ExperimentGameRunning
{
	private final int[] DEFAULT_VALUES = { 3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3 };
	private final int CAPTURED = -100;
	private ProbabilisticDecisionMaker pdm;
	private int wonOneColumnFirst, wonTwoColumnsFirst;
	private int[] captureCounts;
	Random rand = new Random();

	public ExperimentGameRunning()
	{
		pdm = new ProbabilisticDecisionMaker();
		captureCounts = new int[2];
	}

	public int[] playAutomatedGames(int numTimes)
	{
		int countOfWinGameFollowingWinOneColFirst = 0;
		int countOfWinGameFollowingWinTwoColFirst = 0;
		int player1wins = 0;
		int player1missesOnFirstTurn = 0;
		int player1makesfirstProgress = 0;

		int numPlayers = 2;
		boolean gameHasBeenWon;
		int[][] board = new int[numPlayers][DEFAULT_VALUES.length];
		// boolean invest = false;
		for (int turn = 0; turn < numTimes; turn++)
		{
			/*
			 * if(turn == 7) invest = true;
			 */
			// System.out.println("playing turn: " + turn);
			wonOneColumnFirst = -1;
			wonTwoColumnsFirst = -1;
			captureCounts[0] = 0;
			captureCounts[1] = 0;

			boolean firstTurn = true;
			boolean firstProgressMade = false;
			gameHasBeenWon = false;
			instantiateBoard(board);

			int player = 0;
			while (!gameHasBeenWon)
			{
				for (player = 0; player < numPlayers && !gameHasBeenWon; player++)
				{
					boolean willRollAgain = true, notMissed = true;

					int[] startingPositions = getStartingPositions(player, board);
					List<Integer> placeholders = new ArrayList<Integer>();

					while (willRollAgain && notMissed)
					{
						int[] roll = rollDice();
						List<int[]> validMoves = getValidMoves(roll, placeholders, player, board);
						if (validMoves.size() > 1)
						{
							int maxIndex = pdm.decideOnPairOriginal(player, validMoves, board, startingPositions);

							int[] twoNumbersUserChose = validMoves.get(maxIndex);
							willRollAgain = makeMove(twoNumbersUserChose, player, placeholders, board);

							if (!willRollAgain)
							{
								if (checkGame(player, board))
								{
									gameHasBeenWon = true;
									break;
								}

								willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
							}
						}
						else if (validMoves.size() == 1)
						{
							/*
							 * if(invest) System.out.println();
							 */
							int[] onlyChoiceForTwoNumbers = validMoves.get(0);
							willRollAgain = makeMove(onlyChoiceForTwoNumbers, player, placeholders, board);

							if (!willRollAgain)
							{
								if (checkGame(player, board))
								{
									gameHasBeenWon = true;
									break;
								}

								willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
							}
						}
						else
						{
							if (firstTurn) player1missesOnFirstTurn++;
							notMissed = false;
							for (int i = 0; i < 11; i++)
							{
								board[player][i] = startingPositions[i];
							}
						}
					}
					if (notMissed && !firstProgressMade)
					{
						if (player == 0) player1makesfirstProgress++;
						firstProgressMade = true;
					}
					firstTurn = false;
					setCaptured(player, placeholders, board);
				}
			}

			int winnerIndex;
			if (player == 1) winnerIndex = 0;
			else winnerIndex = 1;

			if (winnerIndex == 0) player1wins++;

			if (winnerIndex == wonOneColumnFirst) countOfWinGameFollowingWinOneColFirst++;
			if (winnerIndex == wonTwoColumnsFirst) countOfWinGameFollowingWinTwoColFirst++;
		}
		int[] ret = { countOfWinGameFollowingWinOneColFirst, countOfWinGameFollowingWinTwoColFirst, player1wins, player1missesOnFirstTurn, player1makesfirstProgress };
		return ret;
	}

	private int[] getStartingPositions(int player, int[][] board)
	{
		int[] returnArray = new int[11];
		for (int i = 0; i < 11; i++)
		{
			returnArray[i] = board[player][i];
		}
		return returnArray;
	}

	private boolean checkGame(int player, int[][] board)
	{
		int count = 0;
		for (int i = 0; i < 11; i++)
		{
			if (board[player][i] == 0) count++;
		}
		return count >= 3;
	}

	private List<int[]> getValidMoves(int[] roll, List<Integer> placeholders, int player, int[][] board)
	{
		List<int[]> ret = new ArrayList<int[]>();
		List<int[]> possiblePairs = pdm.getPossiblePairings(roll);
		for (int[] pos : possiblePairs)
		{
			boolean has0 = placeholders.contains(pos[0]);
			boolean has1 = placeholders.contains(pos[1]);

			if (board[player][pos[0] - 2] == 0) has0 = false;
			if (board[player][pos[1] - 2] == 0) has1 = false;
			if (has0 && has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = pos[1];
				ret.add(newMove);
			}
			else if (has0)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0) newMove[1] = pos[1];
				}
				ret.add(newMove);
			}
			else if (has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[1];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0) newMove[1] = pos[0];
				}
				ret.add(newMove);
			}
			else
			{
				if (placeholders.size() < 2)
				{
					int[] newMove = new int[2];
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0) newMove[0] = pos[0];
					else newMove[0] = -1;
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0) newMove[1] = pos[1];
					else newMove[1] = -1;
					if (newMove[0] != -1 || newMove[1] != -1) ret.add(newMove);
				}
				else if (placeholders.size() < 3)
				{
					if (pos[0] != pos[1])
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = -1;
							ret.add(newMove);
						}
						if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
						{
							int[] otherMove = new int[2];
							otherMove[0] = pos[1];
							otherMove[1] = -1;
							ret.add(otherMove);
						}
					}
					else
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = pos[0];
							ret.add(newMove);
						}

					}
				}
			}
		}
		return ret;
	}

	private boolean makeMove(int[] choice, int player, List<Integer> placeholders, int[][] board)
	{
		for (int i = 0; i < 2; i++)
		{
			if (choice[i] != -1)
			{
				if (placeholders.contains(choice[i]))
				{
					if (board[player][choice[i] - 2] != 0) board[player][choice[i] - 2]++;
				}
				else
				{
					placeholders.add(choice[i]);
					board[player][choice[i] - 2]++;

				}
			}
		}
		for (int p = 0; p < board.length; p++)
		{
			for (int p2 = p + 1; p2 < board.length; p2++)
			{
				for (int i = 0; i < board[0].length; i++)
				{
					if (board[p][i] != -1 * DEFAULT_VALUES[i] && board[p2][i] == board[p][i])
					{
						return true;
					}
				}
			}
		}
		return false;
	}

	private void setCaptured(int player, List<Integer> placeholders, int[][] board)
	{
		for (Integer p : placeholders)
		{
			if (board[player][p - 2] == 0)// that player won the column
			{
				if (captureCounts[0] == 0 && captureCounts[1] == 0)
				{
					wonOneColumnFirst = player;
				}
				else if ((player == 0 && captureCounts[0] == 1 && captureCounts[1] != 2) || (player == 1 && captureCounts[1] == 1 && captureCounts[0] != 2))
				{
					wonTwoColumnsFirst = player;
				}

				captureCounts[player]++;

				for (int k = 0; k < board.length; k++)
				{
					if (k != player)
					{
						board[k][p - 2] = CAPTURED;
					}
				}
			}
		}
	}

	private int[] rollDice()
	{
		int[] returnDice = { rand.nextInt(6) + 1, rand.nextInt(6) + 1, rand.nextInt(6) + 1, rand.nextInt(6) + 1 };
		return returnDice;
	}

	private void instantiateBoard(int[][] board)
	{
		for (int i = 0; i < 11; i++)
		{
			for (int j = 0; j < board.length; j++)
			{
				if (i == 0)
				{
					board[j][i] = -3;
				}
				else if (i == 1)
				{
					board[j][i] = -5;
				}
				else if (i == 2)
				{
					board[j][i] = -7;
				}
				else if (i == 3)
				{
					board[j][i] = -9;
				}
				else if (i == 4)
				{
					board[j][i] = -11;
				}
				else if (i == 5)
				{
					board[j][i] = -13;
				}
				else if (i == 6)
				{
					board[j][i] = -11;
				}
				else if (i == 7)
				{
					board[j][i] = -9;
				}
				else if (i == 8)
				{
					board[j][i] = -7;
				}
				else if (i == 9)
				{
					board[j][i] = -5;
				}
				else
				{
					board[j][i] = -3;
				}
			}
		}
	}

	public static void main(String[] args) throws FileNotFoundException
	{
		ExperimentGameRunning t2 = new ExperimentGameRunning();
		System.out.println(Arrays.toString(t2.playAutomatedGames(1000)));
	}
}