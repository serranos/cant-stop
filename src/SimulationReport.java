import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

/**
 * Created by sofiaserrano on 6/28/16.
 */
public class SimulationReport implements GameInterface {
    private OnePlayerSimulation sim;
    private PrintWriter toFile;
    private boolean setColumns;

    public SimulationReport(String filename, boolean setColumns) {
        try {
            toFile = new PrintWriter(new File(filename));
        } catch (FileNotFoundException e) {
            System.err.println(filename + " not created.");
        }
        this.setColumns = setColumns;
    }

    public void setOnePlayerSimulation(OnePlayerSimulation ops) {
        sim = ops;
    }

    public void updateDisplayOfBoard(int[][] arrayOfPlayerPositionArrays, int curPlayer, List<Integer> placeholders,
                              int[] startingPositions) {}

    /**
     *
     * @param roll
     * @param possibleMoves
     * @param placeholders
     * @param computersChoice
     *            (int) : if -1, it's actually the user's turn. Otherwise, it's
     *            the index the computer picked.
     * @return
     */
    public int getChoiceAboutDicePairing(int[] roll, List<int[]> possibleMoves, List<Integer> placeholders,
                                  int computersChoice) {
        return -1;
    }

    public void showWhatOnlyDiceOptionIs(int[] roll, boolean isMiss, List<Integer> placeholders, int[] onlyChoice,
                                  boolean isComputer) {

    }

    /**
     *
     * @param computersChoice
     *            (int) : if -1, it's actually the user's turn. If 0, computer
     *            said false, and if 1, computer said true
     * @return
     */
    public boolean getChoiceWhetherToRollAgain(int computersChoice) {
        return false;
    }

    public void displayRoll(int[] roll) {}

    public void setGame(Game game) {}

    public void showWhoseTurnItIs(int player, boolean computerPlaying) {}

    public void displayWin(int player, boolean computerPlaying) {
        if (setColumns) {
            toFile.println(Arrays.toString(sim.getLastSetOfColumnsOrRoll()) + " " + sim.getTotalTurns());
        } else {
            toFile.println(Arrays.toString(sim.getLastSetOfColumnsOrRoll()) + " " + Arrays.toString(sim.getInitialColumnChoice()));
        }
        toFile.flush();
    }
}
