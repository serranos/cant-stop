import java.util.List;
import java.util.Random;

public class RandomDecisionMaker extends DecisionMaker
{

	@Override
	public boolean shouldRoll(int player, int[][] board, List<Integer> placeholders, int[] startingPositions)
	{
		if(Math.random() > 0.5)
			return true;
		else
			return false;
	}

	@Override
	public int decideOnPair(int player, List<int[]> availableMoves, List<Integer> placeholders, int[][] board, int[] startingPos)
	{
		return new Random().nextInt(availableMoves.size());
	}

}
