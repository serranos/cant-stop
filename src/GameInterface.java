import java.util.List;

public interface GameInterface
{

	void updateDisplayOfBoard(int[][] arrayOfPlayerPositionArrays, int curPlayer, List<Integer> placeholders,
			int[] startingPositions);

	/**
	 *
	 * @param roll
	 * @param possibleMoves
	 * @param placeholders
	 * @param computersChoice
	 *            (int) : if -1, it's actually the user's turn. Otherwise, it's
	 *            the index the computer picked.
	 * @return
	 */
	int getChoiceAboutDicePairing(int[] roll, List<int[]> possibleMoves, List<Integer> placeholders,
			int computersChoice);

	void showWhatOnlyDiceOptionIs(int[] roll, boolean isMiss, List<Integer> placeholders, int[] onlyChoice,
			boolean isComputer);

	/**
	 *
	 * @param computersChoice
	 *            (int) : if -1, it's actually the user's turn. If 0, computer
	 *            said false, and if 1, computer said true
	 * @return
	 */
	boolean getChoiceWhetherToRollAgain(int computersChoice);

	void displayRoll(int[] roll);

	void setGame(Game game);

	void showWhoseTurnItIs(int player, boolean computerPlaying);

	void displayWin(int player, boolean computerPlaying);
}
