import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class FileGenerator
{
	static int faces = 5;
	static List<List<Integer>> diceCombos;
	static List<Integer> frequencies;

	public static void setDiceCombos()
	{
		diceCombos = new ArrayList<>();
		for (int i = 1; i < faces + 1; i++)
		{
			for (int j = 1; j < faces + 1; j++)
			{
				for (int k = 1; k < faces + 1; k++)
				{
					for (int h = 1; h < faces + 1; h++)
					{
						List<Integer> newSet = new ArrayList<>();
						newSet.add(i);
						newSet.add(j);
						newSet.add(k);
						newSet.add(h);
						boolean alreadyIn = false;
						for (List<Integer> savedSet : diceCombos)
						{
							if (listEquals(savedSet, newSet))
							{
								alreadyIn = true;
								break;
							}
						}
						if (!alreadyIn)
						{
							diceCombos.add(newSet);
						}
					}
				}
			}
		}
	}

	public static boolean listEquals(List<Integer> list1, List<Integer> list2)
	{
		ArrayList<Integer> temp1 = new ArrayList<>(list1);
		ArrayList<Integer> temp2 = new ArrayList<>(list2);

		for (Integer i : temp1)
		{
			temp2.remove((Integer) i);
		}

		if (temp2.size() == 0) return true;
		return false;
	}

	public static void setFile() throws FileNotFoundException
	{
		String diceAndWeight = "";
		PrintWriter writer = new PrintWriter(faces + "sidedDieCombsAndWeights.txt");
		for (int i = 0; i < diceCombos.size(); i++)
		{
			diceAndWeight = "";
			for (int j = 0; j < 4; j++)
			{
				diceAndWeight = diceAndWeight + diceCombos.get(i).get(j) + " ";
			}
			writer.print(diceAndWeight);
			if (i != diceCombos.size() - 1) writer.println(frequency(diceCombos.get(i)));
			else writer.print(frequency(diceCombos.get(i)));
		}
		writer.close();
	}

	public static int frequency(List<Integer> roll)
	{
		int[] elementFrequencies = new int[4];

		for (int i = 0; i < 4; i++)
			elementFrequencies[i] = 1;

		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				if (j != i && roll.get(i) == roll.get(j))
				{
					elementFrequencies[j]++;
				}
			}
		}

		int result = factorial(4);
		List<Integer> numbersOnDiceIncludedSoFar = new ArrayList<>();

		for (int i = 0; i < 4; i++)
		{
			if (!numbersOnDiceIncludedSoFar.contains(roll.get(i)))
			{
				numbersOnDiceIncludedSoFar.add(roll.get(i));
				result /= factorial(elementFrequencies[i]);
			}
		}

		return result;
	}

	public static int factorial(int n)
	{
		int fact = 1;
		for (int i = 1; i <= n; i++)
		{
			fact *= i;
		}
		return fact;
	}

	public static void main(String[] args) throws FileNotFoundException
	{
		setDiceCombos();
		setFile();
	}
}
