import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.neuroph.core.NeuralNetwork;
import org.neuroph.core.data.DataSet;
import org.neuroph.core.data.DataSetRow;
import org.neuroph.core.learning.SupervisedLearning;
import org.neuroph.nnet.MultiLayerPerceptron;
import org.neuroph.nnet.learning.BackPropagation;

public class NeuralNetworkDecisionMaker extends DecisionMaker
{
	private final String NETWORK_FILENAME = "network.nnet";
	private final int NUMBER_OF_INPUTS = 448;
	private final int CAPTURED = -100;
	private final int[] DEFAULT_VALUES = { 3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3 };
	private ProbabilisticDecisionMaker pdm;

	private NeuralNetwork<BackPropagation> nnet;

	public static void main(String[] args)
	{
		NeuralNetworkDecisionMaker nntrainer = new NeuralNetworkDecisionMaker(false);
		long start = System.currentTimeMillis();
		for (int i = 0; i < 20; i++)
		{
			System.out.println("Training for game #" + (i + 1));
			nntrainer.trainForOneGame();
		}
		long end = System.currentTimeMillis();
		nntrainer.saveNetwork();

		System.out.println((end - start));
	}

	public NeuralNetworkDecisionMaker(boolean firstCreation)
	{
		pdm = new ProbabilisticDecisionMaker();
		if (firstCreation) createNeuralNetworkForTraining();
		else createNeuralNetworkFromFile();
	}

	private void createNeuralNetworkForTraining()
	{
		nnet = new MultiLayerPerceptron(NUMBER_OF_INPUTS, 2 * NUMBER_OF_INPUTS + 1, 1);

		SupervisedLearning learningRule = nnet.getLearningRule();
		learningRule.setMaxError(0.01);
		learningRule.setLearningRate(0.5);
		learningRule.setMaxIterations(1000);
		/*
		 * learningRule.addListener(new LearningEventListener() { public void
		 * handleLearningEvent(LearningEvent learningEvent) { SupervisedLearning
		 * rule = (SupervisedLearning) learningEvent.getSource();
		 * System.out.println("Network error for interation " +
		 * rule.getCurrentIteration() + ": " + rule.getTotalNetworkError()); }
		 * });
		 */
	}

	@SuppressWarnings("unchecked")
	private void createNeuralNetworkFromFile()
	{
		nnet = NeuralNetwork.createFromFile(NETWORK_FILENAME);
	}

	public void saveNetwork()
	{
		nnet.save(NETWORK_FILENAME);
	}

	private void trainForOneGame()
	{
		DataSet trainingSet = new DataSet(NUMBER_OF_INPUTS, 1);
		ArrayList<ArrayList<double[]>> returnValues = playOneGame();
		boolean playerOneWon = (returnValues.get(2).get(0)[0] < 500);
		for (double[] encoding : returnValues.get(0))
		{
			double value;
			if (playerOneWon) value = 1;
			else value = 0;
			double[] result = { value };
			trainingSet.addRow(new DataSetRow(encoding, result));
		}
		for (double[] encoding : returnValues.get(1))
		{
			double value;
			if (playerOneWon) value = 0;
			else value = 1;
			double[] result = { value };
			trainingSet.addRow(new DataSetRow(encoding, result));
		}
		nnet.learn(trainingSet);
	}

	private ArrayList<ArrayList<double[]>> playOneGame()
	{
		ArrayList<ArrayList<double[]>> returnValues = new ArrayList<ArrayList<double[]>>(3);
		returnValues.add(new ArrayList<double[]>()); // moves of player one
		returnValues.add(new ArrayList<double[]>()); // moves of player two

		int numPlayers = 2;
		int noOfColumns = 11;
		int player = 0;
		boolean gameHasBeenWon = false;
		int[][] board = new int[numPlayers][noOfColumns];
		instantiateBoard(board);

		while (!gameHasBeenWon)
		{
			for (player = 0; player < numPlayers && !gameHasBeenWon; player++)
			{
				boolean willRollAgain = true, notMissed = true;
				int[] startingPositions = getStartingPositions(player, board);
				List<Integer> placeholders = new ArrayList<Integer>();

				while (willRollAgain && notMissed)
				{
					int[] roll = rollDice();
					List<int[]> validMoves = getValidMoves(roll, placeholders, player, board);
					if (validMoves.size() > 1)
					{
						int maxIndex = decideOnPair(player, validMoves, placeholders, board, startingPositions);
						int[] twoNumbersUserChose = validMoves.get(maxIndex);
						willRollAgain = makeMove(twoNumbersUserChose, player, placeholders, board);

						returnValues.get(player).add(encodeBoardAsDoubleArray(startingPositions, board, player));

						if (!willRollAgain)
						{
							if (checkGame(player, board))
							{
								gameHasBeenWon = true;
								break;
							}
							willRollAgain = shouldRoll(player, board, placeholders, startingPositions);
						}
					}
					else if (validMoves.size() == 1)
					{
						willRollAgain = makeMove(validMoves.get(0), player, placeholders, board);

						returnValues.get(player).add(encodeBoardAsDoubleArray(startingPositions, board, player));

						if (!willRollAgain)
						{
							if (checkGame(player, board))
							{
								gameHasBeenWon = true;
								break;
							}
							willRollAgain = shouldRoll(player, board, placeholders, startingPositions);
						}
					}
					else
					{
						notMissed = false;
						for (int i = 0; i < DEFAULT_VALUES.length; i++)
						{
							board[player][i] = startingPositions[i];
						}
					}
				}
				setCaptured(player, placeholders, board);
			}
		}

		ArrayList<double[]> returnListForWhoWon = new ArrayList<double[]>(1);
		if (player == 1) returnListForWhoWon.add(new double[] { 0 });
		else returnListForWhoWon.add(new double[] { 1000 });
		returnValues.add(returnListForWhoWon);

		return returnValues;
	}

	private double[] encodeBoardAsDoubleArray(final int[] startingPositions, final int[][] board, final int player)
	{
		double[] ret = new double[NUMBER_OF_INPUTS];
		int currentBit = 0;
		int opponent = (player == 0) ? 1 : 0;

		for (int i = 0; i < DEFAULT_VALUES.length; i++)
		{
			if (board[player][i] == 0)
			{
				ret[currentBit] = 0;
				currentBit++;
				ret[currentBit] = 0;
				currentBit++;
				ret[currentBit] = 1;
				currentBit++;
			}
			else if (board[opponent][i] == 0)
			{
				ret[currentBit] = 0;
				currentBit++;
				ret[currentBit] = 1;
				currentBit++;
				ret[currentBit] = 0;
				currentBit++;
			}
			else
			{
				ret[currentBit] = 1;
				currentBit++;
				ret[currentBit] = 0;
				currentBit++;
				ret[currentBit] = 0;
				currentBit++;
			}

			for (int j = 1; j <= DEFAULT_VALUES[i]; j++)
			{
				if (board[player][i] + DEFAULT_VALUES[i] == j)// one is on
				{
					if (startingPositions[i] == board[player][i])// one permanently
															// on
					{
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 1;
						currentBit++;
					}
					else
					// one got on this turn
					{
						if (board[opponent][i] + DEFAULT_VALUES[i] == j)// two
																	// permanently
																	// on
						{
							ret[currentBit] = 0;
							currentBit++;
							ret[currentBit] = 0;
							currentBit++;
							ret[currentBit] = 0;
							currentBit++;
							ret[currentBit] = 1;
							currentBit++;
							ret[currentBit] = 0;
							currentBit++;
						}
						else
						{
							ret[currentBit] = 0;
							currentBit++;
							ret[currentBit] = 0;
							currentBit++;
							ret[currentBit] = 1;
							currentBit++;
							ret[currentBit] = 0;
							currentBit++;
							ret[currentBit] = 0;
							currentBit++;
						}
					}
				}
				else
				// one is off
				{
					if (board[opponent][i] + DEFAULT_VALUES[i] == j)// two permanently
																// on
					{
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 1;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
					}
					else
					{
						ret[currentBit] = 1;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
						ret[currentBit] = 0;
						currentBit++;
					}
				}
			}
		}
		return ret;
	}

	// GAME METHODS
	private void instantiateBoard(int[][] board)
	{
		for (int i = 0; i < 11; i++)
		{
			for (int j = 0; j < board.length; j++)
			{
				if (i == 0)
				{
					board[j][i] = -3;
				}
				else if (i == 1)
				{
					board[j][i] = -5;
				}
				else if (i == 2)
				{
					board[j][i] = -7;
				}
				else if (i == 3)
				{
					board[j][i] = -9;
				}
				else if (i == 4)
				{
					board[j][i] = -11;
				}
				else if (i == 5)
				{
					board[j][i] = -13;
				}
				else if (i == 6)
				{
					board[j][i] = -11;
				}
				else if (i == 7)
				{
					board[j][i] = -9;
				}
				else if (i == 8)
				{
					board[j][i] = -7;
				}
				else if (i == 9)
				{
					board[j][i] = -5;
				}
				else
				{
					board[j][i] = -3;
				}
			}
		}
	}

	private int[] getStartingPositions(int player, int[][] board)
	{
		int[] returnArray = new int[11];
		for (int i = 0; i < 11; i++)
		{
			returnArray[i] = board[player][i];
		}
		return returnArray;
	}

	private boolean checkGame(int player, int[][] board)
	{
		int count = 0;
		for (int i = 0; i < board[player].length; i++)
		{
			if (board[player][i] == 0) count++;
		}
		return count >= 3;
	}

	private List<int[]> getValidMoves(int[] roll, List<Integer> placeholders, int player, int[][] board)
	{
		List<int[]> ret = new ArrayList<int[]>();
		List<int[]> possiblePairs = getPossiblePairings(roll);
		for (int[] pos : possiblePairs)
		{
			boolean has0 = placeholders.contains(pos[0]);
			boolean has1 = placeholders.contains(pos[1]);

			if (board[player][pos[0] - 2] == 0) has0 = false;
			if (board[player][pos[1] - 2] == 0) has1 = false;

			if (has0 && has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = pos[1];
				ret.add(newMove);
			}
			else if (has0)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0) newMove[1] = pos[1];
				}
				ret.add(newMove);
			}
			else if (has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[1];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0) newMove[1] = pos[0];
				}
				ret.add(newMove);
			}
			else
			{
				if (placeholders.size() < 2)
				{
					int[] newMove = new int[2];
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0) newMove[0] = pos[0];
					else newMove[0] = -1;
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0) newMove[1] = pos[1];
					else newMove[1] = -1;
					if (newMove[0] != -1 || newMove[1] != -1) ret.add(newMove);
				}
				else if (placeholders.size() < 3)
				{
					if (pos[0] != pos[1])
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = -1;
							ret.add(newMove);
						}
						if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
						{
							int[] otherMove = new int[2];
							otherMove[0] = pos[1];
							otherMove[1] = -1;
							ret.add(otherMove);
						}
					}
					else
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = pos[0];
							ret.add(newMove);
						}

					}
				}
			}
		}
		return ret;
	}

	private boolean makeMove(int[] choice, int player, List<Integer> placeholders, int[][] board)
	{
		for (int i = 0; i < 2; i++)
		{
			if (choice[i] != -1)
			{
				if (placeholders.contains(choice[i]))
				{
					if (board[player][choice[i] - 2] != 0) board[player][choice[i] - 2]++;
				}
				else
				{
					placeholders.add(choice[i]);
					board[player][choice[i] - 2]++;

				}
			}
		}
		boolean hasToRoll = false;
		for (int p = 0; p < board.length && !hasToRoll; p++)
		{
			for (int p2 = p + 1; p2 < board.length && !hasToRoll; p2++)
			{
				for (int i = 0; i < board[0].length && !hasToRoll; i++)
				{
					if (board[p][i] != -1 * DEFAULT_VALUES[i] && board[p][i] != CAPTURED && board[p2][i] == board[p][i])
					{
						hasToRoll = true;
					}
				}
			}
		}
		return hasToRoll;
	}

	private void setCaptured(int player, List<Integer> placeholders, int[][] board)
	{
		for (Integer p : placeholders)
		{
			if (board[player][p - 2] == 0)// that player won the column
			{
				for (int k = 0; k < board.length; k++)
				{
					if (k != player)
					{
						board[k][p - 2] = CAPTURED;
					}
				}
			}
		}
	}

	private int[] rollDice()
	{
		int[] returnDice = { (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1 };
		Arrays.sort(returnDice);
		return returnDice;
	}

	public boolean shouldRoll(int player, int[][] board, List<Integer> placeholders, int[] startingPositions)
	{
		return pdm.shouldRoll(player, board, placeholders, startingPositions);
	}

	public int decideOnPair(int player, List<int[]> availableMoves, List<Integer> placeholders, int[][] board, int[] startingPositions)
	{
		double[] values = new double[availableMoves.size()];

		for (int ind = 0; ind < values.length; ind++)
		{
			int[][] copyBoard = new int[board.length][board[0].length];
			for (int i = 0; i < board.length; i++)
				for (int j = 0; j < board[0].length; j++)
					copyBoard[i][j] = board[i][j];

			List<Integer> copyPlaceholders = new ArrayList<Integer>(placeholders);

			makeMove(availableMoves.get(ind), player, copyPlaceholders, copyBoard);
			double[] boardEncoding = encodeBoardAsDoubleArray(startingPositions, copyBoard, player);
			nnet.setInput(boardEncoding);
			nnet.calculate();
			double[] networkOutput = nnet.getOutput();
			values[ind] = networkOutput[0];
		}

		for (int m = 0; m < availableMoves.size(); m++)
		{
			System.out.println(Arrays.toString(availableMoves.get(m)) + " : " + values[m]);
		}

		int maxIndex = 0;
		for (int i = 1; i < availableMoves.size(); i++)
		{
			if (values[maxIndex] < values[i]) maxIndex = i;
		}
		return maxIndex;
	}

}
