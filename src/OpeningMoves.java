import java.util.List;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class OpeningMoves {
	private ProbabilisticDecisionMaker pdm;
	private int[] DEFAULT_VALUES = {-3, -5, -7, -9, -11, -13, -11, -9, -7, -5, -3 };
	private int[][] board;
	private int[][] rolls;
	public OpeningMoves()
	{
		pdm = new ProbabilisticDecisionMaker();
		board = new int[2][11];
		for (int i =0; i<DEFAULT_VALUES.length; i++)
		{
			board[0][i]= DEFAULT_VALUES[i];
			board[1][i]= DEFAULT_VALUES[i];
			rolls = new int[126][4];
		}
	}
	public void readRolls()
	{
		File file = new File("6sidedDieCombsAndWeights.txt");
		Scanner input = null; 
		try {
			input = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int count = 0;
		int line =0;
		while(input.hasNext())
		{
			int num = input.nextInt();
			rolls[line][count] = num;
			count++;
			if (count==4)
			{
				int junk = input.nextInt();
				count=0;
				line++;
			}
		}
		input.close();		
	}
	public void getFirstDecision()
	{
		List<Integer> placeholders = new ArrayList<Integer>();
		PrintWriter writer = null;
		try {
			writer = new PrintWriter("openingMoves.txt");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for(int i =0; i< rolls.length; i++)
		{
			int[] roll = {rolls[i][0], rolls[i][1], rolls[i][2], rolls[i][3]};	
			List<int[]> availableMoves = pdm.getPossiblePairings(roll);
			int decision = pdm.decideOnPair(0, availableMoves, placeholders, board, DEFAULT_VALUES);
			writer.print(Arrays.toString(roll) + " \t " +  Arrays.toString(availableMoves.get(decision)));
			writer.println();
			System.out.println(i);
		}
		writer.close();
	}
	public static void main(String args[])
	{
		OpeningMoves open = new OpeningMoves();
		open.readRolls();
		open.getFirstDecision();
	}

}
