import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;


public class Simulation2Players
{
	public final int[] defaultValues = { 3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3 };
	int[][] board;
	private int CAPTURED = -50;
	private boolean gameHasBeenWon;
	private int numPlayers;
	private ProbabilisticDecisionMaker pdm;
	private PrintWriter writer;
	private int player0Wins = 0;
	private int player1Wins = 0;

	public Simulation2Players() throws FileNotFoundException
	{
		this.numPlayers = 2;
		gameHasBeenWon = false;
		board = new int[numPlayers][11];
		pdm = new ProbabilisticDecisionMaker();
		writer = new PrintWriter("theGame.txt");
	}


	public int[] rollDice()
	{
		int[] returnDice = { (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1, (int) (Math.random() * 6) + 1 };
		Arrays.sort(returnDice);
		return returnDice;
	}

	public void playGameOrigFirst(int[] firstRoll0, int[] firstRoll1)
	{
		instantiateBoard();
		int player = 0;
		gameHasBeenWon = false;
		// boolean firstTimeThrough0 = true;
		// boolean firstTimeThrough1 = true;

		while (!gameHasBeenWon)
		{
			for (player = 0; player < numPlayers && !gameHasBeenWon; player++)
			{
				boolean willRollAgain = true;
				boolean notMissed = true;
				int[] startingPositions = getStartingPositions(player);
				List<Integer> placeholders = new ArrayList<Integer>();
				while (willRollAgain && notMissed)
				{
					int[] roll = rollDice();
					if (player == 0)
					{

						List<int[]> validMoves = getValidMoves(roll, placeholders, player);
						if (validMoves.size() > 1)
						{
							int maxIndex = pdm.decideOnPairOriginal(player, validMoves, board, startingPositions);


							int[] twoNumbersUserChose = validMoves.get(maxIndex);
							/*
							 * if (firstTimeThrough0) { writer.println(
							 * "Computer chose: " + twoNumbersUserChose[0] +
							 * "   " + twoNumbersUserChose[1]);
							 * firstTimeThrough0 = false; }
							 */
							willRollAgain = makeMove(twoNumbersUserChose, player, placeholders);
							if (!willRollAgain && checkGame(player))
							{
								gameHasBeenWon = true;
								break;
							}
							if (!willRollAgain)
							{
								willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);

							}
						}
						else if (validMoves.size() == 1)
						{

							int[] onlyChoiceForTwoNumbers = validMoves.get(0);
							/*
							 * if (firstTimeThrough0) {
							 * writer.println("Computer chose: " +
							 * onlyChoiceForTwoNumbers[0] + "   " +
							 * onlyChoiceForTwoNumbers[1]); firstTimeThrough0 =
							 * false; }
							 */
							willRollAgain = makeMove(onlyChoiceForTwoNumbers, player, placeholders);
							if (!willRollAgain && checkGame(player))
							{
								gameHasBeenWon = true;
								break;
							}
							if (!willRollAgain)
							{
								willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
							}
						}
						else
						{
							notMissed = false;
							for (int i = 0; i < 11; i++)
							{
								board[player][i] = startingPositions[i];
							}
						}
					}
					else
					{
						List<int[]> validMoves;
						validMoves = getValidMoves(roll, placeholders, player);
						if (validMoves.size() > 1)
						{

							int maxIndex = pdm.decideOnPairOriginal(player, validMoves, board, startingPositions);

							int[] twoNumbersUserChose = validMoves.get(maxIndex);
							/*
							 * if (firstTimeThrough1) { writer.println(
							 * "Computer chose: " + twoNumbersUserChose[0] +
							 * "   " + twoNumbersUserChose[1]);
							 * firstTimeThrough1 = false; }
							 */
							willRollAgain = makeMove(twoNumbersUserChose, player, placeholders);
							if (!willRollAgain && checkGame(player))
							{
								gameHasBeenWon = true;
								break;
							}
							if (!willRollAgain)
							{
								willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
							}
						}
						else if (validMoves.size() == 1)
						{
							int[] onlyChoiceForTwoNumbers = validMoves.get(0);
							/*
							 * if (firstTimeThrough1) {
							 * writer.println("Computer 2 chose: " +
							 * onlyChoiceForTwoNumbers[0] + "   " +
							 * onlyChoiceForTwoNumbers[1]); firstTimeThrough1 =
							 * false; }
							 */
							willRollAgain = makeMove(onlyChoiceForTwoNumbers, player, placeholders);
							if (!willRollAgain && checkGame(player))
							{
								gameHasBeenWon = true;
								break;
							}
							if (!willRollAgain)
							{
								willRollAgain = pdm.shouldRoll(player, board, placeholders, startingPositions);
							}
						}
						else
						{
							notMissed = false;
							for (int i = 0; i < 11; i++)
							{
								board[player][i] = startingPositions[i];
							}
						}

					}

				}
				setCaptured(player, placeholders);
			}
		}
		if (player == 1)
		{
			writer.println("player0 won");
			player0Wins += 1;
		}
		else
		{
			writer.println("player1 won");
			player1Wins += 1;
		}
	}

	private void setCaptured(int player, List<Integer> placeholders)
	{
		for (Integer p : placeholders)
		{
			if (board[player][p - 2] == 0)// that player won the column
			{
				for (int k = 0; k < board.length; k++)
				{
					if (k != player)
					{
						board[k][p - 2] = CAPTURED;
					}
				}
			}
		}

	}

	private boolean checkGame(int player)
	{
		int count = 0;
		for (int i = 0; i < board[player].length; i++)
		{
			if (board[player][i] == 0) count++;
		}
		return count >= 3;
	}

	public List<int[]> getValidMoves(int[] roll, List<Integer> placeholders, int player)
	{
		List<int[]> ret = new ArrayList<int[]>();
		List<int[]> possiblePairs = pdm.getPossiblePairings(roll);
		for (int[] pos : possiblePairs)
		{
			boolean has0 = placeholders.contains(pos[0]);
			boolean has1 = placeholders.contains(pos[1]);

			if (board[player][pos[0] - 2] == 0) has0 = false;
			if (board[player][pos[1] - 2] == 0) has1 = false;
			if (has0 && has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = pos[1];
				ret.add(newMove);
			}
			else if (has0)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[0];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0) newMove[1] = pos[1];
				}
				ret.add(newMove);
			}
			else if (has1)
			{
				int[] newMove = new int[2];
				newMove[0] = pos[1];
				newMove[1] = -1;
				if (placeholders.size() < 3)
				{
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0) newMove[1] = pos[0];
				}
				ret.add(newMove);
			}
			else
			{
				if (placeholders.size() < 2)
				{
					int[] newMove = new int[2];
					if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0) newMove[0] = pos[0];
					else newMove[0] = -1;
					if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0) newMove[1] = pos[1];
					else newMove[1] = -1;
					if (newMove[0] != -1 || newMove[1] != -1) ret.add(newMove);
				}
				else if (placeholders.size() < 3)
				{
					if (pos[0] != pos[1])
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = -1;
							ret.add(newMove);
						}
						if (board[player][pos[1] - 2] != CAPTURED && board[player][pos[1] - 2] != 0)
						{
							int[] otherMove = new int[2];
							otherMove[0] = pos[1];
							otherMove[1] = -1;
							ret.add(otherMove);
						}
					}
					else
					{
						if (board[player][pos[0] - 2] != CAPTURED && board[player][pos[0] - 2] != 0)
						{
							int[] newMove = new int[2];
							newMove[0] = pos[0];
							newMove[1] = pos[0];
							ret.add(newMove);
						}

					}
				}
			}
		}


		return ret;
	}

	public boolean makeMove(int[] choice, int player, List<Integer> placeholders)
	{
		for (int i = 0; i < 2; i++)
		{
			if (choice[i] != -1)
			{
				if (placeholders.contains(choice[i]))
				{
					if (board[player][choice[i] - 2] != 0) board[player][choice[i] - 2]++;
				}
				else
				{
					placeholders.add(choice[i]);
					board[player][choice[i] - 2]++;

				}
			}
		}
		boolean hasToRoll = false;
		for (int p = 0; p < board.length && !hasToRoll; p++)
		{
			for (int p2 = p + 1; p2 < board.length && !hasToRoll; p2++)
			{
				for (int i = 0; i < board[0].length && !hasToRoll; i++)
				{
					if (board[p][i] != -1 * defaultValues[i] && board[p2][i] == board[p][i])
					{
						hasToRoll = true;
					}
				}
			}
		}
		return hasToRoll;
	}

	public void instantiateBoard()
	{
		for (int i = 0; i < 11; i++)
		{
			for (int j = 0; j < board.length; j++)
			{
				if (i == 0)
				{
					board[j][i] = -3;
				}
				else if (i == 1)
				{
					board[j][i] = -5;
				}
				else if (i == 2)
				{
					board[j][i] = -7;
				}
				else if (i == 3)
				{
					board[j][i] = -9;
				}
				else if (i == 4)
				{
					board[j][i] = -11;
				}
				else if (i == 5)
				{
					board[j][i] = -13;
				}
				else if (i == 6)
				{
					board[j][i] = -11;
				}
				else if (i == 7)
				{
					board[j][i] = -9;
				}
				else if (i == 8)
				{
					board[j][i] = -7;
				}
				else if (i == 9)
				{
					board[j][i] = -5;
				}
				else
				{
					board[j][i] = -3;
				}
			}
		}
	}

	public int[] getStartingPositions(int player)
	{
		int[] returnArray = new int[11];
		for (int i = 0; i < 11; i++)
		{
			returnArray[i] = board[player][i];
		}
		return returnArray;
	}

	public static void main(String[] args) throws FileNotFoundException
	{
		System.out.println("program start");
		Simulation2Players game = new Simulation2Players();
		game.instantiateBoard();
		for (int j = 0; j < 1000; j++)
		{
			game.playGameOrigFirst(null, null);
			System.out.println(j);
		}
		System.out.println();
		game.writer.println("player0 won  " + game.player0Wins + " times");
		game.writer.println("player1 won  " + game.player1Wins + " times");
		System.out.println("player0 won  " + game.player0Wins + " times");
		System.out.println("player1 won  " + game.player1Wins + " times");
		game.writer.close();

	}

	@SuppressWarnings("unused")
	private int[][] getRolls() throws FileNotFoundException
	{
		File file = new File("6sidedDieCombsAndWeights.txt");
		Scanner input = new Scanner(file);
		int[][] ret = new int[126][];
		int countRet = 0;
		int count = 0;
		int[] roll = new int[4];
		while (input.hasNext())
		{
			if (count < 4)
			{
				int num = input.nextInt();
				roll[count] = num;
				count += 1;
			}
			else
			{
				int num1 = input.nextInt();
				count = 0;
				ret[countRet] = roll;
				countRet++;
				roll = new int[4];

			}
		}
		input.close();
		return ret;
	}

}