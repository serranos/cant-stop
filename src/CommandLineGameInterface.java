import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Scanner;
import java.sql.Timestamp;
import java.util.Date;

public class CommandLineGameInterface implements GameInterface
{
	private Scanner scan;
	private Game game;
	private boolean isTranscript = false;
	private PrintWriter fileInProgress;

	public CommandLineGameInterface()
	{
		scan = new Scanner(System.in);
	}

	public void updateDisplayOfBoard(int[][] board, int curPlayer, List<Integer> placeholders, int[] startingPositions)
	{
		print("---THE BOARD---", true);
		char playerOne = '*';
		char playerTwo = '%';
		char placeholder = '&';
		for (int p = 0; p < 11; p++)
		{
			if (p < 8) print((p + 2) + ":  ", false);
			else print((p + 2) + ": ", false);
			for (int s = 0; s < game.defaultValues[p]; s++)
			{
				print("[", false);
				for (int i = 0; i < board.length; i++)
				{
					if (game.defaultValues[p] + board[i][p] == s + 1 && board[i][p] != (-1) * game.defaultValues[p])
					{
						if (i == 0)
						{
							if (!(placeholders.contains(p + 2) && curPlayer == 0)) print("" + playerOne, false);
							else print("" + placeholder, false);
						}
						else
						{
							if (!(placeholders.contains(p + 2) && curPlayer == 1)) print("" + playerTwo, false);
							else print("" + placeholder, false);
						}
					}
					else if (game.defaultValues[p] + startingPositions[p] == s + 1 && curPlayer == i)
					{
						if (curPlayer == 0)
						{
							print("" + playerOne, false);
						}
						else
						{
							print("" + playerTwo, false);
						}

					}
				}

				print("]", false);
			}
			if (p != 10) print("", true);
		}

		print("", true);
	}

	public int getChoiceAboutDicePairing(int[] roll, List<int[]> possibleMoves, List<Integer> placeholders, int computersChoice)
	{
		displayRoll(roll);
		boolean haveGivenValidAnswer = false;
		int choice = -1;
		while (!haveGivenValidAnswer)
		{
			for (int i = 0; i < possibleMoves.size(); i++)
			{
				print("" + i, false);
				for (int j = 0; j < 2; j++)
				{
					if (possibleMoves.get(i)[j] != -1)
					{
						if (placeholders.contains(possibleMoves.get(i)[j])) print(" - advance on ", false);
						else print(" - place on ", false);
						print("" + possibleMoves.get(i)[j], false);
					}
				}
				print("", true);
			}
			print("Execute: ", false);
			if (!isTranscript && computersChoice == -1)
			{
				// if we need user input here
				choice = scan.nextInt();
				scan.nextLine();
				if (choice >= 0 && choice < possibleMoves.size()) haveGivenValidAnswer = true;
			}
			else if (computersChoice != -1)
			{
				// if this is the computer's turn in either a transcript or an
				// actual game
				print("" + computersChoice, true);
				haveGivenValidAnswer = true;
			}
			else
			{
				// if this is a transcript but we still don't have the user's
				// answer
				haveGivenValidAnswer = true;
			}
		}
		if (!isTranscript && computersChoice != -1)
		{
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e)
			{
			}
		}
		return choice;
	}

	public boolean getChoiceWhetherToRollAgain(int computersChoice)
	{
		print("Roll again? y/n : ", false);
		if (!isTranscript && computersChoice == -1)
		{
			// if we actually need user input
			String choice = scan.nextLine();
			if (choice.startsWith("y"))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if (computersChoice != -1)
		{
			// if we just need to print what the computer did
			if (computersChoice == 1)
			{
				print("yes", true);
			}
			else if (computersChoice == 0)
			{
				print("no", true);
			}
			if (!isTranscript)
			{
				try
				{
					Thread.sleep(2000);
				}
				catch (InterruptedException e)
				{
				}
			}
			return false;
		}
		else
		{
			// if this is a transcript but we need to wait for the user's input
			// (we just handle writing the whole line later)
			return false;
		}
	}

	public void showWhatOnlyDiceOptionIs(int[] roll, boolean isMiss, List<Integer> placeholders, int[] onlyChoice, boolean isComputer)
	{
		displayRoll(roll);
		if (isMiss)
		{
			print("Player missed.", true);
		}
		else
		{
			print("The only option is ", false);
			for (int j = 0; j < 2; j++)
			{
				if (onlyChoice[j] != -1)
				{
					if (placeholders.contains(onlyChoice[j])) print(" - advance on ", false);
					else print(" - place on ", false);
					print("" + onlyChoice[j], false);
				}
			}
			print("", true);
		}
		if (!isTranscript && isComputer)
		{
			try
			{
				Thread.sleep(2000);
			}
			catch (InterruptedException e)
			{
			}
		}
	}

	public void displayRoll(int[] roll)
	{
		print("Numbers rolled: " + roll[0] + ", " + roll[1] + ", " + roll[2] + ", " + roll[3], true);
	}

	@Override
	public void setGame(Game game)
	{
		this.game = game;
	}

	public void showWhoseTurnItIs(int player, boolean computerPlaying)
	{
		if (player == 0 && computerPlaying)
		{
			print("\nComputer's turn:", true);
		}
		else if (!computerPlaying)
		{
			print("\nPlayer " + (player + 1) + "'s turn:", true);
		}
		else
		{
			print("\nYour turn:", true);
		}
	}

	public void print(String message, boolean blankLineAfterThis)
	{
		if (isTranscript && fileInProgress != null)
		{
			if (blankLineAfterThis)
			{
				fileInProgress.println(message);
			}
			else
			{
				fileInProgress.print(message);
			}
			fileInProgress.flush();
		}
		else
		{
			if (blankLineAfterThis)
			{
				System.out.println(message);
			}
			else
			{
				System.out.print(message);
			}
		}
	}

	public void setAsTranscript()
	{
		isTranscript = true;
		Date date = new java.util.Date();
		Timestamp curTimestamp = new Timestamp(date.getTime());
		String time = curTimestamp.toString();
		time = time.replaceAll(" ", "at");
		time = time.substring(0, time.indexOf('.'));
		time = time.replaceAll(":", "-");
		try
		{
			fileInProgress = new PrintWriter(new File("Transcripts/TranscriptFrom" + time + ".txt"));
		}
		catch (FileNotFoundException e)
		{
			System.err.println("Transcript creation failed.");
			fileInProgress = null;
		}
	}

	public void displayWin(int player, boolean computerPlaying)
	{
		if (player == 0 && computerPlaying)
		{
			print("The computer won!", true);
		}
		else if (computerPlaying)
		{
			print("Congratulations, you beat the computer!", true);
		}
		else
		{
			print("Congratulations player " + (player + 1) + ", you won!", true);
		}
	}

	public void close()
	{
		fileInProgress.close();
	}
}
