import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import java.awt.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.awt.Toolkit;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;

/**
 * Created by sofiaserrano on 6/13/16.
 */
public class Main extends Application {
    private final boolean USE_GUI = true;
    private final Game game = new Game(USE_GUI);
    private boolean setIcon = false;

    @Override
    public void start(Stage primaryStage) throws Exception{
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                Platform.exit();
                System.exit(0);
            }
        });
        if (setIcon)
            primaryStage.getIcons().add(new javafx.scene.image.Image(this.getClass().getResourceAsStream("images/icon.png")));

        FXMLLoader gameScreenLoader = new FXMLLoader(getClass().getResource("fxml/gameScreen.fxml"));
        AnchorPane gameScreenRoot = (AnchorPane) gameScreenLoader.load();
        GraphicalUserGameInterface controller = gameScreenLoader.getController();

        game.setGraphicalInterface(controller);
        Scene gameScene = new Scene(gameScreenRoot, 900, 660);
        gameScene.setOnKeyPressed(controller);
        controller.setGameStageAndScenes(game, primaryStage, gameScene, gameScreenRoot);
        controller.initializeDisplay();
    }

    public static void main(String[] args)
    {
        Main main = new Main();
        try {
            Class util = Class.forName("com.apple.eawt.Application");
            Method getApplication = util.getMethod("getApplication", new Class[0]);
            Object application = getApplication.invoke(util);
            Class params[] = new Class[1];
            params[0] = Image.class;
            Method setDockIconImage = util.getMethod("setDockIconImage", params);
            URL url = ClassLoader.getSystemResource("images/icon.png");
            Image image = Toolkit.getDefaultToolkit().getImage(url);
            setDockIconImage.invoke(application, image);
        } catch (ClassNotFoundException e) {
        	main.setIcon = true;
            // log exception
        } catch (NoSuchMethodException e) {
            // log exception
        } catch (InvocationTargetException e) {
            // log exception
        } catch (IllegalAccessException e) {
            // log exception
        }
        if (!main.USE_GUI) {
            main.game.playGame(null);
        } else {
            main.launch(args);
        }
    }
}