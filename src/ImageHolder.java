import javafx.scene.Node;
import javafx.scene.image.ImageView;

/**
 * Created by sofiaserrano on 6/13/16.
 */
public class ImageHolder {
    private double dieFaceHeightAndWidth = 75;
    private double boardHeight = 600.0;

    private Node dieFace11;
    private Node dieFace12;
    private Node dieFace13;
    private Node dieFace14;
    private Node dieFace21;
    private Node dieFace22;
    private Node dieFace23;
    private Node dieFace24;
    private Node dieFace31;
    private Node dieFace32;
    private Node dieFace33;
    private Node dieFace34;
    private Node dieFace41;
    private Node dieFace42;
    private Node dieFace43;
    private Node dieFace44;
    private Node dieFace51;
    private Node dieFace52;
    private Node dieFace53;
    private Node dieFace54;
    private Node dieFace61;
    private Node dieFace62;
    private Node dieFace63;
    private Node dieFace64;

    private ImageView board;

    private ImageView bluePiece2;
    private ImageView bluePiece3;
    private ImageView bluePiece4;
    private ImageView bluePiece5;
    private ImageView bluePiece6;
    private ImageView bluePiece7;
    private ImageView bluePiece8;
    private ImageView bluePiece9;
    private ImageView bluePiece10;
    private ImageView bluePiece11;
    private ImageView bluePiece12;

    private ImageView orangePiece2;
    private ImageView orangePiece3;
    private ImageView orangePiece4;
    private ImageView orangePiece5;
    private ImageView orangePiece6;
    private ImageView orangePiece7;
    private ImageView orangePiece8;
    private ImageView orangePiece9;
    private ImageView orangePiece10;
    private ImageView orangePiece11;
    private ImageView orangePiece12;

    private ImageView yellowPiece2;
    private ImageView yellowPiece3;
    private ImageView yellowPiece4;
    private ImageView yellowPiece5;
    private ImageView yellowPiece6;
    private ImageView yellowPiece7;
    private ImageView yellowPiece8;
    private ImageView yellowPiece9;
    private ImageView yellowPiece10;
    private ImageView yellowPiece11;
    private ImageView yellowPiece12;

    private ImageView greenPiece2;
    private ImageView greenPiece3;
    private ImageView greenPiece4;
    private ImageView greenPiece5;
    private ImageView greenPiece6;
    private ImageView greenPiece7;
    private ImageView greenPiece8;
    private ImageView greenPiece9;
    private ImageView greenPiece10;
    private ImageView greenPiece11;
    private ImageView greenPiece12;

    private ImageView whitePiece1;
    private ImageView whitePiece2;
    private ImageView whitePiece3;

    public ImageHolder() {
        ImageView tempView = new ImageView("images/dieface1.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace11 = tempView;
        tempView = new ImageView("images/dieface1.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace12 = tempView;
        tempView = new ImageView("images/dieface1.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace13 = tempView;
        tempView = new ImageView("images/dieface1.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace14 = tempView;

        tempView = new ImageView("images/dieface2.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace21 = tempView;
        tempView = new ImageView("images/dieface2.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace22 = tempView;
        tempView = new ImageView("images/dieface2.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace23 = tempView;
        tempView = new ImageView("images/dieface2.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace24 = tempView;

        tempView = new ImageView("images/dieface3.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace31 = tempView;
        tempView = new ImageView("images/dieface3.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace32 = tempView;
        tempView = new ImageView("images/dieface3.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace33 = tempView;
        tempView = new ImageView("images/dieface3.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace34 = tempView;

        tempView = new ImageView("images/dieface4.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace41 = tempView;
        tempView = new ImageView("images/dieface4.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace42 = tempView;
        tempView = new ImageView("images/dieface4.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace43 = tempView;
        tempView = new ImageView("images/dieface4.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace44 = tempView;

        tempView = new ImageView("images/dieface5.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace51 = tempView;
        tempView = new ImageView("images/dieface5.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace52 = tempView;
        tempView = new ImageView("images/dieface5.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace53 = tempView;
        tempView = new ImageView("images/dieface5.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace54 = tempView;

        tempView = new ImageView("images/dieface6.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace61 = tempView;
        tempView = new ImageView("images/dieface6.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace62 = tempView;
        tempView = new ImageView("images/dieface6.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace63 = tempView;
        tempView = new ImageView("images/dieface6.png");
        tempView.setFitHeight(dieFaceHeightAndWidth);
        tempView.setFitWidth(dieFaceHeightAndWidth);
        dieFace64 = tempView;

        tempView = new ImageView("images/board.png");
        tempView.setFitHeight(boardHeight);
        tempView.setFitWidth(boardHeight);
        board = tempView;

        double pieceHeight = (boardHeight / 800.0) * 54.0;

        bluePiece2 = new ImageView("images/bluepiece.jpeg");
        bluePiece2.setFitHeight(pieceHeight);
        bluePiece2.setFitWidth(pieceHeight);
        bluePiece3 = new ImageView("images/bluepiece.jpeg");
        bluePiece3.setFitHeight(pieceHeight);
        bluePiece3.setFitWidth(pieceHeight);
        bluePiece4 = new ImageView("images/bluepiece.jpeg");
        bluePiece4.setFitHeight(pieceHeight);
        bluePiece4.setFitWidth(pieceHeight);
        bluePiece5 = new ImageView("images/bluepiece.jpeg");
        bluePiece5.setFitHeight(pieceHeight);
        bluePiece5.setFitWidth(pieceHeight);
        bluePiece6 = new ImageView("images/bluepiece.jpeg");
        bluePiece6.setFitHeight(pieceHeight);
        bluePiece6.setFitWidth(pieceHeight);
        bluePiece7 = new ImageView("images/bluepiece.jpeg");
        bluePiece7.setFitHeight(pieceHeight);
        bluePiece7.setFitWidth(pieceHeight);
        bluePiece8 = new ImageView("images/bluepiece.jpeg");
        bluePiece8.setFitHeight(pieceHeight);
        bluePiece8.setFitWidth(pieceHeight);
        bluePiece9 = new ImageView("images/bluepiece.jpeg");
        bluePiece9.setFitHeight(pieceHeight);
        bluePiece9.setFitWidth(pieceHeight);
        bluePiece10 = new ImageView("images/bluepiece.jpeg");
        bluePiece10.setFitHeight(pieceHeight);
        bluePiece10.setFitWidth(pieceHeight);
        bluePiece11 = new ImageView("images/bluepiece.jpeg");
        bluePiece11.setFitHeight(pieceHeight);
        bluePiece11.setFitWidth(pieceHeight);
        bluePiece12 = new ImageView("images/bluepiece.jpeg");
        bluePiece12.setFitHeight(pieceHeight);
        bluePiece12.setFitWidth(pieceHeight);

        orangePiece2 = new ImageView("images/orangepiece.jpeg");
        orangePiece2.setFitHeight(pieceHeight);
        orangePiece2.setFitWidth(pieceHeight);
        orangePiece3 = new ImageView("images/orangepiece.jpeg");
        orangePiece3.setFitHeight(pieceHeight);
        orangePiece3.setFitWidth(pieceHeight);
        orangePiece4 = new ImageView("images/orangepiece.jpeg");
        orangePiece4.setFitHeight(pieceHeight);
        orangePiece4.setFitWidth(pieceHeight);
        orangePiece5 = new ImageView("images/orangepiece.jpeg");
        orangePiece5.setFitHeight(pieceHeight);
        orangePiece5.setFitWidth(pieceHeight);
        orangePiece6 = new ImageView("images/orangepiece.jpeg");
        orangePiece6.setFitHeight(pieceHeight);
        orangePiece6.setFitWidth(pieceHeight);
        orangePiece7 = new ImageView("images/orangepiece.jpeg");
        orangePiece7.setFitHeight(pieceHeight);
        orangePiece7.setFitWidth(pieceHeight);
        orangePiece8 = new ImageView("images/orangepiece.jpeg");
        orangePiece8.setFitHeight(pieceHeight);
        orangePiece8.setFitWidth(pieceHeight);
        orangePiece9 = new ImageView("images/orangepiece.jpeg");
        orangePiece9.setFitHeight(pieceHeight);
        orangePiece9.setFitWidth(pieceHeight);
        orangePiece10 = new ImageView("images/orangepiece.jpeg");
        orangePiece10.setFitHeight(pieceHeight);
        orangePiece10.setFitWidth(pieceHeight);
        orangePiece11 = new ImageView("images/orangepiece.jpeg");
        orangePiece11.setFitHeight(pieceHeight);
        orangePiece11.setFitWidth(pieceHeight);
        orangePiece12 = new ImageView("images/orangepiece.jpeg");
        orangePiece12.setFitHeight(pieceHeight);
        orangePiece12.setFitWidth(pieceHeight);

        greenPiece2 = new ImageView("images/greenpiece.jpeg");
        greenPiece2.setFitHeight(pieceHeight);
        greenPiece2.setFitWidth(pieceHeight);
        greenPiece3 = new ImageView("images/greenpiece.jpeg");
        greenPiece3.setFitHeight(pieceHeight);
        greenPiece3.setFitWidth(pieceHeight);
        greenPiece4 = new ImageView("images/greenpiece.jpeg");
        greenPiece4.setFitHeight(pieceHeight);
        greenPiece4.setFitWidth(pieceHeight);
        greenPiece5 = new ImageView("images/greenpiece.jpeg");
        greenPiece5.setFitHeight(pieceHeight);
        greenPiece5.setFitWidth(pieceHeight);
        greenPiece6 = new ImageView("images/greenpiece.jpeg");
        greenPiece6.setFitHeight(pieceHeight);
        greenPiece6.setFitWidth(pieceHeight);
        greenPiece7 = new ImageView("images/greenpiece.jpeg");
        greenPiece7.setFitHeight(pieceHeight);
        greenPiece7.setFitWidth(pieceHeight);
        greenPiece8 = new ImageView("images/greenpiece.jpeg");
        greenPiece8.setFitHeight(pieceHeight);
        greenPiece8.setFitWidth(pieceHeight);
        greenPiece9 = new ImageView("images/greenpiece.jpeg");
        greenPiece9.setFitHeight(pieceHeight);
        greenPiece9.setFitWidth(pieceHeight);
        greenPiece10 = new ImageView("images/greenpiece.jpeg");
        greenPiece10.setFitHeight(pieceHeight);
        greenPiece10.setFitWidth(pieceHeight);
        greenPiece11 = new ImageView("images/greenpiece.jpeg");
        greenPiece11.setFitHeight(pieceHeight);
        greenPiece11.setFitWidth(pieceHeight);
        greenPiece12 = new ImageView("images/greenpiece.jpeg");
        greenPiece12.setFitHeight(pieceHeight);
        greenPiece12.setFitWidth(pieceHeight);

        yellowPiece2 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece2.setFitHeight(pieceHeight);
        yellowPiece2.setFitWidth(pieceHeight);
        yellowPiece3 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece3.setFitHeight(pieceHeight);
        yellowPiece3.setFitWidth(pieceHeight);
        yellowPiece4 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece4.setFitHeight(pieceHeight);
        yellowPiece4.setFitWidth(pieceHeight);
        yellowPiece5 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece5.setFitHeight(pieceHeight);
        yellowPiece5.setFitWidth(pieceHeight);
        yellowPiece6 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece6.setFitHeight(pieceHeight);
        yellowPiece6.setFitWidth(pieceHeight);
        yellowPiece7 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece7.setFitHeight(pieceHeight);
        yellowPiece7.setFitWidth(pieceHeight);
        yellowPiece8 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece8.setFitHeight(pieceHeight);
        yellowPiece8.setFitWidth(pieceHeight);
        yellowPiece9 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece9.setFitHeight(pieceHeight);
        yellowPiece9.setFitWidth(pieceHeight);
        yellowPiece10 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece10.setFitHeight(pieceHeight);
        yellowPiece10.setFitWidth(pieceHeight);
        yellowPiece11 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece11.setFitHeight(pieceHeight);
        yellowPiece11.setFitWidth(pieceHeight);
        yellowPiece12 = new ImageView("images/yellowpiece.jpeg");
        yellowPiece12.setFitHeight(pieceHeight);
        yellowPiece12.setFitWidth(pieceHeight);

        whitePiece1 = new ImageView("images/placeholder.png");
        whitePiece1.setFitHeight(pieceHeight);
        whitePiece1.setFitWidth(pieceHeight);
        whitePiece2 = new ImageView("images/placeholder.png");
        whitePiece2.setFitHeight(pieceHeight);
        whitePiece2.setFitWidth(pieceHeight);
        whitePiece3 = new ImageView("images/placeholder.png");
        whitePiece3.setFitHeight(pieceHeight);
        whitePiece3.setFitWidth(pieceHeight);
    }

    public Node getNextAvailableDieImage(int dieFaceValue, int dieNumber) {
        if (dieNumber == 1) {
            if (dieFaceValue == 1) {
                return dieFace11;
            } else if (dieFaceValue == 2) {
                return dieFace21;
            } else if (dieFaceValue == 3) {
                return dieFace31;
            } else if (dieFaceValue == 4) {
                return dieFace41;
            } else if (dieFaceValue == 5) {
                return dieFace51;
            } else if (dieFaceValue == 6) {
                return dieFace61;
            }
        } else if (dieNumber == 2) {
            if (dieFaceValue == 1) {
                return dieFace12;
            } else if (dieFaceValue == 2) {
                return dieFace22;
            } else if (dieFaceValue == 3) {
                return dieFace32;
            } else if (dieFaceValue == 4) {
                return dieFace42;
            } else if (dieFaceValue == 5) {
                return dieFace52;
            } else if (dieFaceValue == 6) {
                return dieFace62;
            }
        } else if (dieNumber == 3) {
            if (dieFaceValue == 1) {
                return dieFace13;
            } else if (dieFaceValue == 2) {
                return dieFace23;
            } else if (dieFaceValue == 3) {
                return dieFace33;
            } else if (dieFaceValue == 4) {
                return dieFace43;
            } else if (dieFaceValue == 5) {
                return dieFace53;
            } else if (dieFaceValue == 6) {
                return dieFace63;
            }
        } else if (dieNumber == 4) {
            if (dieFaceValue == 1) {
                return dieFace14;
            } else if (dieFaceValue == 2) {
                return dieFace24;
            } else if (dieFaceValue == 3) {
                return dieFace34;
            } else if (dieFaceValue == 4) {
                return dieFace44;
            } else if (dieFaceValue == 5) {
                return dieFace54;
            } else if (dieFaceValue == 6) {
                return dieFace64;
            }
        }

        System.err.println("Should never get here -- means die face not returned");
        return new ImageView();
    }

    public double getDieHeightAndWidth() {
        return dieFaceHeightAndWidth;
    }

    public Node getBoard() {
        return board;
    }

    public Node getPiece(String color, int column) {
        if (color.equals("blue")) {
            if (column == 2) {
                return bluePiece2;
            } else if (column == 3) {
                return bluePiece3;
            } else if (column == 4) {
                return bluePiece4;
            } else if (column == 5) {
                return bluePiece5;
            } else if (column == 6) {
                return bluePiece6;
            } else if (column == 7) {
                return bluePiece7;
            } else if (column == 8) {
                return bluePiece8;
            } else if (column == 9) {
                return bluePiece9;
            } else if (column == 10) {
                return bluePiece10;
            } else if (column == 11) {
                return bluePiece11;
            } else if (column == 12) {
                return bluePiece12;
            }
        } else if (color.equals("yellow")) {
            if (column == 2) {
                return yellowPiece2;
            } else if (column == 3) {
                return yellowPiece3;
            } else if (column == 4) {
                return yellowPiece4;
            } else if (column == 5) {
                return yellowPiece5;
            } else if (column == 6) {
                return yellowPiece6;
            } else if (column == 7) {
                return yellowPiece7;
            } else if (column == 8) {
                return yellowPiece8;
            } else if (column == 9) {
                return yellowPiece9;
            } else if (column == 10) {
                return yellowPiece10;
            } else if (column == 11) {
                return yellowPiece11;
            } else if (column == 12) {
                return yellowPiece12;
            }
        } else if (color.equals("orange")) {
            if (column == 2) {
                return orangePiece2;
            } else if (column == 3) {
                return orangePiece3;
            } else if (column == 4) {
                return orangePiece4;
            } else if (column == 5) {
                return orangePiece5;
            } else if (column == 6) {
                return orangePiece6;
            } else if (column == 7) {
                return orangePiece7;
            } else if (column == 8) {
                return orangePiece8;
            } else if (column == 9) {
                return orangePiece9;
            } else if (column == 10) {
                return orangePiece10;
            } else if (column == 11) {
                return orangePiece11;
            } else if (column == 12) {
                return orangePiece12;
            }
        } else if (color.equals("green")) {
            if (column == 2) {
                return greenPiece2;
            } else if (column == 3) {
                return greenPiece3;
            } else if (column == 4) {
                return greenPiece4;
            } else if (column == 5) {
                return greenPiece5;
            } else if (column == 6) {
                return greenPiece6;
            } else if (column == 7) {
                return greenPiece7;
            } else if (column == 8) {
                return greenPiece8;
            } else if (column == 9) {
                return greenPiece9;
            } else if (column == 10) {
                return greenPiece10;
            } else if (column == 11) {
                return greenPiece11;
            } else if (column == 12) {
                return greenPiece12;
            }
        }
        System.err.println("Invalid color (" + color + ") or column (" + column + ") requested from getPiece().");
        return new ImageView();
    }

    public Node getPlaceholder(int zeroOneOrTwo) {
        if (zeroOneOrTwo == 0) {
            return whitePiece1;
        } else if (zeroOneOrTwo == 1) {
            return whitePiece2;
        } else if (zeroOneOrTwo == 2) {
            return whitePiece3;
        }
        System.err.println("Placeholder requested was not 0, 1, or 2.");
        return new ImageView();
    }
}
