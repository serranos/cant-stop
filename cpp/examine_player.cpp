#include "rollout.cpp"
#include <iostream>

using std::ofstream;
using std::cout;
using std::endl;

double cur_error_rate;
int cur_count;

void estimate_rolling_error(bool decision_made, int** initial_board, int* initial_starting_pos, vector<int>& initial_placeholders)
{
	cout << endl << "\topponent's current error is " << cur_error_rate << endl;
	double temp = 0;

	bool first_time = true;

	int max_times = 30;
	int turn = 0;
	while(turn < max_times)
	{
		turn++;

		int roll = play_rollout_game(1, temp, initial_board, initial_starting_pos, initial_placeholders, empty_move, empty_move, preset_roll, 1500);
		int stop = play_rollout_game(1, temp, initial_board, initial_starting_pos, initial_placeholders, empty_move, empty_move, preset_stop, 1500);
		
		if(decision_made)
		{
			cout << "shouldn't be here yet"  << endl;
			if(roll > stop)
				break;
			else
				temp -= 0.05;
		}
		else
		{
			if(roll < stop)
			{
				/*if(first_time)
				{
					while(turn < max_times)
					{
						turn++;
						int roll = play_rollout_game(1, temp, initial_board, initial_starting_pos, initial_placeholders, empty_move, empty_move, preset_roll, 1500);
						int stop = play_rollout_game(1, temp, initial_board, initial_starting_pos, initial_placeholders, empty_move, empty_move, preset_stop, 1500);

						if(roll > stop)
						{
							temp += 0.05;
							break;
						}
						else
							temp -= 0.05;							
					}
				}*/
				break;				
			}				
			else
				temp += 0.05;
		}
		first_time = false;
	}

	cur_error_rate = (cur_error_rate*cur_count + temp)/(cur_count + 1);
	cur_count++;

	ofstream write("cpp/comm/error_data.txt");
	write << cur_error_rate << " " << cur_count;
	write.close();

	cout << "\tnew turn's error is estimated at " << temp << endl;
	cout <<  "\taverage error updated to " << cur_error_rate  << endl<<endl;
}

int main()
{
	ifstream read("cpp/comm/error_data.txt");
	read >> cur_error_rate;
	read >> cur_count;
	read.close();
	read.open("cpp/comm/call_to_cpp.txt");
	bool action;
	read >> action;
	int num;
	int** board = new int*[num_players];
	for(int i = 0; i < num_players; i++)
	{
		board[i] = new int[num_columns];
		for(int j = 0; j < num_columns; j++)
		{
			read >> num;
			board[i][j] = num;
		}
	}
	vector<int> placeholders;
	int placeholders_size;
	read >> placeholders_size;
	for(int i = 0; i < placeholders_size; i++)
	{
		read >> num;
		placeholders.push_back(num);
	}

	int* starting_pos = new int[num_columns];
	for(int i = 0; i < num_columns; i++)
	{
		read >> num;
		starting_pos[i] = num;
	}
	read.close();

	initialize_prob_arrays();
	estimate_rolling_error(action, board, starting_pos, placeholders);
	free_board(board);
	free_prob_arrays();
	delete[] starting_pos;

	return 0;
}
