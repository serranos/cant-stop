#include <fstream>
#include <vector>
#include <limits>
#include "constants.h"

using std::vector;
using std::ifstream;
using std::numeric_limits;

const int permutations = 6*6*6*6;

int*** hitting_first;
int*** hitting_first_and_second;
int*** hitting_first_twice;
int* probs_for_cols;

//dealing with arrays for mathematical model probabilities
void initialize_prob_arrays()
{
	int num_lines = 990;
	int i, j, k, result;
	hitting_first = new int**[num_columns];
	hitting_first_and_second = new int**[num_columns];
	hitting_first_twice = new int**[num_columns];
	probs_for_cols = new int[num_columns];

	for(int h = 0; h < num_columns; h++)
	{
		hitting_first[h] = new int*[num_columns];
		hitting_first_and_second[h] = new int*[num_columns];
		hitting_first_twice[h] = new int*[num_columns];
		for(int l = 0; l < num_columns; l++)
		{
			hitting_first[h][l] = new int[num_columns];
			hitting_first_and_second[h][l] = new int[num_columns];
			hitting_first_twice[h][l] = new int[num_columns];
		}
	}
	
	ifstream read("cpp/files/prob_of_hitting_first.txt");	
	for(int line = 0; line < num_lines; line++)
	{
		read >> i >> j >> k >> result;
		hitting_first[i][j][k] = result;
	}
	read.close();

	read.open("cpp/files/prob_of_hitting_first_and_second.txt");
	for(int line = 0; line < num_lines; line++)
	{
		read >> i >> j >> k >> result;
		hitting_first_and_second[i][j][k] = result;
	}
	read.close();

	read.open("cpp/files/prob_of_hitting_first_twice.txt");
	for(int line = 0; line < num_lines; line++)
	{
		read >> i >> j >> k >> result;
		hitting_first_twice[i][j][k] = result;
	}
	read.close();

	read.open("cpp/files/probs_for_columns.txt");
	for(int line = 0; line < num_columns; line++)
	{
		read >> result;
		probs_for_cols[line] = result;
	}
	read.close();
}

void free_prob_arrays()
{
	for(int h = 0; h < num_columns; h++)
	{
		for(int l = 0; l < num_columns; l++)
		{
			delete[] hitting_first[h][l];
			delete[] hitting_first_twice[h][l];
			delete[] hitting_first_and_second[h][l];
		}
		delete[] hitting_first[h];
		delete[] hitting_first_twice[h];
		delete[] hitting_first_and_second[h];
	}

	delete[] hitting_first;
	delete[] hitting_first_twice;
	delete[] hitting_first_and_second;
	delete[] probs_for_cols;
}

//mathematical model decision makers
int decide_on_pair(int player, vector<int> valid_moves, vector<int>& placeholders, int** board, int* starting_pos)
{
	int vm_size = valid_moves.size();
	
	double max_value = numeric_limits<double>::min();
	double value;
	int max_index = 0;

	for (int i = 0; i < vm_size; i+=2)
	{
		value = 0;
		int x = valid_moves.at(i), y = valid_moves.at(i+1);
		if(x!=empty_move)
			value += (board[player][x-2] + 1.0 + default_values[x-2]) * default_values[num_columns/2] / default_values[x-2];
		if(y!=empty_move)
			value += (board[player][y-2] + 1.0 + default_values[y-2]) * default_values[num_columns/2] / default_values[y-2];

		if(value > max_value)
		{
			max_value = value;
			max_index = i;
		}
	}
	
	return max_index;
}

bool should_roll_prob(int col1, int col2, int col3, int c1, int c2, int c3, double error_rate)
{	
	long long int p1 = hitting_first[col1][col2][col3];
	int p2 = hitting_first[col2][col1][col3];
	int p3 = hitting_first[col3][col1][col2];
	int p12 = hitting_first_and_second[col1][col2][col3];
	int p13 = hitting_first_and_second[col1][col3][col2];
	int p23 = hitting_first_and_second[col2][col3][col1];
	int p11 = hitting_first_twice[col1][col2][col3];
	int p22 = hitting_first_twice[col2][col1][col3];
	int p33 = hitting_first_twice[col3][col2][col1];

	long long int w1 = probs_for_cols[col2] * probs_for_cols[col3];
	int w2 = probs_for_cols[col1] * probs_for_cols[col3];
	int w3 = probs_for_cols[col1] * probs_for_cols[col2];

	long long int left_side = p1 * (w1 * (c1 + 1) + w2 * c2 + w3 * c3) + p2 * (w1 * c1 + w2 * (c2 + 1) + w3 * c3)
			+ p3 * (w1 * c1 + w2 * c2 + w3 * (c3 + 1)) + p12 * (w1 * (c1 + 1) + w2 * (c2 + 1) + w3 * c3)
			+ p13 * (w1 * (c1 + 1) + w2 * c2 + w3 * (c3 + 1)) + p23 * (w1 * c1 + w2 * (c2 + 1) + w3 * (c3 + 1))
			+ p11 * (w1 * (c1 + 2) + w2 * c2 + w3 * c3) + p22 * (w1 * c1 + w2 * (c2 + 2) + w3 * c3)
			+ p33 * (w1 * c1 + w2 * c2 + w3 * (c3 + 2));

	long long int right_side = (c1 * w1 + c2 * w2 + c3 * w3) * permutations;

	return left_side > right_side + right_side * error_rate;
}

bool should_roll(int player, int** board, vector<int>& placeholders, int* starting_pos, double error_rate)
{
	int col1 = 0, col2 = 0, col3 = 0, c1 = 0, c2 = 0, c3 = 0;
	bool won1, won2, won3;
	bool col1free = false, col2free = false, col3free = false;

	if (placeholders.size() > 0)
	{
		col1 = placeholders.at(0);
		col1 -= 2;
		c1 = board[player][col1] - starting_pos[col1];
		won1 = (board[player][col1] == 0);
	}
	else
	{
		col1free = true;
		won1 = false;
	}
	if (placeholders.size() > 1)
	{
		col2 = placeholders.at(1);
		col2 -= 2;
		c2 = board[player][col2] - starting_pos[col2];
		won2 = (board[player][col2] == 0);
	}
	else
	{
		col2free = true;
		won2 = false;
	}
	if (placeholders.size() > 2)
	{
		col3 = placeholders.at(2);
		col3 -= 2;
		c3 = board[player][col3] - starting_pos[col3];
		won3 = (board[player][col3] == 0);
	}
	else
	{
		col3free = true;
		won3 = false;
	}

	if(col1free || col2free || col3free)
		return true;
	else if (!won1 && !won2 && !won3)
		return should_roll_prob(col1, col2, col3, c1, c2, c3, error_rate);
	else
		return false;
}
