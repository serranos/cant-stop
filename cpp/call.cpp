#include <fstream>
#include "rollout.cpp"

using std::vector;
using std::ifstream;
using std::ofstream;

const int rolling_decision = 0;
const int pairing_decision = 1;

int main()
{
	ifstream read("cpp/comm/call_to_cpp.txt");	
	int action;
	read >> action;
	int num;
	int** board = new int*[num_players];
	for(int i = 0; i < num_players; i++)
	{
		board[i] = new int[num_columns];
		for(int j = 0; j < num_columns; j++)
		{
			read >> num;
			board[i][j] = num;
		}
	}
	vector<int> placeholders;
	int placeholders_size;
	read >> placeholders_size;
	for(int i = 0; i < placeholders_size; i++)
	{
		read >> num;
		placeholders.push_back(num);
	}

	int vm_size;
	read >> vm_size;
	vector<int> valid_moves;
	for(int i = 0; i < vm_size; i++)
	{
		read >> num;
		valid_moves.push_back(num);
	}

	run_errorless_games;
	read >> run_errorless_games;

	int* starting_pos = new int[num_columns];
	for(int i = 0; i < num_columns; i++)
	{
		read >> num;
		starting_pos[i] = num;
	}
	read.close();

	srand((unsigned)time(NULL));
	initialize_prob_arrays();
	int value;
	if(action == rolling_decision)
	{
		if(should_roll_mc(0, board, placeholders, starting_pos))
			value = 1;
		else
			value = 0; 
	}
	else
		value = decide_on_pair_mc(0, valid_moves, placeholders, board, starting_pos);

	free_board(board);
	free_prob_arrays();
	delete[] starting_pos;

	return value;
}