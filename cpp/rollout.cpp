#include <iostream>
#include "game.cpp"
#include "math.cpp"

using std::endl;
using std::cout;

bool consider_opponent_error = true;
bool run_errorless_games;

bool play_full_game()
{
    int player = 0;
    bool game_won = false;
    int** board = instantiate_board();
    while(!game_won)
    {
        for(player = 0; player < num_players && !game_won; player++)
        {
            bool will_roll = true, not_missed = true;
            int* starting_pos = get_starting_pos(player, board);
            vector<int> placeholders;

            while(will_roll && not_missed)
            {
                int* roll = roll_dice();                         
                vector<int> valid_moves = get_valid_moves(player, roll, placeholders, board); 
                
                if(valid_moves.size() > 2)
                {
                    int index = decide_on_pair(player, valid_moves, placeholders, board, starting_pos);
                    will_roll = make_move(player, valid_moves.at(index), valid_moves.at(index + 1), placeholders, board);

                    if(!will_roll)
                    {
                        if(check_won(player, board))
                        {
                            game_won = true;
                            delete[] roll;
                         	break;
                        }
                        will_roll = should_roll(player, board, placeholders, starting_pos, 0);
                    }
                }
                else if(valid_moves.size() == 2)
                {
                    will_roll = make_move(player, valid_moves.at(0), valid_moves.at(1), placeholders, board);

                    if(!will_roll)
                    {
                        if(check_won(player, board))
                        {
                            game_won = true;
                            delete[] roll;
                            break;
                        }
                        will_roll = should_roll(player, board, placeholders, starting_pos, 0);
                    }
                }
                else
                {
                    not_missed = false;
                    for (int i = 0; i < num_columns; i++)
                    {
                        board[player][i] = starting_pos[i];
                    }
                }
                delete[] roll;
            }
            set_captured(player, placeholders, board);
            delete[] starting_pos;
        }
    }

    free_board(board);
    if(player == 1)
    	return true;
    return false;
}

int play_rollout_game(int player_no, double error_rate, int** initial_board, int* initial_starting_pos, vector<int>& initial_placeholders, int future_pos0, int future_pos1, int roll_decision, int num_turns)
{
	int** board = instantiate_board();
	bool game_won, first_time;
	int player, num_wins = 0;
	int winner = -1;

	for(int turn = 0; turn < num_turns; turn++)
	{	
		game_won = false;
		first_time = true;
		for(int i = 0; i < num_players; i++)
			for(int j = 0; j < num_columns; j++)
				board[i][j] = initial_board[i][j];    

		while(!game_won)
		{
		    for(player = 0; player < num_players && !game_won; player++)
		    {
		    	bool will_roll = true, not_missed = true;
		        int* starting_pos = get_starting_pos(player, board);
		        vector<int> placeholders;      
		        if (first_time)
				{
					first_time = false;
					player = player_no;

					for(int i = 0; i < num_columns; i++)
						starting_pos[i] = initial_starting_pos[i];					
					placeholders = initial_placeholders;
					if(roll_decision == roll_not_preset)
					{
						will_roll = make_move(player, future_pos0, future_pos1, placeholders, board);
						if (!will_roll)
						{
							if(check_won(player, board))
							{
								game_won = true;
								winner = player;
								break;
							}
							if(player == 0)
								will_roll = should_roll(player, board, placeholders, starting_pos, 0);
							else
								will_roll = should_roll(player, board, placeholders, starting_pos, error_rate);
						}
					}
					else if(roll_decision == preset_stop)
						will_roll = false;
					else
						will_roll = true;					
				}

		        while(will_roll && not_missed)
		        {
		            int* roll = roll_dice();
		            vector<int> valid_moves = get_valid_moves(player, roll, placeholders, board); 
		            if(valid_moves.size() > 2)
		            {
		                int index = decide_on_pair(player, valid_moves, placeholders, board, starting_pos);

		                will_roll = make_move(player, valid_moves.at(index), valid_moves.at(index + 1), placeholders, board);

		                if(!will_roll)
		                {
		                    if(check_won(player, board))
		                    {
		                        game_won = true;
		                        winner = player;
		                        delete[] roll;
		                     	break;
		                    }
		                    if(player == 0)
		                    	will_roll = should_roll(player, board, placeholders, starting_pos, 0);
		                    else
		                    	will_roll = should_roll(player, board, placeholders, starting_pos, error_rate);
		                }
		            }
		            else if(valid_moves.size() == 2)
		            {
		                will_roll = make_move(player, valid_moves.at(0), valid_moves.at(1), placeholders, board);

		                if(!will_roll)
		                {
		                    if(check_won(player, board))
		                    {
		                        game_won = true;
		                        winner = player;
		                        delete[] roll;
		                        break;
		                    }
		                    if(player == 0)
		                    	will_roll = should_roll(player, board, placeholders, starting_pos, 0);
		                    else
		                    	will_roll = should_roll(player, board, placeholders, starting_pos, error_rate);
		                }
		            }
		            else
		            {
		                not_missed = false;
		                for (int i = 0; i < num_columns; i++)
		                {
		                    board[player][i] = starting_pos[i];
		                }
		            }
		            delete[] roll;
		        }
		        set_captured(player, placeholders, board);
		        delete[] starting_pos;
		    }
		}
		if(winner == player_no)
			num_wins++;
    }

    free_board(board);    
    return num_wins;	
}

int decide_on_pair_mc(int player, vector<int> valid_moves, vector<int>& placeholders, int** board, int* starting_pos)
{
	int num_rolls = 60000;
	int vm_size = valid_moves.size();
	
	double max_value = numeric_limits<double>::min();
	double value;
	int max_index = 0;
	double error_rate = 0;
	
	cout << endl;
	if(consider_opponent_error)
	{
		ifstream read("cpp/comm/error_data.txt");
		read >> error_rate;
		read.close();
		if(error_rate != 0)
			cout << "\tplaying against opponent with " << error_rate << " error" << endl << endl;
	}

	for (int i = 0; i < vm_size; i+=2)
	{
		int x = valid_moves.at(i), y = valid_moves.at(i+1);
		value = play_rollout_game(player, error_rate, board, starting_pos, placeholders, x, y, roll_not_preset, num_rolls);
		cout << "for " << x << " and " << y << " - i win " << value << "/" << num_rolls << " ~ " << (double)value/num_rolls << endl;
		
		if(value > max_value)
		{
			max_value = value;
			max_index = i;
		}

		if(consider_opponent_error && run_errorless_games && error_rate != 0)
		{
			value = play_rollout_game(player, 0, board, starting_pos, placeholders, x, y, roll_not_preset, num_rolls);
			cout << "\tif opponent had no error: " << value << "/" << num_rolls << " ~ " << (double)value/num_rolls << endl;
		}
	}
	cout << endl;

	return max_index;
}

bool should_roll_mc(int player, int** board, vector<int>& placeholders, int* starting_pos)
{
	int num_rolls = 60000;

	double error_rate = 0;

	cout << endl;
	if(consider_opponent_error)
	{
		ifstream read("cpp/comm/error_data.txt");
		read >> error_rate;
		read.close();
		if(error_rate != 0)
			cout << "\tplaying against opponent with " << error_rate << " error"  << endl << endl;
	}

	int value_if_roll = play_rollout_game(player, error_rate, board, starting_pos, placeholders, empty_move, empty_move, preset_roll, num_rolls);
	cout << "for roll i win " << value_if_roll << "/" << num_rolls << " ~ " << (double)value_if_roll/num_rolls <<endl;
	int value_if_stop = play_rollout_game(player, error_rate, board, starting_pos, placeholders, empty_move, empty_move, preset_stop, num_rolls);
	cout << "for stop i win " << value_if_stop << "/" << num_rolls << " ~ " << (double)value_if_stop/num_rolls << endl;

	if(consider_opponent_error && run_errorless_games && error_rate != 0)
	{
		int errorless_value_if_roll = play_rollout_game(player, 0, board, starting_pos, placeholders, empty_move, empty_move, preset_roll, num_rolls);
		cout << "\tfor no opponent error & roll i win " << errorless_value_if_roll << "/" << num_rolls << " ~ " << (double)errorless_value_if_roll/num_rolls <<endl;
		int errorless_value_if_stop = play_rollout_game(player, 0, board, starting_pos, placeholders, empty_move, empty_move, preset_stop, num_rolls);
		cout << "\tfor no opponent error & stop i win " << errorless_value_if_stop << "/" << num_rolls << " ~ " << (double)errorless_value_if_stop/num_rolls << endl;
	}

	cout << endl;

	return value_if_roll > value_if_stop;
}

/*int main()
{
	srand((unsigned)time(NULL));
	initialize_prob_arrays();

	int count = 0;
	for(int i = 0; i < 8; i++)
	{
		if(play_full_game())
			count++;
	}

	cout << count << endl;
	free_prob_arrays();
}*/