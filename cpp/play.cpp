#include <iostream>
#include <string>
#include "colors.h"
#include "rollout.cpp"

using std::string;
using std::cout;
using std::cin;
using std::endl;

//console game playing functions
void print_board_horizontal(int** board, int player, vector<int> placeholders, int* starting_pos)
{
    cout << "\033[2J\033[1;1H";
    string playerOne = BOLDRED + "*" + RESET, playerTwo = BOLDGREEN + "*" + RESET, placeholder = BOLDWHITE + "*" + RESET;
    for (int p = 0; p < 11; p++)
    {
        if (p < 8) cout << BOLDBLUE << (p + 2) <<  ":  " << RESET;
        else cout << BOLDBLUE << (p + 2) << ": " << RESET;
        for (int s = 0; s < default_values[p]; s++)
        {
            cout << "[";
            for (int i = 0; i < num_players; i++)
            {
                if (default_values[p] + board[i][p] == s + 1 && board[i][p] != (-1) * default_values[p])
                {
                    if (i == 0) { if (!(vec_contains(placeholders, p+2) && player == 0)) cout << playerOne; else cout << placeholder; }
                    else { if (!(vec_contains(placeholders, p + 2) && player == 1)) cout << playerTwo; else cout << placeholder; }
                }
                else if (default_values[p] + starting_pos[p] == s + 1 && player == i)
                {
                    if (player == 0) cout << playerOne;
                    else cout << playerTwo;
                }
            } cout << "]";
        }
        if (p != 10) cout << endl;
    } cout << endl;
}

void print_board(int** board, int player, vector<int> placeholders, int* starting_pos)
{
    int* perm_player1;
    int* perm_player2;
    if(player == 0)
    {
        perm_player1 = starting_pos;
        perm_player2 = board[1];
    }
    else
    {
        perm_player1 = board[0];
        perm_player2 = starting_pos;
    }

    cout << "\033[2J\033[1;1H";
    string player_one = BOLDBLUE + "*" + RESET, player_two = BOLDGREEN + "*" + RESET, placeholder = BOLDWHITE + "*" + RESET;
    string placeholder_on = BOLDRED + "*" + RESET;
    for(int i = 0; i < 14; i++)
    {
        int diff = 14 - i;
        for(int j = 0; j < num_columns; j++)
        {
            if(default_values[j] + 1 < diff) cout << "    ";
            else if(default_values[j] + 1 == diff) { if(j < 8) cout << BOLDBLACK << " " << (j+2) << "  " << RESET; else cout << BOLDBLACK << " " << (j+2) << " " << RESET;}
            else
            {
                int pr = 0;
                cout << "[";
                if((!player && board[0][j] != default_values[j] * -1 && board[0][j] == diff - default_values[j] && board[0][j] == perm_player2[j]) 
                    || (player && board[1][j] != default_values[j] * -1 && board[board[0][j] == diff - default_values[j] && 1][j] == diff - default_values[j] && board[1][j] == perm_player1[j])) {cout << placeholder_on; pr++;}
                else
                {
                    if(perm_player1[j] == captured) {cout << player_two; pr++;}
                    else if (perm_player2[j] == captured) {cout << player_one; pr++;}
                    else
                    {
                        if(perm_player1[j] == diff - default_values[j]) {cout << player_one; pr++;}
                        if(perm_player2[j] == diff - default_values[j]) {cout << player_two; pr++;}
                        if(vec_contains(placeholders, j + 2) && board[player][j] == diff - default_values[j]) {cout << placeholder; pr++;}
                    }
                }

                for(int k = 1 - pr; k > 0; k--) cout << " ";
                cout << "] ";
            }
        }
        cout << endl;
    }
}
void print_roll_valid_moves(int* roll, vector<int> valid_moves)
{
    cout << BLUE << "roll is" << BLACK << " -> ";
    for(int i = 0; i < 4; i++) cout << CYAN << roll[i] <<  BLACK << " , ";
    cout << endl << BLUE << "valid moves are" << BLACK << " -> |";
    for(int i = 0; i < valid_moves.size(); i += 2) 
    {
        if(valid_moves.at(i) == empty_move)
            cout << BOLDRED  << " " << i << RESET << BLACK << " :   " << YELLOW << valid_moves.at(i+1) << BLACK << "   |";
        else if(valid_moves.at(i+1) == empty_move)
            cout << BOLDRED  << " " << i << RESET << BLACK << " :   " << YELLOW << valid_moves.at(i) << BLACK << "   |";
        else
             cout << BOLDRED  << " " << i << RESET << BLACK << " : " << YELLOW << valid_moves.at(i) << BLACK << " , " << YELLOW << valid_moves.at(i+1) << BLACK << " |";
    }
    cout << RESET << endl;
}

int get_user_choice_of_pairing()
{
    cout << BLUE << "enter index of move: " << RESET;
    int choice; cin >> choice; cin.ignore();
    return choice;
}

bool get_user_choice_of_stopping()
{
    cout << BLUE << "enter " << RED << "0" << BLUE << " to stop, " << GREEN << "1" << BLUE << " to roll: " << RESET; 
    int choice; cin >> choice; cin.ignore();
    return choice;
}

void get_user_acknowledgment()
{
    cout << BLACK << "enter to continue" << RESET;
    cin.ignore();
    cout << endl;
}

//error estimation
void estimate_rolling_error(bool decision_made, int** initial_board, int* initial_starting_pos, vector<int>& initial_placeholders)
{
    double cur_error_rate;
    int cur_count;
    ifstream read("cpp/comm/error_data.txt");
    read >> cur_error_rate;
    read >> cur_count;
    read.close();
    cout <<  endl << "\topponent's current error is " << cur_error_rate << endl;
    double temp = 0;

    bool first_time = true;

    int max_times = 30;
    int turn = 0;
    while(turn < max_times)
    {
        turn++;

        int roll = play_rollout_game(1, temp, initial_board, initial_starting_pos, initial_placeholders, empty_move, empty_move, preset_roll, 1500);
        int stop = play_rollout_game(1, temp, initial_board, initial_starting_pos, initial_placeholders, empty_move, empty_move, preset_stop, 1500);
        
        if(decision_made)
        {
            cout << "shouldn't be here yet"  << endl;
            if(roll > stop)
                break;
            else
                temp -= 0.05;
        }
        else
        {
            if(roll < stop)
            {
                cout << "\t\t" << roll << " vs " << stop << endl;
                 break;
            }                                          
            else
                temp += 0.05;
        }
        first_time = false;
    }

    cur_error_rate = (cur_error_rate*cur_count + temp)/(cur_count + 1);
    cur_count++;

    std::ofstream write("cpp/comm/error_data.txt");
    write << cur_error_rate << " " << cur_count;
    write.close();

    cout << "\tnew turn's error is estimated at " << temp << endl;
    cout <<  "\taverage error updated to " << cur_error_rate  << endl << endl;
    get_user_acknowledgment();
}

void play_game(int starter)
{
    int player = starter;
    bool game_won = false, first_time = true;
    int** board = instantiate_board();

    while(!game_won)
    {
        if(!first_time)
            player = 0;
        first_time = false;
        for(; player < num_players && !game_won; player++)
        {
            bool will_roll = true, not_missed = true;
            int* starting_pos = get_starting_pos(player, board);
            vector<int> placeholders;
            while(will_roll && not_missed)
            {
                print_board(board, player, placeholders, starting_pos);
                int* roll = roll_dice();
                vector<int> valid_moves = get_valid_moves(player, roll, placeholders, board);
                print_roll_valid_moves(roll, valid_moves);
                if(valid_moves.size() > 2)
                {
                    int index;
                    if(player == 0)
                        index = decide_on_pair_mc(player, valid_moves, placeholders, board, starting_pos);
                    else
                        index  = get_user_choice_of_pairing();

                    will_roll = make_move(player, valid_moves.at(index), valid_moves.at(index + 1), placeholders, board);
                
                    if(!will_roll)
                    {
                        if(check_won(player, board))
                        {
                            get_user_acknowledgment();
                            game_won = true;
                            set_captured(player, placeholders, board);
                            print_board(board, player, placeholders, starting_pos);
                            delete[] roll;
                            break;
                        }
                        if(player == 0)
                        {
                            get_user_acknowledgment();
                            print_board(board, player, placeholders, starting_pos);
                            cout << BLUE << "deciding to roll or not..." << RESET << endl;
                            will_roll = should_roll_mc(player, board, placeholders, starting_pos);
                        }
                        else
                        {
                            print_board(board, player, placeholders, starting_pos);
                            will_roll = get_user_choice_of_stopping();
                            if(!will_roll)
                                estimate_rolling_error(will_roll, board, starting_pos, placeholders);
                        }
                    }
                }
                else if(valid_moves.size() == 2)
                {
                    cout << GREEN << "only one move available" << RESET << endl;
                    will_roll = make_move(player, valid_moves.at(0), valid_moves.at(1), placeholders, board);
                    if(!will_roll)
                    {
                        if(check_won(player, board))
                        {
                            get_user_acknowledgment();
                            game_won = true;
                            set_captured(player, placeholders, board);
                            print_board(board, player, placeholders, starting_pos);
                            delete[] roll;
                            break;
                        }
                        if(player == 0)
                        {
                            get_user_acknowledgment();
                            print_board(board, player, placeholders, starting_pos);
                            cout << BLUE << "deciding to roll or not..." << RESET << endl;
                            will_roll = should_roll_mc(player, board, placeholders, starting_pos);
                        }
                        else
                        {
                            get_user_acknowledgment();
                            print_board(board, player, placeholders, starting_pos);
                            will_roll = get_user_choice_of_stopping();
                            if(!will_roll)
                                estimate_rolling_error(will_roll, board, starting_pos, placeholders);                 
                        }
                    }
                    else
                        if(player != 0)
                            get_user_acknowledgment();
                }
                else
                {
                    not_missed = false;
                    for (int i = 0; i < num_columns; i++)
                    {
                        board[player][i] = starting_pos[i];
                    }
                    cout << RED << "missed :(" << RESET << endl;
                    if(player != 0)
                    {
                        //cin.ignore();
                        get_user_acknowledgment();
                    }
                }

                delete[] roll;
                if(player == 0)
                    get_user_acknowledgment();
                  
            }
            set_captured(player, placeholders, board);
            delete[] starting_pos;
        }
    }

    if(player == 1)
    {
        cout << RED << "the computer has won" << RESET << endl;
    }
    else
    {
        cout << GREEN << "you have won" << RESET << endl;
    }

    free_board(board);
}

int main()
{
    srand((int)time(NULL));
    std::ofstream write("cpp/comm/error_data.txt");
    write << 0 << " " << 0;
    write.close();
    initialize_prob_arrays();
    cout << "do you want to run errorless games, 1 or 0: ";
    int ans;
    cin >> ans; cout << endl;
    if(ans != 0)
        run_errorless_games = true;
    play_game(1);
    free_prob_arrays();
}

/*void test_code()
{
  int** board = instantiate_board();
    board[0][0] = 0;
    board[0][5] = captured;
    board[0][2] = captured;
    vector<int> vals;
    for(int i = 0; i < 13; i++)
         vals.push_back(0);
    for(int i = 1; i < 7; i++)
        for(int j = 1; j < 7; j++)
            for(int k = 1; k < 7; k++)
                for(int t = 1; t < 7; t++)
                {
                    int* roll = new int[4]; roll[0] = i; roll[1] = j; roll[2] = k; roll[3] = t;
                    int* starting_pos = get_starting_pos(0, board);
                    vector<int> placeholders;
                    placeholders.push_back(3);
                    placeholders.push_back(12);
                    vector<int> valid_moves = get_valid_moves(0, roll, placeholders, board);
                    for(int p = 0; p < valid_moves.size(); p+=2)
                    {
                        if(valid_moves.at(p) != empty_move)
                            vals.at(valid_moves.at(p)) = vals.at(valid_moves.at(p)) + 1;
                        if(valid_moves.at(p+1) != empty_move)
                        vals.at(valid_moves.at(p+1)) = vals.at(valid_moves.at(p+1)) + 1;

                        //cout << valid_moves.at(p) << "," << valid_moves.at(p+1) << "|";
                    }
                    //cout << endl;
                    //cout << ind << " " << valid_moves.at(ind) << "  " << valid_moves.at(ind + 1) << endl;
                   
                    delete[] starting_pos;
                    delete[] roll;
                }
    for(int i = 0; i < vals.size(); i++)
        cout << vals.at(i) << endl;
    free_board(board);
}*/