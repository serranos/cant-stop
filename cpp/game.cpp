#include <cstdlib>
#include <vector>
#include "constants.h"
#include <time.h>

using std::vector;

const int captured = -100;
const int won = 0;
const int num_players = 2;

//utility
bool vec_contains(vector<int>& vec, int x)
{
    int s = vec.size();
    for(int i = 0; i < s; i++)
        if(vec.at(i) == x)
            return true;
    return false;
}

void free_board(int** board)
{
    for(int i = 0; i < num_players; i++)
        delete[] board[i];
    delete[] board;
}

int** copy_of_board(int** board)
{
    int** copy_board = new int*[num_players];
    for(int i = 0; i < num_players; i++)
        copy_board[i] = new int[num_columns];

    for(int i = 0; i < num_players; i++)
        for(int j = 0; j < num_columns; j++)
            copy_board[i][j] = board[i][j];

    return copy_board;
}

//game control functions
int** instantiate_board()
{
    int** board = new int*[num_players];
    for (int i = 0; i < num_players; i++)
        board[i] = new int[num_columns];

    for (int i = 0; i < num_columns; i++)
    {
        for (int j = 0; j < num_players; j++)
        {
            board[j][i] = -1 * default_values[i];
        }
    }

   return board;
}

int* get_starting_pos(int player, int** board)
{
    int* starting_pos = new int[num_columns];
    for (int i = 0; i < num_columns; i++)
    {
        starting_pos[i] = board[player][i];
    }
    return starting_pos;
}

void set_captured(int player, vector<int>& placeholders, int** board)
{
    int pl_size = placeholders.size();
    int p;
    for (int i = 0; i < pl_size; i++)
    {
        p = placeholders.at(i);
        if (board[player][p-2] == 0)
        {
            for (int k = 0; k < num_players; k++)
            {
                if (k!=player)
                    board[k][p-2] = captured;
            }
        }
    }
}

bool check_won(int player, int** board)
{
    int won_cols = 0;
    for (int i = 0; i < num_columns; i++)
    {
        if (board[player][i] == won)
            won_cols++;
    }
    return won_cols >= 3;
}

bool make_move(int player, int pos0, int pos1, vector<int>& placeholders, int** board)
{
    if(pos0 != empty_move)
    {
        if(vec_contains(placeholders, pos0))
        {
            if(board[player][pos0 - 2] != won)
                board[player][pos0 - 2]++;
        }
        else
        {
            placeholders.push_back(pos0);
            board[player][pos0 - 2]++;
        }
    }
    if(pos1 != empty_move)
    {
        if(vec_contains(placeholders, pos1))
        {
            if(board[player][pos1 - 2] != won)
                board[player][pos1 - 2]++;
        }
        else
        {
            placeholders.push_back(pos1);
            board[player][pos1 - 2]++;
        }
    }

    for(int p = 0; p < num_players; p++)
    {
        for(int p2 = p + 1; p2 < num_players; p2++)
        {
            for(int i = 0; i < num_columns; i++)
            {
                if(board[p][i] != -1 * default_values[i] && board[p][i] == board[p2][i])
                    return true;
            }
        }
    }

    return false;
}

int* roll_dice()
{
    int* ret = new int[4];
    for (int i = 0; i < 4; i++)
        ret[i] = rand() % 6 + 1;
    return ret;
}

vector<int> get_possible_pairs(int* roll)
{
    vector<int>  pairs;
    int total_sum = roll[0] + roll[1] + roll[2] + roll[3];

    for (int i = 1; i < 4; i++)
    {
        int sum1 = roll[0] + roll[i];
        int sum2 = total_sum - sum1;

        bool found = false;
        int pairs_size = pairs.size();
        for(int j = 0; j < pairs_size && !found; j = j + 2)
        {
            if(pairs.at(j) * pairs.at(j + 1) == sum1 * sum2)
                found = true;
        }

        if(!found)
        {
            pairs.push_back(sum1);
            pairs.push_back(sum2);
        }
    }

    return pairs;
}

vector<int> get_valid_moves(int player, int* roll, vector<int>& placeholders, int** board)
{
    vector<int> moves;
    vector<int> pairs = get_possible_pairs(roll);
    int pairs_size = pairs.size();
    int placeholders_size = placeholders.size();
    for (int i = 0; i < pairs_size; i = i + 2)
    {
        int pos0 = pairs.at(i), pos1 = pairs.at(i + 1);

        bool has0 = false, has1 = false;
        for (int j = 0; j < placeholders_size; j++)
        {
            if(pos0 == placeholders.at(j))
                has0 = true;
            if(pos1 == placeholders.at(j))
                has1 = true;
        }

        if(board[player][pos0 - 2] == won)
            has0 = false;
        if(board[player][pos1 - 2] == won)
            has1 = false;

        if(has0 && has1)
        {
            moves.push_back(pos0);
            moves.push_back(pos1);
        }
        else if(has0)
        {
            moves.push_back(pos0);
            if(placeholders_size < 3 && board[player][pos1 - 2] != captured && board[player][pos1 - 2] != won)
                moves.push_back(pos1);
            else
                moves.push_back(empty_move);
        }
        else if(has1)
        {
            moves.push_back(pos1);
            if(placeholders_size < 3 && board[player][pos0 - 2] != captured && board[player][pos0 - 2] != won)
                moves.push_back(pos0);
            else
                moves.push_back(empty_move);
        }
        else
        {
            if(placeholders_size < 2)
            {
                if(board[player][pos0 - 2] != captured && board[player][pos0 - 2] != won)
                    moves.push_back(pos0);
                else
                    moves.push_back(empty_move);
                if(board[player][pos1 - 2] != captured && board[player][pos1 - 2] != won)
                    moves.push_back(pos1);
                else
                    moves.push_back(empty_move);
                if(moves.at(moves.size() - 1) == empty_move && moves.at(moves.size() - 2) == empty_move)
                {
                    moves.pop_back();
                    moves.pop_back();
                }
            }
            else if(placeholders_size < 3)
            {
                if(pos0 != pos1)
                {
                    if(board[player][pos0 - 2] != captured && board[player][pos0 - 2] != won)
                    {
                        moves.push_back(pos0);
                        moves.push_back(empty_move);
                    }
                    if(board[player][pos1 - 2] != captured && board[player][pos1 - 2] != won)
                    {
                        moves.push_back(pos1);
                        moves.push_back(empty_move);
                    }
                }
                else if(board[player][pos0 - 2] != captured && board[player][pos0 - 2] != won)
                {
                    moves.push_back(pos0);
                    moves.push_back(pos0);
                }
            }
        }
    }

    return moves;
}