#include "../../rollout.cpp"

using std::ofstream;

ofstream write;

int decide_on_pair_ob(int player, vector<int>& valid_moves, vector<int>& placeholders, int** board, int* starting_pos)
{
	int num_rolls = 60000;
	int vm_size = valid_moves.size();
	
	double max_value = numeric_limits<double>::min();
	double value;
	int max_index = 0;

	for (int i = 0; i < vm_size; i+=2)
	{
		int x = valid_moves.at(i), y = valid_moves.at(i+1);
		value = play_rollout_game(0, 0, board, starting_pos, placeholders, x, y, roll_not_preset, num_rolls);
		write << "\t\t[ " << x << " " << y << " ] winrate: " << (double)value/num_rolls << " (" << value << "/" << num_rolls << ")" << endl; 

		if(value > max_value)
		{
			max_value = value;
			max_index = i;
		}
	}

	return max_index;
}

int main()
{
	srand((unsigned)time(NULL));
	initialize_prob_arrays();
	ifstream read("cpp/files/dice_combos.txt");
	write.open("opening_book.txt");
	write << "------------------- OPENING BOOK -----------------------" << endl;
	int** board = instantiate_board();
	int* starting_pos = get_starting_pos(0, board);
	vector<int> placeholders;
	int *roll = new int[4];
	for(int i = 0; i < 126; i++)
	{
		cout << i << endl;
		write << "Roll #" << i << ": ";
		int value;
		for(int j = 0; j < 4; j++)
		{
			read >> value;
			roll[j] = value;
			write << roll[j] << " ";
		}
		read >> value; //frequency
		write << " - frequency of roll: " << value << endl;

		vector<int> valid_moves = get_valid_moves(0, roll, placeholders, board);
		int index = decide_on_pair_ob(0, valid_moves, placeholders, board, starting_pos);

		write << "\tChosen move: " << valid_moves.at(index) << " , " << valid_moves.at(index + 1) << endl << endl;
	}
	read.close();
	write.close();
	free_prob_arrays();
}