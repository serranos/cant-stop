#include <iostream>
#include "../../game.cpp"
#include "doublefann.h"
#include "fann_cpp.h"


using std::string;
using std::cout;
using std::cin;
using std::endl;

FANN::neural_net pairnet, rollnet;
const int num_inputs = 448;
const unsigned int num_output = 1;
const double desired_error = 0.001;
const unsigned int max_iterations = 1000;

//encoding functions
double* encode_board(int* starting_pos, int** board, int player)
{
    double* encoding;
    encoding = new double[num_inputs];
	
	int opp;	
	if(player == 0)
		opp = 1;
	else
		opp = 0;
    
	int cur = 0;

    for(int i = 0; i < num_columns; i++)
    {
        if (board[player][i] == 0)
        {
            encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 1; cur++;
        }
        else if (board[opp][i] == 0)
        {
            encoding[cur] = 0; cur++; encoding[cur] = 1; cur++; encoding[cur] = 0; cur++;
        }
        else
        {
            encoding[cur] = 1; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++;
        }

        for (int j = 1; j <= default_values[i]; j++)
        {
            if (board[player][i] + default_values[i] == j)//one is on
            {
                if (starting_pos[i] == board[player][i])//one permanently on
                {
                    encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 1; cur++;
                }
                else //one got on this turn
                {
                    if (board[opp][i] + default_values[i] == j)//two permanently on
                    {
                        encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 1; cur++; encoding[cur] = 0; cur++;
                    }
                    else
                    {
                        encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 1; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++;
                    }
                }
            }
            else//one is off
            {
                if (board[opp][i] + default_values[i] == j)//two permanently on
                {
                    encoding[cur] = 0; cur++; encoding[cur] = 1; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++;
                }
                else
                {
                    encoding[cur] = 1; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++; encoding[cur] = 0; cur++;
                }
            }
        }
    }
    return encoding;
}

double* encode_board_with_roll(double* encoding, bool rolled)
{
    double* new_encoding = new double[num_inputs + 1];

    for(int i = 0; i < num_inputs; i++)
        new_encoding[i] = encoding[i];

    new_encoding[num_inputs] = rolled;
    return new_encoding;
}

//decision making functions
bool should_roll(int player, int** board, vector<int>& placeholders, int* starting_pos)
{
    fann_type* board_encoding = encode_board(starting_pos, board);
    fann_type* roll = encode_board_with_roll(board_encoding, true);
    fann_type* stop = encode_board_with_roll(board_encoding, false);
    fann_type* res_roll = rollnet.run(roll);
    fann_type* res_stop = rollnet.run(stop);

    //cout << "for roll: " << res_roll[0] << ", for stop: " << res_stop[0] << endl;

    bool ret = res_roll[0] > res_stop[0];
    delete[] board_encoding;
    delete[] roll;
    delete[] stop;

    return ret;
}

int decide_on_pair(int player, vector<int> valid_moves, vector<int>& placeholders, int** board, int* starting_pos)
{
    double max_value = 0;
    int max_index = 0;
    int vm_size = valid_moves.size();
    for (int i = 0; i < vm_size; i += 2)
    {
        int** copy_board = copy_of_board(board);
        vector<int> copy_placeholders(placeholders);
        make_move(player, valid_moves.at(i), valid_moves.at(i + 1), copy_placeholders, copy_board);
        fann_type* m = encode_board(starting_pos, copy_board);
        fann_type* res = pairnet.run(m);

        //cout << "for " << i << ": " << res[0] << ", ";
        if(res[0] >= max_value)
        {
            max_value = res[0];
            max_index = i;
        }

        delete[] m;
        free_board(copy_board);
    }

    //cout << endl;
    return max_index;
}

//network setup functions
void initialize_networks()
{
    pairnet.create_from_file("pairnet.net");
    rollnet.create_from_file("rollnet.net");
}

void print_net_info(FANN::neural_net net) //from fann example code
{
    cout << endl << "Network Type                         :  ";
    switch (net.get_network_type())
    {
        case FANN::LAYER:
            cout << "LAYER" << endl;
            break;
        case FANN::SHORTCUT:
            cout << "SHORTCUT" << endl;
            break;
        default:
            cout << "UNKNOWN" << endl;
            break;
    }
    net.print_parameters();
}

//training functions
vector<vector<double*>> play_training_game(bool& player_one_won)
{
    vector<vector<double*>> return_values;
    vector<double*> vec0, vec1, vec2, vec3;
    return_values.push_back(vec0);
    return_values.push_back(vec1);
    return_values.push_back(vec2);
    return_values.push_back(vec3);

    int player = 0;
    bool game_won = false;
    int** board = instantiate_board();

    while(!game_won)
    {
        for(player = 0; player < num_players && !game_won; player++)
        {
            bool will_roll = true, not_missed = true;
            int* starting_pos = get_starting_pos(player, board);
            vector<int> placeholders;
            while(will_roll && not_missed)
            {
                //print_board(board, player, placeholders, starting_pos); int c; cin >> c;
                int* roll = roll_dice();
                //cout << "roll: ";  for(int i = 0; i < 4; i++) cout << roll[i] << " "; cout << endl;
                vector<int> valid_moves = get_valid_moves(player, roll, placeholders, board);
                if(valid_moves.size() > 2)
                {
                    int index = decide_on_pair(player, valid_moves, placeholders, board, starting_pos);

                    will_roll = make_move(player, valid_moves.at(index), valid_moves.at(index + 1), placeholders, board);

                    if(!will_roll)
                    {
                        if(check_won(player, board))
                        {
                            game_won = true;
                            delete[] roll;
                            double* encoding = encode_board(starting_pos, board);
                            return_values.at(2*player).push_back(encoding);
                            return_values.at(2*player + 1).push_back(encode_board_with_roll(encoding, false));
                            break;
                        }
                        will_roll = should_roll(player, board, placeholders, starting_pos);
                    }
                    double* encoding = encode_board(starting_pos, board);
                    return_values.at(2*player).push_back(encoding);
                    return_values.at(2*player + 1).push_back(encode_board_with_roll(encoding, will_roll));
                }
                else if(valid_moves.size() == 2)
                {
                    will_roll = make_move(player, valid_moves.at(0), valid_moves.at(1), placeholders, board);

                    if(!will_roll)
                    {
                        if(check_won(player, board))
                        {
                            game_won = true;
                            delete[] roll;
                            double* encoding = encode_board(starting_pos, board);
                            return_values.at(2*player).push_back(encoding);
                            return_values.at(2*player + 1).push_back(encode_board_with_roll(encoding, false));
                            break;
                        }
                        will_roll = should_roll(player, board, placeholders, starting_pos);
                    }
                    double* encoding = encode_board(starting_pos, board);
                    return_values.at(2*player).push_back(encoding);
                    return_values.at(2*player + 1).push_back(encode_board_with_roll(encoding, will_roll));
                }
                else
                {
                    not_missed = false;
                    for (int i = 0; i < num_columns; i++)
                    {
                        board[player][i] = starting_pos[i];
                    }
                }

                delete[] roll;
            }
            set_captured(player, placeholders, board);
            delete[] starting_pos;
        }
    }

    if(player == 1)
    {
        player_one_won = true;
    }
    else
    {
       player_one_won = false;
    }

    free_board(board);
    return return_values;
}

void train_for_one_game()
{
    bool player_one_won;
    vector<vector<double*>> results = play_training_game(player_one_won);
    for(int i = 0; i < num_players; i++)
    {
        double player_won = (player_one_won == (i == 0));
        FANN::training_data pair_data, roll_data;
        unsigned int num_of_moves = results.at(2*i).size();
        fann_type** pair_inputs = new double*[num_of_moves];
        fann_type** pair_outputs = new double*[num_of_moves];
        fann_type** roll_inputs = new double*[num_of_moves];

        for(unsigned int j = 0; j < num_of_moves; j++)
        {
            pair_inputs[j] = results.at(2*i).at(j);
            roll_inputs[j] = results.at(2*i + 1).at(j);
            pair_outputs[j] = new double[1]; pair_outputs[j][0] = player_won;
        }

        pair_data.set_train_data(num_of_moves, num_inputs, pair_inputs, num_output, pair_outputs);
        roll_data.set_train_data(num_of_moves, num_inputs + 1, roll_inputs, num_output, pair_outputs);

        cout << "training pairnet for player " << i << "..." << endl;
        pairnet.train_on_data(pair_data, max_iterations, 0, desired_error);
        cout << "training rollnet for player " << i << "... "<< endl;
        rollnet.train_on_data(roll_data, max_iterations, 0, desired_error);

        for(unsigned int j = 0; j < num_of_moves; j++)
        {
            delete[] pair_inputs[j];
            delete[] pair_outputs[j];
            delete[] roll_inputs[j];
        }
        delete[] pair_inputs;
        delete[] pair_outputs;
        delete[] roll_inputs;
    }
}

void train_network(bool from_file)
{
    if(!from_file)
    {
        const float learning_rate = 0.7f;
        const unsigned int num_layers = 4;
        const unsigned int num_hidden = 300;

        pairnet.create_standard(num_layers, num_inputs, num_hidden, num_hidden, num_output);
        pairnet.set_learning_rate(learning_rate);
        pairnet.set_activation_steepness_hidden(1.0);
        pairnet.set_activation_steepness_output(1.0);
        pairnet.set_activation_function_hidden(FANN::SIGMOID_SYMMETRIC_STEPWISE);
        pairnet.set_activation_function_output(FANN::SIGMOID_SYMMETRIC_STEPWISE);
        pairnet.randomize_weights(-0.1, 0.1);
        print_net_info(pairnet);

        rollnet.create_standard(num_layers, num_inputs + 1, num_hidden, num_hidden, num_output);
        rollnet.set_learning_rate(learning_rate);
        rollnet.set_activation_steepness_hidden(1.0);
        rollnet.set_activation_steepness_output(1.0);
        rollnet.set_activation_function_hidden(FANN::SIGMOID_SYMMETRIC_STEPWISE);
        rollnet.set_activation_function_output(FANN::SIGMOID_SYMMETRIC_STEPWISE);
        rollnet.randomize_weights(-0.1, 0.1);
        print_net_info(rollnet);      
    }
    else
        initialize_networks();

    for(int i = 0; i < 15; i++)
    {
        cout << "training for game " << i << "..." << endl;
        train_for_one_game();
    }

    pairnet.save("pairnet.net");
    rollnet.save("rollnet.net");
}

int main()
{
    srand((int)time(NULL));

    //initialize_networks();
    //play_game();
    train_network(true);
}