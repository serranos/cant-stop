#ifndef CONSTANTS_H
#define CONSTANTS_H

const int default_values[] = { 3, 5, 7, 9, 11, 13, 11, 9, 7, 5, 3 };
const int num_columns = 11;
const int empty_move = -1;
const int roll_not_preset = -1;
const int preset_stop = 0;
const int preset_roll = 1;

#endif
