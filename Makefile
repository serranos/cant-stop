gui:
	javac -sourcepath src src/Main.java
	java -cp src Main

cons:
	g++ -O3 cpp/play.cpp -o play
	./play

opening_book:
	g++ -O3 cpp/miscellaneous/opening_book/openingbook.cpp -o opening_book
	./opening_book 

reset: 
	rm -rf *o call exam
	g++ -O3 cpp/call.cpp -o call
	g++ -O3 cpp/examine_player.cpp -o exam

clean:
	rm -rf *o play opening_book src/*.class
